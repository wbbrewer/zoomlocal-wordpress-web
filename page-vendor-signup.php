<?php
/*
Template Name: Vendor-Signup
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$user_id = - 1;
if ( is_user_logged_in() ) {
	$user_id = get_current_user_id();
}

zl_vendor_signup();
$yith_instance = yith_get_vendor( $user_id, 'user' );
if ( $yith_instance->id > 0 && ( $yith_instance->is_super_user( $user_id ) || $yith_instance->is_user_admin( $user_id ) ) ) {
	wp_redirect( home_url() . "/my-account/" );
	exit;
} else {
	?>

	<?php get_header(); ?>

	<div id="content">
<section class='vsignup'>
		<div id="inner-content" class="row">

			<div id="main" class="large-12 medium-12 columns" role="main">
				<div class="row zl-shop-head">
					<div class="medium-5 medium-offset-1 columns text-left">
						<ul class="breadcrumbs">
							<li><a href="#">Home</a></li>
							<li><a href="#">My Account</a></li>
							<li><a href="#">Setup</a></li>
						</ul>
					</div>
					<div class="medium-6 columns login-signup text-right">
						<?php 
							if( is_user_logged_in() ){
								if( is_vendor() || is_administrator() ){
									echo '<a class="login" href="'.get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ).'">My Account</a>';	
								}else{
									echo '<a class="login" href="http://listings-dev.zoomlocal.com/my-account/edit-account/">My Account</a>';	
								}
							}else{
								echo '<a class="login" href="'.get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ).'">My Account</a>';		
							}
						?>
					</div>
				</div>
				<?php
				if ( $yith_instance->is_user_admin( $user_id ) && $yith_instance->id > 0 ) {
					echo "<span>dev note: User is already assigned to a store.</span>";
				} else {
					?>
					<form method="post" class="zl-register">
						<?php
						wc_print_notices();
						?>
						<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

						<?php do_action( 'woocommerce_register_form_start' ); ?>
						<?php
						if ( function_exists( 'yith_wcpv_get_template' ) ):
							yith_wcpv_get_template( 'vendor-registration', array(), 'woocommerce/myaccount' );
						endif;
						?>

						<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
						<p class="form-row">
							<?php wp_nonce_field( 'woocommerce-register' ); ?>
							<input type="submit" class="button"
							       name="<?php echo ( is_user_logged_in() ) ? "register-vendor" : "register"; ?>"
							       value="<?php _e( 'Register', 'woocommerce' ); ?>"/>
						</p>

					</form>
					<?php do_action( 'woocommerce_register_form_end' ); ?>
				<?php
				}
				?>


			</div>
			<!-- end #main -->

		</div>
		<!-- end #inner-content -->
		</section>

	</div> <!-- end #content -->
	<?php get_footer();
} ?>
