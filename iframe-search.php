<?php 
/**
 *
 * Template Name: iframe Search 
 *
 */
//get_header();
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<style type="text/css">
.mimage img {
   max-width: 100%;
   width: 100%;
}
.left.mimage {
   width: 70px;
   float: left;
}
.mdata {
   font-size: 14px;
   font-weight: 600;
   line-height: normal;
   margin-left: 5px;
   float:right;
}
.mdata span {
   color:#af1556;
   display: block;
}
.slink {
   float:right;
}
.slink a {
	text-decoration: none;
}

.slink a:before {
   content: "\f002";
   font-family: FontAwesome;
   font-size: 22px;
   margin-right: 7px;
   color: #333;
}	
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.js"></script>

<!--   -->
<!-- <link rel="stylesheet" href="<?php //echo get_template_directory_uri(); ?>/assets/css/foundation.css" />  -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1W13GLH0Lx5esP98njIlPOz_eDolo-dI&libraries=geometry,places,visualization&signed_in=true&callback=initMap" async defer></script>
<script type="text/javascript">
var citymap = {
  gulfport: {
    center: {lat: 30.367420, lng: -89.092816}
  },
};

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 30.367420, lng: -89.092816},
    zoom: 9,
    scrollwheel: false
  });
  my_pos = {lat: 32.269718, lng: -89.992447};
  $('#lat').attr('val', 32.269718);
  $('#lon').attr('val', -89.992447);       
  geocoder = new google.maps.Geocoder();
  infoWindow = new google.maps.InfoWindow({map: map});
  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      my_pos = pos;
      $('#lat').attr('val', pos['lat']);
      $('#lon').attr('val', pos['lng']);
      infoWindow.setPosition(pos);
      infoWindow.setContent('You are here.');
      map.setCenter(pos);
    }, function() {
      // handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    // handleLocationError(false, infoWindow, map.getCenter());
  }
 // for (var city in citymap) {
    // Add the circle for this city to the map.
   // var cityCircle = new google.maps.Circle({
    //  strokeColor: '#FF0000',
     // strokeOpacity: 0.8,
      //strokeWeight: 2,
      //fillColor: '#FF0000',
      //fillOpacity: 0.35,
      //map: map,
      //center: citymap[city].center,
      //radius: 80465
    //});
  //}
  
  google.maps.event.addListener(map, 'bounds_changed', function() {           
            // find out the map's bounds:
            var bounds = map.getBounds();

            // from that, get the coordinates for the NE and SW corners
            var NE = bounds.getNorthEast();
            var SW = bounds.getSouthWest();

            // from that, figure out the latitudes and the longitudes
            var lat1 =  NE.lat();
            var lat2 =  SW.lat();

            var lng1 =  NE.lng();
            var lng2 =  SW.lng();

            // construct new LatLngs using the coordinates for the horizontal distance between lng1 and lng2
            var horizontalLatLng1 = new google.maps.LatLng(lat1,lng1);
            var horizontalLatLng2 = new google.maps.LatLng(lat1,lng2);

            // construct new LatLngs using the coordinates for the vertical distance between lat1 and lat2
            var verticalLatLng1 = new google.maps.LatLng(lat1,lng1);
            var verticalLatLng2 = new google.maps.LatLng(lat2,lng1);

            // work out the distance horizontally
            var horizontal = google.maps.geometry.spherical.computeDistanceBetween(horizontalLatLng1,horizontalLatLng2);

            // work out the distance vertically
            var vertical = google.maps.geometry.spherical.computeDistanceBetween(verticalLatLng1,verticalLatLng2);

            // round to kilometres to 1dp
            var horizontalkm = convertMetresToKm(horizontal);
            var verticalkm = convertMetresToKm(vertical);

            //alert(horizontalkm + ' km horizontal, ' + verticalkm + ' km vertical');
        });
}
function convertMetresToKm(metres) {
        return Math.round(metres / 1000 *10)/10;    // distance in km rounded to 1dp
    }

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	infoWindow.setPosition(pos);
	// infoWindow.setContent(browserHasGeolocation ? '' : 'Error: Your browser doesn\'t support geolocation.');
}
</script>
<script type="text/javascript">
$(document).ready(function(){
	$(document).ajaxComplete(function(){
		// $('.list-fiel-wrapper').on('click', '.shop-title', function() {
		// 	window.location = '#map';
		// 	geo = $(this).attr('pos');
		// 	$('#com').html(geo);
		//     geocodeAddress(geocoder, map);
		//     map.setZoom(15);
		// });
	});
});
function geocodeAddress(geocoder, resultsMap) {
	var address = $('#com').html();
	$('#com').html('');
	geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
		resultsMap.setCenter(results[0].geometry.location);
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
	});
}

var ajax_url = '<?php echo admin_url( "admin-ajax.php", "relative" ); ?>';
$(document).ready(function(){
	$('form#location-search').submit( function(e) {
		e.preventDefault();
		$.ajax({
	        url: ajax_url,
	        type: "POST",
	        data: {
	            'action':'location_search',
	            'value': $(this).serialize(),
	        },
	        success:function(data) {
	        	console.log(data);
	        	start = 0;
	        	per_page = 20;
	        	curr_loc = $('#loc-cord').html();
	        	$('#search-result').html('');
	        	// alert(data);
	        	curr_page = 1;
	            // console.log(data);
	            result = $.parseJSON(data);
	           
	            if(result['success'] == 'true'){
	            	results = result['result'];
	            	icon_url = result['icon_url'];
	            	teldata = result['tel'];
		            // console.log(icon_url);
		            count_result = results.length;
	            	paging();
		            contentstring = [];
			        regionlocation = [];
			        markers = [];
			        iterator = 0;
			        areaiterator = 0;
			        infowindow = [];
			        contentstring = result['contentstring'];
		            regionlocation = result['regionlocation'];
					showmarker();	        	
		        	show_result();
	            }
	            else{
	            	$('#search-result').html('No record found.');
	            	$('.result-count').html('');	
	        		$('.page-count').html('');
	        		$('.page-nav').html('');	
	            	deleteMarkers();
	            }	            
	        },
	        error: function(errorThrown){
	            console.log(errorThrown);
	        }
	    });
	    return false;
	});
	
	$('.page-nav').on('click', '.next', function(){
		$('#search-result').html('');
		curr_page++;
		start = ((curr_page-1)*per_page);
		paging();
		show_result();
	});
	$('.page-nav').on('click', '.prev', function(){
		$('#search-result').html('');
		curr_page--;
		start = (curr_page-1)*per_page;
		paging();
		show_result();
	});
	$(document).on('click', '.shop-title', function() {
		window.location = '#map';
		geo = $(this).attr('pos');
		$('#com').html(geo);
	    geocodeAddress(geocoder, map);
	    map.setZoom(20);
	});
	
		/************* Function to get distance between two cordinates ********************/
function paging(){
	if(count_result <= per_page){
		$('.page-nav').html('');
		total_page = 1;
	    if(count_result == 0){
	        $('.result-count').html('No result found.');	
	        $('.page-count').html('');
	    }else{
	        $('.result-count').html('Showing 1 - '+count_result+' of '+count_result+' results');
	        $('.page-count').html('Page 1 of 1');
	    }
	}else{
	    total_page = parseInt(count_result/per_page);
	    if(count_result%per_page != 0){
	        total_page++;
	    } 
	    $('.result-count').html('Showing '+(start+1)+' - '+(start+per_page)+' of '+count_result+' results');	
	    $('.page-count').html('Page '+curr_page+' of '+total_page);
	    if(curr_page == 1){
	    	$('.page-nav').html( curr_page+' <a class="next"> > </a>');	
	    }
	    else if(curr_page == total_page){
	    	$('.page-nav').html('<a class="prev"> < </a> '+curr_page);
	    }
	    else{
			$('.page-nav').html('<a class="prev"> < </a> '+curr_page+' <a class="next"> > </a>');
		}
	}
}

function show_result(){
	if(curr_page == total_page){
		loop = (count_result % per_page)+2;
	}
	else{
		loop = per_page;
	}
	for( var i=0; i<loop; i++){
		var object = results[start];
		// console.log(object['geoloc']);
		var distance = distanceFromCurrent(curr_loc, object['geoloc']);
		var distance = distance ? distance+' miles' : 'Not Available';
		var geo = object['geoloc'] ? "class='shop-title' pos='"+object['geoloc']+"'" : '';
		var shop_name = object['shop_name'] ? "<a "+geo+">"+object['shop_name']+"</a>" : '';
		var add1 = object['address1'] ? object['address1']+', ' : '';
		var add2 = object['address2'] ? object['address2']+', ' : '';
		var loc = object['location'] ? object['location'] : '';
		var city = object['city'] ? object['city']+', ' : '';
		var state = object['state'] ? object['state'] : '';
		var web = object['store_website'] ? ' or '+object['store_website'] : '';
		var shop_image = object['header_image'] ? object['header_image'] : '<?php echo get_template_directory_uri(); ?>/assets/img/shop.png';
		telephone = object['telephone'] ? object['telephone'] : '';
		$('#search-result').append("<div class=\"list-fiel-wrapper\"><div class=\"large-1 medium-1 columns list-text-color\"><img src="+shop_image+" class=\"shop-images\"></div><div class=\"large-8 medium-8 columns list-text-color padder\"><h3>"+shop_name+"</h3><p>"+add1+""+add2+""+loc+"</p><p>"+city+""+state+"</p><p>"+telephone+""+web+"</p></div><div class=\"large-3 medium-3 small-12 columns list-text-field\"><p class=\"miles\"> "+distance+" </p><span class=\"more\"> <a target='_blank' href="+object['permalink']+">more info >></a> </span></div></div>");
		start++;
	}
}

function distanceFromCurrent(org, des){  
    var currLat = $('#lat').attr('val');
    var currLon = $('#lon').attr('val');
    if(des == null){
    	return '';
    }
    georss = des;
    var pointLatLon = georss.split(",");
    var pointLat = parseFloat(pointLatLon[0]);
    var pointLon = parseFloat(pointLatLon[1]);
    var R = 6371;                   //Radius of the earth in Km             
    var dLat = toRad(pointLat - currLat);  
   	var dLon = toRad((pointLon - currLon));    //delta (difference between) longitude in radians
	currLat = toRad(currLat);  
	//conversion to radians
	pointLat = toRad(pointLat);
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(currLat) * Math.cos(pointLat);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));   //must use atan2 as simple arctan cannot differentiate 1/1 and -1/-1
	var distance = R * c;   //sets the distance
    distance = Math.round(distance*10)/10;      //rounds number to closest 0.1 km
	    distance = distance*0.621371;    //returns the distance
		return distance.toFixed(2)
	}
	function toRad(thi) 
	{ 
	    return thi * Math.PI / 180;
	}
	/****************************** End of function ************************/
		

	function showmarker() {           
        infowindow = [];
        markers = [];
       	// Getes();
        iterat = 0;
        areaiterator = 0;
        region = new google.maps.LatLng(regionlocation[areaiterator].split(',')[0], regionlocation[areaiterator].split(',')[1]);
        drop();
    }
    
    function drop() {
        for (var i = 0; i < contentstring.length; i++) {
            setTimeout(function() {
            addMarker(i);
            }, 800);
        }
    }

    function addMarker(i) {
    	// alert(icon_url[i]);
        var address = contentstring[areaiterator];
        var icons = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
		var tempicon = 'http://listings-dev.zoomlocal.com/wp-content/themes/ZoomLocal-Merge/assets/img/Nav_Pin.png';
        var templat = regionlocation[areaiterator].split(',')[0];
        var templong = regionlocation[areaiterator].split(',')[1];
         //alert(templat + ',' + templong);
        var temp_latLng = new google.maps.LatLng(templat, templong);
        marker = new google.maps.Marker({
            position: temp_latLng,
            map: map,
            //icon: icon_url[areaiterator],
            icon: tempicon,
			draggable: false
        });
        markers.push( marker );            
        iterator++;
        info(iterator);
        areaiterator++;
  //       bounds = new google.maps.LatLngBounds();
  //       bounds.extend(marker.position);
	
		// map.fitBounds(bounds);
		// map.setZoom(4);
		// //(optional) restore the zoom level after the map is done scaling
		// listener = google.maps.event.addListener(map, "idle", function () {
		    
		//     google.maps.event.removeListener(listener);
		// });

    }

    function info(i) {
    	var stingss = '<div id="spopup">'+'<div class="left mimage">'+'<img src='+teldata[i-1][4]+'>'+'</div>' +'<div class="right mdata">'+
      contentstring[i - 1]+'</br>'+ teldata[i-1][1] + teldata[i-1][2] + teldata[i-1][3]+'</br><span class="stel">'+teldata[i-1][0] +'</span>'+'<div class="slink">'+'<a target="_blank" href='+teldata[i-1][5]+'>Zoom to listings</a>'+'</div>'+
      '</div>' ;
        infowindow[i] = new google.maps.InfoWindow({
            content: stingss
        });
        //infowindow[i].content = contentstring[i - 1];
        google.maps.event.addListener(markers[i - 1], 'click', function() {
	        for (var j = 1; j < contentstring.length + 1; j++) {
	            infowindow[j].close();
	        }
	        infowindow[i].open(map, markers[i - 1]);
	    });
	}

	function deleteMarkers() {
  		clearMarkers();
  		markers = [];
	}
	function clearMarkers() {
		setMapOnAll(null);
	}
	function setMapOnAll(maps) {
  		if(markers)
  		for (var i = 0; i < markers.length; i++) {
    		markers[i].setMap(maps);
  		}
  		infoWindow.setPosition(my_pos);
      	infoWindow.setContent('You are here.');
      	map.setCenter(my_pos);
      	map.setZoom(20);
  	}
  	jQuery(document).ready(function(){
     jQuery(".small").submit();
});
});

</script>
<div id="com"></div>

<style type="text/css">
    #map { height: 100%; }
</style>

<div id="content">
	<div id="inner-content" class="row">
		<div id="main" class="large-12 medium-12 columns" role="main">
			<div class="row">
				<div class="large-12 columns">
					<div class="panel">
					    <div class="row">
					        <div class="large-12 medium-12 small-12 columns">
					        	<div id="map" style="width:100% !important;"></div>
					        </div>
					        <div class="large-12 medium-12 small-12 columns">
					            <h3 class="partner-location">PARTNER LOCATIONS </h3>
					        </div>
					        <div class="large-12 medium-12 small-12 columns" >
					        	<form id="location-search" method="post">
					 			<ul class="Search-feild">
						            <li>
							 			<p class="searching-for">Searching for</p>
							 			<input type="text" placeholder="Store Name" class="shopping-rtl" name="place"/>
						            </li>
						            <li>  
							 			<p class="searching-for">nearby</p>
							 			<input type="text" placeholder="City/State or Zipcode" class="shopping-rtl" name="location"/>
						           	</li>
									<li>
										<input type="submit" class="small button" name="search" value="Search" />
								   	</li>
							  	</ul>
					        	</form>
					        	<div class="border"></div>
					        </div>
					        <div  class="row">
					            <div class="large-12 medium-12 columns">
					            	<ul class="Location-Show">
						            	<li class="result-count"></li>
						            	<li class="page-count"></li>
						            	<li class="page-nav"></li>
					              	</ul>
					            </div>
					        </div>
					      	<div  id="search-result" class="large-12 medium-12 columns"></div>
					    </div>
					</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
<div id="lat" val="" style="display:none;"></div>
<div id="lon" val="" style="display:none;"></div>

<script type="text/javascript">
	
</script>

<?php 
//get_footer();
?>