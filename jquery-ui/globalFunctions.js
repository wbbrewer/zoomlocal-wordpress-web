function displayErrorMessage(idDiv,idSpan, message)
{
	window.scroll(0, 0);
    $( idSpan ).html(message);
    $(idDiv).show();
    setTimeout(function(){ $(idDiv).hide();}, 5000);
}

 function isNumber(evt) 
 {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

//block screen when save data and image
function block_screen() {
  $.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 
}

//unblock screen after  save data and image
function unblock_screen() {
  setTimeout($.unblockUI, 2000); 
}

//share on facebook
function shareFacebookWindow(guid){
    var url = 'https://www.facebook.com/sharer/sharer.php?u='+guid;
    window.open(url);
} 
//share on twitter
function shareTwitterWindow(guid){
    var url = 'https://twitter.com/intent/tweet?url='+guid;
    window.open(url);
}


