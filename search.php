<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . "/assets/css/zl-shop.css"; ?>">
			<div class="home archive post-type-archive post-type-archive-product logged-in woocommerce woocommerce-page">
			<div id="content">

				<div id="inner-content" >
			
					<div id="main" class="large-12" role="main">
					<section class="viewlisting">
						<div class="container">
						<div class="row">
						<div class="small-12 listing-date">
						<div class="small-5 medium-5 columns"><span><?php _e('Search Results for:', 'jointstheme'); ?></span> <?php echo esc_attr(get_search_query()); ?></div>
						</div>
						</div>
						</div>
						</section>
						<p class="woocommerce-result-count"> </p>
					<section class="consumer_products">
                     <div class="row">
                        <ul class="products">
						

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
							<li class="product post-<?php echo get_the_ID(); ?> type-product status-publish has-post-thumbnail product_cat-womens-footware yith_shop_vendor-bay-tique downloadable virtual shipping-taxable purchasable product-type-simple product-cat-womens-footware instock">

								<div class="zl-product-info row small-collapse">

											<div class="small-12 medium-5 columns image-div">
										<a href="<?php echo get_post_permalink(get_the_ID()); ?>">
											<?php $post_image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), $size='medium' ); ?>			
											<img class="attachment-shop_catalog wp-post-image" width="300" height="300" src="<?php echo  $post_image_data[0]; ?>">
											</a>
									</div>
									<div class="small-12 medium-7 columns">

										<h3><a href="<?php echo get_post_permalink(get_the_ID()); ?>"><?php echo get_the_title(); ?></a></h3>

										<div class="despara">
											<span class="desctit">Item Description: </span><?php echo $excerpt = get_the_excerpt() ?>
										</div>

										
									</div>
									<div class="small-12 medium-12 columns">
										<div class="main-price row">
											<div class="small-12 medium-4 columns">
												
							<?php $price =get_post_meta (get_the_ID(),'_sale_price');?>
								<span class="price"><span class="amount"><?php if(!empty($price[0])){echo '$'.$price[0];}?></span></span>
											</div>
											<div class="medium-8 columns clearfix">
											<ul class="soc-list inline-list right">
												<li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><img
															src="<?php echo get_template_directory_uri() . "/images/icons/fb.png"; ?>"/></a>
												</li>
												<li><a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=&description="><img
															src="<?php echo get_template_directory_uri() . "/images/icons/pin.png"; ?>"/></a>
												</li>
												<li><a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>"><img
															src="<?php echo get_template_directory_uri() . "/images/icons/tw.png"; ?>"/></a>
												</li>
												<li><a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><img
															src="<?php echo get_template_directory_uri() . "/images/icons/gplus.png"; ?>"/></a>
												</li>
												<li><a href="mailto:?subject=<?php urlencode(the_title('Check this listing at Zoomlocal: '))?>&body=Hey,%20I%20found%20this%20listing%20at%20Zoomlocal.%20Check%20this%20link%20out%20<?php the_permalink();?>"><img
															src="<?php echo get_template_directory_uri() . "/images/icons/mail.png"; ?>"/></a>
												</li>
												<li><a href="#" onclick="window.prompt('Copy the link: Ctrl+C/Cmd-C, Enter', '<?php the_permalink();?>');"><img
															src="<?php echo get_template_directory_uri() . "/images/icons/dots.png"; ?>"/></a>
												</li>
											</ul>
											</div>

										</div>

									</div>
							</div></li>
												
						<?php endwhile; ?>	
					
						        <?php joints_page_navi(); ?>	
					
					    <?php else : ?>
					
    					    <article id="post-not-found" class="hentry clearfix">
    					    	<header class="article-header">
    					    		<h1>Sorry, No Results.</h1>
    					    	</header>
    					    	<section class="entry-content">
    					    		<p>Try your search again.</p>
    					    	</section>
    					    	<section class="search">
                                    			<p><?php get_search_form(); ?></p>
                		                </section> <!-- end search section -->
    					    	<footer class="article-footer">
    					    	    <p>This is the error message in the search.php template.</p>
    					    	</footer>
    					    </article>
					
					    <?php endif; ?>
			
				    
    			  </ul>
    			  </div>
    			  </section>
    			  </div> <!-- end #main -->
    			    <?php get_sidebar(); ?>
    			
    			</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->
			</div>

<?php get_footer(); ?>
