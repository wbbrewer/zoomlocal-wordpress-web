<div id="sidebar-cta" class="sidebar-cta" role="complementary">

	<?php if ( is_active_sidebar( 'sidebar-cta' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar-cta' ); ?>

	<?php endif; ?>

</div>