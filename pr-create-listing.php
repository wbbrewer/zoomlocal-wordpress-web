<?php
/*
Template Name: pr-create-listing
*/
?>

<?php get_header(); ?>
<head>
    <!--Special configuration to css -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php  bloginfo("template_directory"); ?>/jquery-ui/specialAdjustments.css" />
</head>
<input type="hidden" id="productId"  value="0">
<input type="hidden" id="productSlug"  value="0">
<input type="hidden" id="redirect"  value="N">
			
			<div id="content" style="margin-top: 0px;">
			
				<div id="inner-content" class="row">
			
				    <div id="main" class="large-12 medium-12 columns" role="main">
					
						<?php get_template_part( 'parts/loop', 'page' ); ?>
						 <!-- Success messages -->
                            <div class="success" id="susccessDiv" style="display:none">
                            	<span id="successMsg"></span>
                            </div>

                            <div class="error" id="errorDiv" style="display:none">
                            	<span id="errorMsg"></span>
                            </div>

                            <div class="warning" id="warningDiv" style="display:none">
                            	<span id="warningMsg"></span>
                            </div>

						<!-- PRODUCT AND ACCOUNT INFORMATION SECTION-->
						<section id="productBasic"class="listingsection">

							<div class="user-detail mobile-pe show-for-small-only">
			                     <p>Welcome:&nbsp; <span class="displayLabelInfo" id="userNames"></span></p>
			                     <p>Company:&nbsp;<span class="displayLabelInfo" id="companyNames"></span></span></p>
			                     <p>Account level:&nbsp;<span class="displayLabelInfo" id="levelTitles"></span></p>
                                 <p>Posting remaining:&nbsp;<span class="displayLabelInfo" id="creditss"> </span></p>
                            </div>
                        <div class="row listing-wrapper">
                        <div class="medium-7 columns">
								<label class="displayLabel">Listing Title *</label>
								<div class="input-section">
								<input type="text" name="fname" id="listingTitle" style="width:100%;">
								</div>
								<label class="displayLabel">Listing Description *</label>
								<div class="text-area-section">
								<textarea id="listingDescription" rows="10" cols="50" style="width:100%;" placeholder="Insert up to 500 characters..."></textarea><br>
						        </div>
						        </div>
							<div class="medium-5 columns">
							 <aside class="dynamic-user-detail">
								<!-- <nav class="top-bar" data-topbar style="padding-left:140px;" id="myAccountNav">
			                         my account menu -->
			                        <!-- <section class="top-bar-section left">
			                            <?php //wp_nav_menu('menu=myAccount_menu'); ?>
			                        </section>
			                    </nav> -->
			                     <div class="user-detail hide-for-small-only">
			                     <p>Welcome:&nbsp; <span class="displayLabelInfo" id="userName"></span></p>
			                     <p>Company:&nbsp;<span class="displayLabelInfo" id="companyName"></span></span></p>
			                     <p>Account level:&nbsp;<span class="displayLabelInfo" id="levelTitle"></span></p>
                                 <p>Posting remaining:&nbsp;<span class="displayLabelInfo" id="credits"> </span></p>
                                 </div>
			                    	<!-- <span class="displayLabel">Company:</span><span class="displayLabelInfo" id="companyName"></span><br>
			                    	<span class="displayLabel">Name:</span><span class="displayLabelInfo" id="userName"></span><br>
			                    	<span class="displayLabel">Account Level:</span><span class="displayLabelInfo" id="levelTitle"></span><br>
			                    	<span class="displayLabel">Posting left Pay Period:</span><span class="displayLabelInfo" id="credits"></span><br> -->
			                    
			                    <!-- START: the product image -->
			                    <div class="entry-content imagesection" style="padding-left:90px;">
									<form id="featured_upload" method="post" action="#" enctype="multipart/form-data">
									<label for="image_upload" class='imglable'></label>
										<input type="file" name="image_upload" id="image_upload"  multiple="false" />
										<input style="display: none;" id="submit_image_upload" name="submit_image_upload" type="submit" value="Upload" />
										<input type="hidden" name="post_id" id="post_id" value="0" />
										<?php wp_nonce_field( 'image_upload', 'my_image_upload_nonce' ); ?>
										<input type='hidden' name='action' value='upload_file_request'>
										<input type="hidden" id="attachId"  value="0">
									</form>
									
									<div id="image_preview" >
										<img id="previewing" />
										<h4 id="loading" style="display: none;">loading...</h4>
										<div id="message"></div>
									</div>
									<div id='loadingmessage' style='display:none'>
									  <img src="<?php echo get_template_directory_uri(); ?>/jquery-ui/images/loading.gif"/> Loding.....
									</div>
								</div>
								<!-- END: the product image -->
								</aside>
							</div>
						</div>
						</section>
						<!-- PRODUCT TABS SECTION-->
			    <section class="listing-detail">
                  <div class="row listing-detail-wrapper">
                    <div class="medium-12 columns">
                      <div class="list-detai-content">
						 <div id="productTabsSection" >
							<div id="tabs">
								<ul>
									<li><a href="#tabs-1">Listing Detail</a></li>
									<li><a href="#tabs-2">Matching Item</a></li>
								</ul>
								<div id="tabs-1" class="tabs-content tabcontent">
									<div id="listingDetailCont">
										<label class="displayLabel" >Quantity</label>
										<input type="text" id="listingQty" name="listingQty" style="width:30%;" onKeyPress="return isNumber(event)" ><br>
										<label class="displayLabel" >Price <span>*</span></label>
										<input type="text" id="listingPriceRange" style="width:30%;" onKeyPress="return isNumber(event)"><br>
										<div id="timePickerSpecial">
										<label class="displayLabel" >Listing Date <span>*</span></label>
										 	<input type="text" id="listingDate" style="width:30%; ">
										</div>
									</div>
								</div>
								<div id="tabs-2">
									<table id="matchingItemsGrid"></table>
									<div id="pmatchingItemsGrid"></div>

								</div>
							</div>
						</div>
					</div>
                  </div>
               </div>
            </section>

						<!-- CATEGORY SECTION-->
						<section class="category-section">
                        <div class="row">
                        <div class="medium-12 columns">
						<div id="productCategory">
							<div class="category-header">
                    <h5><span>Next step &gt;</span>Choose product or service categories</h5>
                    <p>Check as many categories of interest. Change selections as often as you’d like.</p>
                           </div>
							<!-- Category Accordion-->
							<div id="categoryDiv">
								<!-- <ul class="accordion" data-accordion style="line-height:15%">
								</ul> -->
								<div class="accordion">
								</div>
								<div class="scriptsdata"></div>
							</div>
						</div>
						</div>
						<!-- SOCIAL MEDIA SECTION-->
						<!-- <div id="productSocialMedia">
						<br><br><br>
						<table style="width:100%; background-color: transparent; border: none !important;">
						  <tr>
						    <td>
								<input type="checkbox" name="facebookCheck" id="facebookCheck" value="facebook">&nbsp;
								<a class="social-icon fb" href="#"><i class="fa fa-facebook"></i>Share on Facebook</a>
								</a>
						    </td>
						    <td>
						    	<input type="checkbox" name="twitterCheck" id="twitterCheck" value="twitter">&nbsp;
								<a class="social-icon twit" href="#"><i class="fa fa-twitter"></i></i>Share on Twitter</a>

						    </td>		
						  </tr>
						  </table>
						</div> -->
						<div class="medium-12 columns">
						<div class="edit-button">
                    <div class="button-group buttongroup">
						<div id="productButtons">
						<!-- <button id="insertBut" onClick="saveAndClose()" class="buttonProducts buttonProductsBlueBack success button clear-button">Save & Close</button> -->
						<!--<button id="deleteBut" onClick="deleteListingData()" class="btn button yel">Clear All</button>-->
						<button id="clearrall" onClick="clearForm()" class="btn button yel">Clear All</button>
						<button id="deleteBut" onClick="deleteListingData()" class="btn button mar">Delete</button>
						<button id="updateBut" onClick="saveOnly()" class="btn button gre">Save</button>
						<button id="insertBut" onClick="saveAndClose()" class="btn button navy">Save & Close</button>								
						<!-- <button id="cancelBut" onClick="clearForm()" class="buttonProducts buttonProductsGrayBack">Cancel</button> -->
							    
						
						</div>
						</div>
						</div>
						</div>
						</div>
						</section>
						<a data-open="shareModal" class="sharepop" href="#" aria-controls="shareModal" id="fvn6y5-reveal" aria-haspopup="true" tabindex="0">Share</a>
           <div id="shareModal" class="reveal-modal reveal login sharepopup" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
         <!--  <div id="productSocialMedia">
			<input type="checkbox" name="facebookCheck" id="facebookCheck" value="facebook">&nbsp;
			<a class="social-icon fb" href="#"><i class="fa fa-facebook"></i>Share on Facebook</a>
			
			<input type="checkbox" name="twitterCheck" id="twitterCheck" value="twitter">&nbsp;
			<a class="social-icon twit" href="#"><i class="fa fa-twitter"></i></i>Share on Twitter</a>

						    
						</div> -->
						<form id="login" action="login" method="post">
    	<a aria-label=""><img class="sm-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_125pxH.png" /></a>
        <a class="close-button" data-close aria-label="Close reveal" >&#215;</a>
        <!-- header -->
        <div class="row centered collapse">
			<div class="small-10 small-centered medium-8 columns  text-center">
            	<h3><i class="fa fa-bullhorn"></i>Share Your Listing</h3>
                <hr>
                 <h5> Choose what social networks you whould like to share your listing.</h5>
			</div>
		</div>
        <div class="row">
        	<div class="small-12 small-centered medium-12 medium-uncentered columns text-center">
                                       <input type="checkbox" name="facebookCheck" id="facebookCheck" value="facebook">
                                        <a rel="nofollow" href="<?php echo site_url( 'wp-login.php', 'https' ); ?>?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Facebook&amp;redirect_to=<?php $redirect_url = site_url( 'my-account', 'https' ); echo urlencode($redirect_url); ?>" title="Connect with Facebook" class="button icon facebook" data-provider="Facebook">
                                            <span><?php _e('Facebook'); ?></span>
                                        </a>
                                    <input type="checkbox" name="facebookCheck" id="facebookCheck" value="facebook">
                                        <a rel="nofollow" href="<?php echo site_url( 'wp-login.php', 'https' ); ?>?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Twitter&amp;redirect_to=<?php $redirect_url = site_url( 'my-account', 'https' ); echo urlencode($redirect_url); ?>" title="Connect with Twitter" class="button icon twitter" data-provider="Twitter">
                                            <span><?php _e('Twitter'); ?></span>
                                        </a>
                                    <input type="checkbox" name="facebookCheck" id="facebookCheck" value="facebook">
                                        <a rel="nofollow" href="#" title="Connect with Google+" class="button icon google-plus" data-provider="Google">
                                            <span><?php _e('Google'); ?></span>
                                        </a>
                                    </div>

                </div>
                                <!-- Cancel -->
                                <div class="row centered collapse">

                                    <div class="small-10 small-centered medium-7 columns text-center">
                                        <a class="button">Share</a>
                                    </div>

                                </div>
                            </form>
           </div>
    				</div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
<!-- Confirmation dialog -->
<div id="dialog-confirm" title="Confirmation">
  <p>
    <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
    Product Record will be deleted, Do you want to proceed?
  </p>
</div>

<?php include 'jqGridEnqueue.php'; ?>
<body>
<script type="text/javascript">
var ajax_url = '<?php echo admin_url( 'admin-ajax.php', 'relative' ); ?>';

/*//share on facebook
function shareFacebookWindow(guid){
    //slug is used to serach product
    //var pageurl = "<?php echo get_site_url() . '/listings/'?>"+slug;
    var url = 'https://www.facebook.com/sharer/sharer.php?u='+guid;//"<?php echo get_site_url();?>/create_listing?productId="+row_id;
    window.open(url);
} 

//share on twitter
function shareTwitterWindow(guid){
	//slug is used to serach product
    //var pageurl = "<?php echo get_site_url() . '/listings/'?>"+slug;
    var url = 'https://twitter.com/intent/tweet?url='+guid;
    window.open(url);
}*/

//logic to save and close functionality
function saveAndClose()
{
 	$("#redirect").val("Y");
 	if( $("#productId").val() != "0" )
 	{
 		updateProduct(true);
 	}else{
 		insertProduct(true);
 	}
}

//logic to save only
function saveOnly()
{
 	$("#redirect").val("N");
 	if( $("#productId").val() != "0" )
 	{
 		updateProduct(false);
 	}else{
 		insertProduct(false);
 	}
}

//request to insert product
function insertProduct(close)
{
	var allCategoryVals = [];
	$('#categoryDiv :checked').each(function() {
       allCategoryVals.push($(this).val());
     });

	if( $("#listingTitle").val() == "" ||
		$("#listingDescription").val() == "" ||
		$("#listingDate").val() == "" ||
		$("#listingPriceRange").val() == "" || allCategoryVals.length == 0)
	{
		displayErrorMessage("#errorDiv","#errorMsg" ,"Fields with * are mandatory, Please complete information");
	}
	else
	{
		if(allCategoryVals.length > 2){
			displayErrorMessage("#errorDiv","#errorMsg" ,"No more than two categories are allowed, please verify selected categories");
		}
		else
		{
			var data = {
		        action: 'create_listings_request',
		        param: 'insert_product',
		        post_title: $("#listingTitle").val(),
		        post_content: $("#listingDescription").val(),
		        post_date: $("#listingDate").val(),
		        quantity: $("#listingQty").val(),
		        price: $("#listingPriceRange").val(),
		        category: allCategoryVals.join(','),
		        attach_id: $("#attachId").val(),
		        facebook_check: $('#facebookCheck').is(":checked"),
		        twitter_check:  $('#twitterCheck').is(":checked"),
		        instagram_check: $('#instagramCheck').is(":checked")
		    };

		    //blocking screen
		    block_screen();
		  	$.ajax({                            
		        url: ajax_url,  
		        type: "POST",
		        data:data, 
		        action:'create_listings_request',
		        dataType: 'json', 
		        success: function(data){ 
		            displayErrorMessage("#susccessDiv","#successMsg" ,"Product saved successfully");
		            $("#productId").val(data.data);
		            $("#post_id").val(data.data);
		    		//share content on facebook and twitter
		    		if($('#facebookCheck').is(":checked") == true){shareFacebookWindow(data.guid);}
					if($('#twitterCheck').is(":checked") == true){shareTwitterWindow(data.guid);}
					if($("#image_upload").val() != ""){ 
						$("#image_upload").submit(); 
					}

					if(close == true && $("#image_upload").val() == ""){
						setTimeout(function(){
				  			window.location.replace("<?php echo get_site_url();?>/promoted-listing-dashboard/");
						}, 2000);					
					}else if( $("#image_upload").val() == "" ){
						unblock_screen();
					}
		        },
		        error: function(data) {
		            console.log(data);            
		        }
		    });
            $('.sharepop').trigger('click');
		}//else
	}
}

function updateProduct(close){
	var allCategoryVals = [];
	$('#categoryDiv :checked').each(function() {
       allCategoryVals.push($(this).val());
     });

	if( $("#listingTitle").val() == "" ||
		$("#listingDescription").val() == "" ||
		$("#listingDate").val() == "" ||
		$("#listingPriceRange").val() == "" || allCategoryVals.length == 0 )
	{
		displayErrorMessage("#errorDiv","#errorMsg" ,"Fields with * are mandatory, Please complete information");
	}
	else
	{
		if(allCategoryVals.length > 2){
			displayErrorMessage("#errorDiv","#errorMsg" ,"No more than two categories are allowed, please verify selected categories");
		}
		else
		{
			
			var data = {
		        action: 'create_listings_request',
		        param: 'update_product',
		        post_title: $("#listingTitle").val(),
		        post_content: $("#listingDescription").val(),
		        post_date: $("#listingDate").val(),
		        quantity: $("#listingQty").val(),
		        price: $("#listingPriceRange").val(),
		        category: allCategoryVals.join(',') ,
		        attach_id: $("#attachId").val(),
		        facebook_check: $('#facebookCheck').is(":checked"),
		        twitter_check:  $('#twitterCheck').is(":checked"),
		        instagram_check: $('#instagramCheck').is(":checked"),
		        productId: $("#productId").val()
		    };
	    
	    	//blocking screen
		    block_screen();

		    $.ajax({                            
		        url: ajax_url,  
		        type: "POST",
		        data:data, 
		        action:'create_listings_request',
		        dataType: 'json',       
		        success: function(data){ 
		            displayErrorMessage("#susccessDiv","#successMsg" ,"Changes saved successfully");
		            //share content on facebook and twitter
		    		if($('#facebookCheck').is(":checked") == true){shareFacebookWindow(data.guid);}
					if($('#twitterCheck').is(":checked") == true){shareTwitterWindow(data.guid);}
					//load image
					 if($("#image_upload").val() != ""){ 
						$("#image_upload").submit(); 
					}
					if(close == true && $("#image_upload").val() == ""){
						setTimeout(function(){
						  	window.location.replace("<?php echo get_site_url();?>/promoted-listing-dashboard/");
						}, 2000);
					}else if( $("#image_upload").val() == "" ){
						unblock_screen();
					}
		        },
		        error: function(data) {
		            console.log(data);            
		        }
		    });	
		}
		
	}
}

//Load promoted grid data
function loadCategoryData(productId){
    var dataPromoted = {
        action: 'create_listings_request',
        param: 'get_categories',
        productId: productId 
    };
    $.ajax({                            
        url: ajax_url,  
        type: "POST",
        data:dataPromoted, 
        action:'create_listings_request',
        dataType: 'json',       
        success: function(data){
        	$('.accordion').html(data.accordionContent);
        	$('.scriptsdata').html(data.accorscript);
        },
        error: function(data) {
            console.log(data);            
        }
    });
}

//request to load product information when page is call for update
function loadProductInformationUpdate(productId){
		var dataSend = {
        action: 'create_listings_request',
        param: 'load_product',
        productId:productId,
        type:"simple"
    };
    $("#productId").val(productId);
    $("#post_id").val(productId);

    $.ajax({                            
        url: ajax_url,  
        type: "POST",
        data:dataSend, 
        action:'create_listings_request',
        dataType: 'json',
        success: function(data){ 
        	$("#listingTitle").val(data.dataProduct[0].title);
			$("#listingDescription").val(data.dataProduct[0].description);
			$("#listingDate").val(data.dataProduct[0].listingDate);
			$("#listingQty").val(data.dataProduct[0].qty);
			$("#listingPriceRange").val(data.dataProduct[0].regularPrice);
			$('#listingPriceRange').priceFormat();
			$("#productSlug").val(data.dataProduct[0].slug);
			//product image
			$("#image_preview").html(data.htmlImage);

        },
        error: function(data) {
            console.log(data);            
        }
    });
}

//load products by catgeory
function loadMatchingItemsData()
{
	var allCategoryVals = [];
	$('#categoryDiv :checked').each(function() {
       allCategoryVals.push($(this).val());
     });

    var dataSend = {
        action: 'create_listings_request',
        param: 'load_matchingItems',
        CategoryIds:allCategoryVals.join(','),
        post_type: 'simple'
    };

    $.ajax({                            
        url: ajax_url,  
        type: "POST",
        data:dataSend, 
        action:'create_listings_request',
        dataType: 'json',       
        success: function(data){ 
            createMatchingItemsGrid_grid(data.data);
        },
        error: function(data) {
            console.log(data);            
        }
    });
}

//create matching items grid
function createMatchingItemsGrid_grid(jsonToDisplay){
     jQuery("#matchingItemsGrid").jqGrid({
		data: jsonToDisplay,
		datatype: "local",
		rowNum: 30,
		rowList: [10,20,30],
	   	colNames:['id','Product','Category','Listing Date','SoldOut'],
	   	colModel:[
	   		{name:'id',index:'id', width:10, sorttype:"int", key: true, hidden:true},
	   		{name:'ProductName',index:'ProductName', width:300},
			{name:'Category',index:'Category', width:100, editable:false},
			{name:'ListingDate',index:'ListingDate', width:100, formatter:'date', sortable:true, sorttype:'date',
            formatoptions: {srcformat:"m/d/Y ", newformat: 'ShortDate' }, editable:false,edittype:"text",
            editoptions: {size: 10, maxlengh: 100,dataInit : function (elem) {
                    //$(elem).datepicker();
            }}},
            {name: "SoldOut", width: 50, align: "center",
            formatter: "checkbox", formatoptions: { disabled: false},
            edittype: "checkbox", editoptions: {value: "Yes:No", defaultValue: "Yes"},
            stype: "select", searchoptions: { sopt: ["eq", "ne"], 
                value: ":Any;true:Yes;false:No" } }
	   	],
	   	pager: "#pmatchingItemsGrid",
	   	viewrecords: true,
	   	height: 250,
        width: 700,
	   	sortname: 'ProductName',
	   	multiselect: true,
	   	grouping:true,
	   	groupingView : {
	   		groupField : ['Category'],
	   		groupColumnShow : [false],
	   		groupCollapse : true,
	   	},
	   	caption: "Matching Items"
	});
}

//delete listing
function deleteListingData(){
	$( "#dialog-confirm" ).dialog({
      resizable: false,
      height:280,
      width:400,
      modal: true,
      buttons: {
        "Ok": function() {
          $( this ).dialog( "close" );
          	var productId = $("#productId").val();
		    var dataSend = {
		        action: 'create_listings_request',
		        param: 'delete_record',
		        id:productId
		    };

		    $.ajax({                            
		        url: ajax_url,  
		        type: "POST",
		        data:dataSend, 
		        action:'create_listings_request',
		        dataType: 'json',       
		        success: function(data){ 
		        	clearForm();
		        	 displayErrorMessage("#susccessDiv","#successMsg" ,"Product deleted successfully");
		        },
		        error: function(data) {
		            console.log(data);            
		        }
		    });
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }
    });
}

//Clear form fields
function clearForm(){
	$("#listingTitle").val("");
	$("#listingDescription").val("");
	$("#listingDate").val("");
	$("#listingQty").val("");
	$("#listingPriceRange").val("");
	jQuery("#categoriesGrid").jqGrid('setGridParam',{}).jqGrid('hideCol',"somecol").trigger("reloadGrid");
}

//Load user account information
function loadAccountInfo(){
	var dataSend = {
		        action: 'create_listings_request',
		        param: 'load_accountInfo'
		    };

		    $.ajax({                            
		        url: ajax_url,  
		        type: "POST",
		        data:dataSend, 
		        action:'create_listings_request',
		        dataType: 'json',       
		        success: function(data){ 
		        	$( "#companyName" ).html(" " + data.vendorName);
		        	$( "#userName" ).html(" " + data.userName);
		        	$( "#credits" ).html(" " + data.credits);
		        	$( "#companyNames" ).html(" " + data.vendorName);
		        	$( "#userNames" ).html(" " + data.userName);
		        	$( "#creditss" ).html(" " + data.credits);
		        },
		        error: function(data) {
		            console.log(data);            
		        }
		    });
}

//Clicks the upload file input element 
function chooseImage() {
  $("#eventImage").click();
}

//preview image before upload
function displayImage(input) {
	//$('#loadingmessage').show();
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewing').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
    //$('#loadingmessage').hide();
}

//create link between product and file
function linkPostWithFile(){
	var data = {
		      action: 'create_listings_request',
		      param: 'link_file_with_post',
		      attach_id: $("#attachId").val(),
		      post_id: $("#productId").val()
	};
	    
	$.ajax({                            
		url: ajax_url,  
		type: "POST",
		data:data, 
		action:'create_listings_request',
		dataType: 'json',       
		success: function(data){ 
			$("#image_upload").val("");
			unblock_screen();
			if($("#redirect").val() == "Y")
			{
				setTimeout(function(){
				  	window.location.replace("<?php echo get_site_url();?>/promoted-listing-dashboard/");
				}, 2000);
			}
		},
		error: function(data) {
		    console.log(data);            
		}
	});	
}


$(document).ready(function () {
    //Initialized tabs section
    var productId = "<?php  if( isset($_REQUEST["productId"])){echo $_REQUEST["productId"];} else {echo "0";}?>";
    $("#dialog-confirm").hide();
    $( "#tabs" ).tabs();
    $(".accordion div").addClass("active");
    
	//$('#listingPriceRange').priceFormat();
	$('#listingPriceRange').priceFormat({
	    centsLimit: 2,
            thousandsSeparator: ''
	});
    
    
    loadMatchingItemsData();
    loadAccountInfo();

    if(productId != null && productId != "0"){
    	loadProductInformationUpdate(productId);
    	loadCategoryData(productId);
    }else{
    	//$("#insertBut").prop( "disabled", false);
    	//$("#updateBut").prop( "disabled", true);
    	loadCategoryData("0");
    }

    //date picker 
    $( "#listingDate" ).datepicker({
        showOn: "button",
        buttonImage: "<?php echo get_template_directory_uri(); ?>/jquery-ui/images/calendar_icon.png",
        buttonImageOnly: true,
        inline: true
    });

    $.datepicker.setDefaults({
	    beforeShow: function ( input, inst ) {
	        setTimeout(function(){
	            inst.dpDiv.css({
	                zIndex: 10000
	            });
	        })
	    }
	});

    // START: For the upload image feature
    $("#featured_upload").on('submit',(function(e) {
		e.preventDefault();
		if($("#productId").val() == "0")
		{
			displayErrorMessage("#warningDiv","#warningMsg" ,"Please save information before upload image");
		}
		else{
			if( $("#image_upload").val() != "" )
			{
				//$('#loadingmessage').show();
				$.ajax({
					url: ajax_url, // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: new FormData($(this)[0]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					processData:false,        // To send DOMDocument or non processed data file it is set to false
					success: function(data)   // A function to be called if request succeeds
					{
						$("#attachId").val(data.data);
						linkPostWithFile();
						//$('#loadingmessage').hide();
					},
			        error: function(data) {
			            console.log(data);            
			        }
				});
			}
		}
	}));
	// Function to preview image after validation
	$(function() {
		$("#image_upload").change(function() {
				displayImage(this);
		});
	});
	function imageIsLoaded(e) {
		$("#image_upload").css("color","green");
		$('#image_preview').css("display", "block");
		$('#previewing').attr('src', e.target.result);
		$('#previewing').attr('width', '250px');
		$('#previewing').attr('height', '230px');
	};
	// END: For the upload image feature
	   $( "#clearrall" ).click(function() {
       $('.listing-wrapper').find('input:text').val('');
       $('.listing-detail').find('input:text').val('');
       $('#listingDescription').val('');
       $('#previewing').removeAttr('src');
       $('input:checkbox').removeAttr('checked'); 
         });
});

</script>
</body>