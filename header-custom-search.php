<?php 
/**
 *
 * Template Name: Custom Product search
 *
 */
?>
<!DOCTYPE html>
<html>
<head>
<?php wp_head(); 
if ( !defined( 'YITH_WCAS' ) ) { exit; } // Exit if accessed directly
wp_enqueue_script('yith_wcas_frontend' );

?>
<style type="text/css">
input#yith-s {
   background: #f5f7f6  none repeat scroll 0 0;
   border: medium none;
   color: #b8b8b8 ;
   display: inline-block;
   font-size: 18px;
   height: 52px;
   width: 70%;
   max-width: 550px;
   border-bottom: 6px solid #e0e0e0 ;
   border-radius: 2px;
}

input#yith-searchsubmit{
      background: #37a3bf  url("../images/search-icon.png") no-repeat scroll 15px center!important;
   border: medium none;
   color: #ffffff ;
  font-size: 18px; 
   float: right;
   padding: 12px 30px 12px 19px;
   border-bottom: 6px solid #267e97 ;
   border-radius: 2px;
   text-align: right;
   width: 106px;
   height: 52px;
   margin-right: 0;
}


.yith-ajaxsearchform-container {
   float: right;
   margin: 0 !important;
   padding: 0;
   width: 100% !important;
   overflow: hidden;
   clear: both;
   text-align: right;
}
.autocomplete-suggestions {
    right: 105px;
    top: 53PX;
    height: 200px;
    overflow: auto;
}
</style>
<div class="yith-ajaxsearchform-container">
  <form  target = '_blank'role="search" method="get" id="yith-ajaxsearchform" action="<?php echo esc_url( home_url( '/'  ) ) ?>">
<div>
 <input type="search"
                   value="<?php echo get_search_query() ?>"
                   name="s"
                   id="yith-s"
                   class="yith-s search"
                   placeholder="What are you looking for?"
                   data-loader-icon="<?php echo str_replace( '"', '', apply_filters('yith_wcas_ajax_search_icon', '') ) ?>"
                   data-min-chars="<?php echo get_option('yith_wcas_min_chars'); ?>" />

            <input type="submit" class="button postfix" id="yith-searchsubmit" value="<?php echo get_option('yith_wcas_search_submit_label') ?>" />
            <input type="hidden" name="post_type" value="product" />
            <?php if ( defined( 'ICL_LANGUAGE_CODE' ) ): ?>
                <input type="hidden" name="lang" value="<?php echo( ICL_LANGUAGE_CODE ); ?>" />
            <?php endif ?>
        </div>
    </form>
</div>
 <?php //echo do_shortcode('[yith_woocommerce_ajax_search]'); ?>
<?php 
 wp_footer(); 
?>