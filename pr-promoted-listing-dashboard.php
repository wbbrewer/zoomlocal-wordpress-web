<?php
/*<?php
/*
Template Name: pr-promoted-listing-dashboard

*/
?>

<?php get_header(); ?>
<head>
    <!--Special configuration to css -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php  bloginfo("template_directory"); ?>/jquery-ui/specialAdjustments.css" />
</head>
        <section class='dashnotification'>
            <div class="row">
            <div class="ui-widget">
                <div class="ui-corner-all" style="margin-top: 5px;" id="UserMessage">
                    <p>
                    <strong>Listings Remaining Until Next Pay Period : <span id="creditsValue"></span></strong><a href="#"> need more?</a></p>
                </div>
                </div>
            </div>
        </section>
        <section class="promoted-listing">

            <div id="content">
                <div id="inner-content">

                    <div id="main" class="large-50 medium-50 columns" role="main">
                        <?php //get_template_part( 'parts/loop', 'page' ); ?>
                        <!-- Credits message -->

                        <section class="dashboard row" >
                        <div class="leftmenu mobile-pmenu show-for-small-only">
                           <?php  $current_user = wp_get_current_user(); ?>
                             <span class="displayLabel">Welcome:</span><span class="displayLabelInfo" id="loginname"> <?php echo $current_user->user_firstname; ?></span><br><!--Comapny Name-->
                           <ul class="user-info">
                            <li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a></li>
                          <?php $user_id = $current_user->ID;
                                        $key = 'yith_product_vendor_owner';
                                        $single = true;
                                        $user_last = get_user_meta( $user_id, $key, $single );
                                        $term = get_term($user_last,'yith_shop_vendor'); 
                                        $vnameslug = $term->slug ;?>
                           <li><a href="<?php echo  get_bloginfo( 'url' ).'/vendor/'.$vnameslug; ?>">My store</a></li>
                           <!--<li><a href="">Messages</a></li>
                           <li><a href="">Billing</a></li>-->
                           <li><a href="<?php echo wp_logout_url(home_url()); ?>">Sign Out</a></li>
                           </ul>
                        </div>
                        <div class="small-12 medium-8 left-side columns">
                        <a class='button lbutton'href="<?php echo get_page_link(1789); ?>"><i class="fa fa-plus-circle"></i> Create Listing</a>
                            <h4>Recent Listings</h4>
                            <!-- Error messages -->
                            <div class="ui-widget" id="errorDiv">
                                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                                    <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                                    <strong>Alert:</strong> <span id="errMsg"></span></p>
                                </div>
                            </div>
                            <label>Listings for Week of <span> (Promoted Listings)</span></label>
                                <table id="promotedListings"></table>
                                <div id="ppromotedListings" ><br><br></div>
            
                             <label>Listings for Week of <span> (Non Promoted Listings)</span></label>
                                <table id="NopromotedListings"></table>
                                <div id="NoppromotedListings" ><br><br></div>
                        </div>
                        <div class="small-12 medium-4 right-side columns">
                           <div class="leftmenu hide-for-small-only">
                           <?php  $current_user = wp_get_current_user(); ?>
                             <span class="displayLabel">Welcome:</span><span class="displayLabelInfo" id="loginname"> <?php echo $current_user->user_firstname; ?></span><br><!--Comapny Name-->
                           <ul class="user-info">
                           <li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a></li>
                          <?php $user_id = $current_user->ID;
                                        $key = 'yith_product_vendor_owner';
                                        $single = true;
                                        $user_last = get_user_meta( $user_id, $key, $single );
                                        $term = get_term($user_last,'yith_shop_vendor'); 
                                        $vnameslug = $term->slug ;?>
                           <li><a href="<?php echo  get_bloginfo( 'url' ).'/vendor/'.$vnameslug; ?>">My store</a></li>
                           <!--<li><a href="">Messages</a></li>
                           <li><a href="">Billing</a></li>-->
                           <li><a href="<?php echo wp_logout_url(home_url()); ?>">Sign Out</a></li>
                           </ul>
                             </div>

                            <div id="datepicker"></div>
                        </div>
                        </section>
                        
                    </div> <!-- end #main -->
                </div> <!-- end #inner-content -->
            </div> <!-- end #content -->
        </section>
<?php get_footer(); ?>
<!-- Confirmation dialog -->
<div id="dialog-confirm2" title="Confirmation" style="display:none">
  <p>
    <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
    Select new listing date (Leave blank for current date). <br> One credit will be used, Do you want to proceed?
    <div id="timePickerSpecial">
        <label class="displayLabel" >New Listing Date</label>
        <input type="text" id="listingDate_new" style="width:40%; ">
    </div>
  </p>
</div>
<div id="dialog-confirm" title="Confirmation" style="display:none">
  <p>
    <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
    Select new listing date (Leave blank for current date). <br> One credit will be used, Do you want to proceed?
    <div id="timePickerSpecial_1">
        <label class="displayLabel" >New Listing Date</label>
        <input type="text" id="listingDate_new_1" style="width:40%; ">
    </div>
  </p>
</div>
<?php include 'jqGridEnqueue.php'; ?>
<body>
<script type="text/javascript">
//The variable ajax_url should be the URL of the admin-ajax.php file\
var ajax_url = '<?php echo admin_url( 'admin-ajax.php', 'relative' ); ?>';

//create promoted listings grid
function createPromoted_grid(jsonToDisplay){
    var pageWidth = $("#promotedListings").parent().width() - 100;
    $("#promotedListings").jqGrid({
        data:jsonToDisplay, //insert data from the data object we created above
        datatype: 'local',
        colNames:['Actions','id','Product', 'Category','Listing Date','Sold Out'],
        colModel:[
            {name:'act',index:'act', width:(pageWidth*(10/100)),sortable:false},
            {name:'id', index:'id', key: true, width:(pageWidth*(6/100)), editable:true, editoptions:{readonly:true}, 
            sorttype:'int',  hidden:true, editrules:{
                                                    required:true, 
                                                    edithidden:true
                                                    },
            },
            {name:'ProductName',index:'ProductName asc', width:(pageWidth*(45.5/100)), editable:true},
            {name:'Category',index:'category asc', width:(pageWidth*(20/100)), editable:true},
            {name:'ListingDate',index:'ListingDate', align: "center", width:(pageWidth*(24/100)), formatter:'date', sortable:true, sorttype:'date',
            formatoptions: {srcformat:"m/d/Y ", newformat: 'ShortDate' }, editable:true,edittype:"text",
            editoptions: {size: 10, maxlengh: 10,dataInit : function (elem) {
                    //$(elem).datepicker();
            }}},
            {name: "SoldOut", width:(pageWidth*(14/100)), align: "center",
            formatter: "checkbox", formatoptions: { disabled: false},
            edittype: "checkbox", editoptions: {value: "Yes:No", defaultValue: "Yes"},
            stype: "select", searchoptions: { sopt: ["eq", "ne"], 
                value: ":Any;true:Yes;false:No" } }
            //{name:'SoldOut',index:'SouldOut asc, ListingDate', width:50, editable:true}
        ],
        height: 150,
        width: null,
        shrinkToFit: false,
        rowNum:10,
        rowTotal: 50,
        rowList:[10,20,30],
        pager: '#ppromotedListings',
        sortname: 'ListingDate',
        sortorder: "desc",
        loadonce: true,
        viewrecords: true,
        editurl: 'server.php', // this is dummy existing url
        caption:"Promoted Listings",
        multiselect: false,
        loadComplete: function() {
            this.grid.hDiv.scrollLeft = this.grid.bDiv.scrollLeft;
        },
        gridComplete: function(){
            var ids = jQuery("#promotedListings").jqGrid('getDataIDs');
            for(var i=0;i < ids.length;i++){
                var cl = ids[i];
                be = "<input class='gridSearchBut' type='button' onclick=\"openEditWindow('"+cl+"')\" />"; 
                ce = "<input class='gridRefreshBut' type='button' onclick=\"updateListingDate('"+cl+"',1)\" />"; 
                se = "";//"<input class='gridCatBut' type='button' onclick=\"updateToPromoted('"+cl+"',1)\" />"; 
               //ce = "<input class='gridRefreshBut' type='button' onclick=\"jQuery('#promotedListings').restoreRow('"+cl+"');\" />"; 

                jQuery("#promotedListings").jqGrid('setRowData',ids[i],{act:be+se+ce});
            }   
        },
        beforeSelectRow: function (rowid, e) 
        {
            var $self = $(this), selectedRowid = $self.jqGrid("getGridParam", "selrow");

            if (selectedRowid === rowid) {
                $self.jqGrid("resetSelection");
            } else {
                $self.jqGrid("setSelection", rowid, true, e);
            }

            return false; // don't process the standard selection
        }
    });
    $('#promotedListings').navGrid('#ppromotedListings',
        // the buttons to appear on the toolbar of the grid
        { edit: false, add: false, del: false, search: false, refresh: false, 
            view: false, position: "left", cloneToTop: false });
    
    $("#promotedListings").jqGrid('navGrid','#ppromotedListings',{ });
}

//create No promoted listings grid
function createNoPromoted_grid(jsonToDisplay){
    var pageWidth = $("#NopromotedListings").parent().width() - 100;
    $("#NopromotedListings").jqGrid({
        data:jsonToDisplay, //insert data from the data object we created above
        datatype: 'local',
        colNames:['Actions','id','Product', 'Category','Listing Date','Sold Out'],
        colModel:[
            {name:'act',index:'act', width:(pageWidth*(10/100)),sortable:false},
            {name:'id', index:'id', key: true, width:(pageWidth*(6/100)), editable:true, editoptions:{readonly:true}, sorttype:'int',  hidden:true, 
                editrules:
                {
                required:true, 
                edithidden:true
                },
            },
            {name:'ProductName',index:'ProductName asc, ListingDate', width:(pageWidth*(45.5/100)), editable:true},
            {name:'Category',index:'category asc, ListingDate', width:(pageWidth*(20/100)), editable:true},
            {name:'ListingDate',index:'ListingDate', align: "center", width:(pageWidth*(24/100)), formatter:'date', sortable:true, sorttype:'date',
            formatoptions: {srcformat:"m/d/Y ", newformat: 'ShortDate'}, editable:true, edittype:"text", 
            editoptions: {size: 10, maxlengh: 10,dataInit : function (elem) {
                    //$(elem).datepicker();
            }}},            
            {name: "SoldOut", width: (pageWidth*(14/100)), align: "center",
            formatter: "checkbox", formatoptions: { disabled: false},
            edittype: "checkbox", editoptions: {value: "Yes:No", defaultValue: "Yes"},
            stype: "select", searchoptions: { sopt: ["eq", "ne"], 
                value: ":Any;true:Yes;false:No" } }
        ],
        height: 150,
        width: null,
        shrinkToFit: false,
        rowNum:10,
        rowTotal: 50,
        rowList:[10,20,30],
        pager: '#NoppromotedListings',
        loadonce: true,
        viewrecords: true,
        sortname: 'ListingDate',
        sortorder: "desc",
        editurl: 'server.php', // this is dummy existing url
        caption:"Listings Not Promoted",
        multiselect: false,
        gridComplete: function(){
            var ids = $("#NopromotedListings").jqGrid('getDataIDs');
            for(var i=0;i < ids.length;i++){
                var cl = ids[i];
                be = "<input class='gridSearchBut' type='button' onclick=\"openEditWindow('"+cl+"')\"  />"; 
                se = "<input class='gridCatBut' type='button' onclick=\"updateToPromoted('"+cl+"',1)\" />"; 
               //ce = "<input class='gridRefreshBut' type='button' onclick=\"jQuery('#promotedListings').restoreRow('"+cl+"');\" />"; 
                //ce = "<input class='gridRefreshBut' type='button' onclick=\"updateListingDate('"+cl+"',2)\" />"; 

                jQuery("#NopromotedListings").jqGrid('setRowData',ids[i],{act:be+se});
            }   
        },
        beforeSelectRow: function (rowid, e) 
        {
            var $self = $(this), selectedRowid = $self.jqGrid("getGridParam", "selrow");

            if (selectedRowid === rowid) {
                $self.jqGrid("resetSelection");
            } else {
                $self.jqGrid("setSelection", rowid, true, e);
            }

            return false; // don't process the standard selection
        }
    });
    $('#NopromotedListings').navGrid('#NoppromotedListings',
        // the buttons to appear on the toolbar of the grid
        { edit: false, add: false, del: false, search: false, refresh: false, 
            view: false, position: "left", cloneToTop: false });
    $("#NopromotedListings").jqGrid('navGrid','#NoppromotedListings',{});
}

//reload promoted grid data  after any modification, refresh functionality
function reloadPromotedData(){
    var dataPromoted = {
        action: 'get_data',
        param: 'promoted',
        type: 'simple'
    };
    $.ajax({                            
        url: ajax_url,  
        type: "POST",
        data:dataPromoted, 
        action:'get_data',
        dataType: 'json',       
        success: function(data){ 
            $('#promotedListings').jqGrid('clearGridData');
            $('#promotedListings').jqGrid('setGridParam', {data: data.data, page: 1})
            $('#promotedListings').trigger('reloadGrid');
        },
        error: function() {
            console.log("Error");            
        }
    });
}

//reload no promoted grid data after any modification, refresh functionalit
function reloadNoPromotedData(){
    var dataNoPromoted = {
        action: 'get_data',
        param: 'NoPromoted',
        type: 'simple'
    };
    $.ajax({                            
        url: ajax_url,  
        type: "POST",
        data:dataNoPromoted, 
        action:'get_data',
        dataType: 'json',       
        success: function(data){ 
            $('#NopromotedListings').jqGrid('clearGridData');
            $('#NopromotedListings').jqGrid('setGridParam', {data: data.data, page: 1});
            $('#NopromotedListings').trigger('reloadGrid');
        },
        error: function() {
            console.log("Error");            
        }
    });
} 

//load promoted grid data
function loadPromotedData(){
    var dataPromoted = {
        action: 'get_data',
        param: 'promoted',
        type: 'simple'
    };
    $.ajax({                            
        url: ajax_url,  
        type: "POST",
        data:dataPromoted, 
        action:'get_data',
        dataType: 'json',       
        success: function(data){ 
            createPromoted_grid(data.data);
        },
        error: function(data) {
            console.log(data);            
        }
    });
}
//load no pormoted grid data
function loadNoPromotedData(){
     var dataNoPromoted = {
        action: 'get_data',
        param: 'NoPromoted',
        type: 'simple'
    };
    $.ajax({                            
        url: ajax_url,  
        type: "POST",
        data:dataNoPromoted, 
        action:'get_data',
        dataType: 'json',       
        success: function(data){ 
            createNoPromoted_grid(data.data);
        },
        error: function(data) {
            console.log(data);            
        }
    });
}

//generate request to update listing date when refresh button is clicked
function updateListingDate(row_id,type){
    var status;
    if(type==1){status='publish';}else{status='draft';}
    if( $('#creditsValue').text() == 0)
    {
        displayErrorMessage("#errorDiv","#errMsg" ,"No credits available");
    }
    else
    {
        $( "#dialog-confirm2" ).dialog({
            resizable: false,
            height:380,
            width:500,
            modal: true,
            buttons: 
            {
                "Ok": function() {
                    $( this ).dialog( "close" );
                    var data = {
                            action: 'get_data',
                            param: 'updateListingDate',
                            id:row_id,
                            post_date:$("#listingDate_new").val(),
                            status:status,
                            type: 'product'
                        }

                    $.ajax({                            
                        url: ajax_url,  
                        type: "POST",
                        data:data, 
                        action:'get_data',
                        dataType: 'json',       
                        success: function(data){
                            if(type==1){reloadPromotedData();}else{reloadNoPromotedData();}
                            $( "#creditsValue" ).html(data.totalCredits);
                        },
                        error: function(data) {
                            console.log(data);            
                        }
                    });
                },
                Cancel: function() { $( this ).dialog( "close" ); }
            }
        });
    }
}

function openEditWindow(row_id){
   /*product Id for selected row is get and send via url param when create listings page is called*/
   var url = "<?php echo get_site_url();?>/create-listing?productId="+row_id;
   window.location.replace(url);
}


//Generate request to update listings date and featured to yes when tag button is clicked
function updateToPromoted(row_id,type)
{
    var status;
    if(type==1){status='publish';}else{status='draft';}
    if( $('#creditsValue').text() == 0)
    {
        displayErrorMessage("#errorDiv","#errMsg" ,"No credits available");
    }
    else
    {
          $( "#dialog-confirm" ).dialog({
            resizable: true,
            height:380,
            width:500,
            modal: true,
            buttons: 
            {
                "Ok": function() 
                {
                    $( this ).dialog( "close" );

                    $.ajax({                            
                        url: ajax_url,  
                        type: "POST",
                        data:{
                            action: 'get_data',
                            param: 'updateToPromoted',
                            id:row_id,
                            status:status,
                            type: 'product',
                            post_date:$("#listingDate_new_1").val()
                        }, 
                        action:'get_data',
                        dataType: 'json',       
                        success: function(data){ 
                            reloadPromotedData();
                            reloadNoPromotedData();
                            $( "#creditsValue" ).html(data.totalCredits);
                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });
                },
                Cancel: function() { $( this ).dialog( "close" ); }
            }
        });  
    }
}

//load credits
function loadCreditsPerUser(){
    var dataNoPromoted = {
        action: 'get_data',
        param: 'DisplayCredits',
        type: 'product'
    };
    $.ajax({                            
        url: ajax_url,  
        type: "POST",
        data:dataNoPromoted, 
        action:'get_data',
        dataType: 'json',       
        success: function(data){ 
            $( "#creditsValue" ).html(data.data);
        },
        error: function(data) {
            console.log(data);            
        }
    });
}

//after a date is less than today's date product should be display in no promoted grids
function updateToNotPromoted(){
    var dataSend = {
            action: 'create_listings_request',
            param: 'update_to_not_promoted',
            post_type: 'product'
        };
        
    $.ajax({                            
        url: ajax_url,  
        type: "POST",
        data:dataSend, 
        action:'create_listings_request',
        dataType: 'json',       
        success: function(data){ 
            
        },
        error: function(data) {
            console.log(data);            
        }
    });
}

$(document).ready(function () {
    //initialized datepicker
    $( "#datepicker" ).datepicker({
        inline: true
    });
    $("#errorDiv").hide();

    //date picker refresh top grid
    $( "#listingDate_new" ).datepicker({
        showOn: "button",
        buttonImage: "<?php echo get_template_directory_uri(); ?>/jquery-ui/images/calendar_icon.png",
        buttonImageOnly: true,
        inline: true
    });

    //date picker tag button grid
    $( "#listingDate_new_1" ).datepicker({
        showOn: "button",
        buttonImage: "<?php echo get_template_directory_uri(); ?>/jquery-ui/images/calendar_icon.png",
        buttonImageOnly: true,
        inline: true
    });

    //if date is less that current date  products should be displayed in not promoted listings
    updateToNotPromoted();
    //document.getElementById("UserMessage").style.visibility = "hidden";
    loadPromotedData();//load promoted listings grid data
    loadNoPromotedData();//load no promoted listings grid data
    loadCreditsPerUser(); //load credit per user


    
});

            

 </script>
</body>