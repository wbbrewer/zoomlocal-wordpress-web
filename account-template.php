<?php
/*
Template Name: Vendor Account
*/
?>
<?php get_header(); ?>
			
			<div id="content">
			<section class='vendoraccount'>
			
				<div id="inner-content" class="row">
			
				    <div id="main" class="large-12 medium-12 small-centered columns" role="main">
					
					    	<?php get_template_part( 'parts/loop', 'page' ); ?>
					    					
    				</div> <!-- end #main -->
    
				    <?php get_sidebar(); ?>
				    
				</div> <!-- end #inner-content -->
				</section>
    
			</div> <!-- end #content -->

<?php get_footer(); ?>