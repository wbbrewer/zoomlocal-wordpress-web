<?php
/*
Template Name: Create Event Page
*/
?>

<?php
//need to make sure required plugins are enabled - move this to functions.php???
if ( ! class_exists( 'YITH_Vendors' ) ){
	exit;
}


//add conditional tags here and some checkers
global $current_user;
get_currentuserinfo();
//if its an edit post = make sure the user is the author of the post

$subscription_type = null;
$user_id           = get_current_user_id();
$access            = true;
$yith_shop = yith_get_vendor( get_current_user_id(), 'user' );
if($yith_shop->id===0){
	$access = false;
	$message = "Please create your vendor profile.";
}
if(isset($_GET['productId'])){
	$event_id = $_GET['productId'];
	$post = array(
		'post_type' => 'product',
		'author' => '69',
		'include' => '2464'
	);
	$post_array = get_posts($post);
	if(empty($post_array)){
		$access=false;
		$message="You done have access to this event. Please Contact Our Customer Service Team.";
	}
	var_dump($post_array);
	wp_reset_postdata();
}


//upload image handler
if (
	isset( $_POST['my_image_upload_nonce'] )
	&& wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' )
) {
	// The nonce was valid and the user has the capabilities, it is safe to continue.

	// These files need to be included as dependencies when on the front end.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );

	// Let WordPress handle the upload.
	// Remember, 'my_image_upload' is the name of our file input in our form above.
	/*$attachment_id = media_handle_upload( 'my_image_upload', 0 );

	if ( is_wp_error( $attachment_id ) ) {
		echo "There was an error uploading the image.";
	} else {
		echo "The image was uploaded successfully!";
		$yith_shop->header_image = $attachment_id;
	}*/

} else {

	// The security check failed, maybe show the user an error.
}
$title = (isset($_POST['event-title']))?sanitize_text_field($_POST['event-title']):false;
$description = (isset($_POST['event-description']))?sanitize_text_field($_POST['event-description']):false;
$street = (isset($_POST['event-address-street']))?sanitize_text_field($_POST['event-address-street']):false;
$city = (isset($_POST['event-address-city']))?sanitize_text_field($_POST['event-address-city']):false;
$state = (isset($_POST['event-address-state']))?sanitize_text_field($_POST['event-address-state']):false;
$zip = (isset($_POST['event-address-zip']))?sanitize_text_field($_POST['event-address-zip']):false;
$start_date = (isset($_POST['event-date-start']))?preg_replace("([^0-9-])", "", $_POST['event-date-start']):false;
$end_date = (isset($_POST['event-end-start']))?preg_replace("([^0-9-])", "", $_POST['event-date-start']):$start_date;
$map_lat = (isset($_POST['event-lat']))?sanitize_text_field($_POST['event-lat']):false;
$map_lng = (isset($_POST['event-lng']))?sanitize_text_field($_POST['event-lng']):false;
//validations
if($title && $description && $street && $city && $state && $zip && $start_date && $end_date && $map_lat && $map_lng){
	//process insert or update product here
	$post = array(
		'post_title' => $title,
		'post_content' => $description,
		'post_status' => 'publish',
		'post_type' => 'product'
	);
	if(isset($_POST['event-id']) && $_POST['event-id']<>""){
		$post['ID']=$event_id;
	}
	$post_id = wp_insert_post($post);
	//update meta if post is properly inserted
	if($post_id<>0){
		//set vendor
		wp_set_object_terms($post_id, $yith_shop->term_taxonomy_id, 'yith_shop_vendor', true );
		//update meta
		update_post_meta($post_id, '_rq_event_start_date', $start_date);
		update_post_meta($post_id, '_rq_event_end_date', $end_date);
		update_post_meta($post_id, '_rq_event_address_name', $street);
		update_post_meta($post_id, '_rq_event_region_name', $state);
		update_post_meta($post_id, '_rq_event_city_name', $city);
		update_post_meta($post_id, '_rq_event_zip_code', $zip);
	    update_post_meta($post_id, '_rq_event_lon_name', $map_lat);
	    update_post_meta($post_id, '_rq_event_lat_name', $map_lng);
		update_post_meta($post_id, '_rq_event_country_name', 'USA');
		wp_set_object_terms($post_id, 'event', 'product_type');

		$attachment_id = media_handle_upload( 'my_image_upload', 0 );
		if ( is_wp_error( $attachment_id ) ) {

		} else {
			echo "The image was uploaded successfully!";
			update_post_meta($post_id, '_thumbnail_id', $attachment_id);
		}
		//redirect back to event dashboard
		wp_redirect( home_url().'/promoted-event-dashboard/' );
		exit;
	}
	var_dump($post);
}else{
	$message = "Please Check the fields and try again.";
}
?>
<?php get_header(); ?>

<div id="content">

	<div id="inner-content" class="row">

		<div id="main" class="large-12 medium-12 columns" role="main">
			<?php
			if ( $access ) {
				//get_template_part( 'parts/create', 'event' );
				include(locate_template('parts/create-event.php'));

			} else {
				echo "<p>You don't have access to this page. If you already have a subscription and you are seeing this message, please contact us. </p>
					  <p><span>$message<span></p>";
			}
			?>

		</div>
		<!-- end #main -->

	</div>
	<!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
