<?php
/*
Template Name: pr-social-share-screen
*/
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content" class="row">
			
				    <div id="main" class="large-12 medium-12 columns" role="main">
						<?php get_template_part( 'parts/loop', 'page' ); ?>
						<div id="productContainerShare">
							
						</div>
					    					
    				</div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
<?php include 'jqGridEnqueue.php'; ?>
<body>
<script type="text/javascript">
function loadProductInformation(productId)
{
	var ajax_url = '<?php echo admin_url( 'admin-ajax.php', 'relative' ); ?>';
	var data = {
        action: 'share_info_request',
        post_id: productId
       
    };
  	$.ajax({                            
        url: ajax_url,  
        type: "POST",
        data:data, 
        action:'share_info_request',
        dataType: 'json',       
        success: function(data){ 
            console.log(data);
            $("#productContainerShare").html(data.html);

        },
        error: function(data) {
            console.log(data);            
        }
    });
}
$(document).ready(function () {
    //Initialized tabs section
    var productId = "<?php  echo  $_REQUEST["PXWTMYA1"]; ?>";
    if(productId != null && productId != ""){
    	loadProductInformation(productId);
    }
 });
 </script>
</body>
