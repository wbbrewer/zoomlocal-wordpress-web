<div class="footer">
<?php get_sidebar( 'cta' ); ?>
<?php if ( is_front_page() ) : ?>
    <?php get_sidebar( 'locations' ); ?>
<?php endif; ?>
<?php // get_sidebar( 'about' ); ?>
    <!-- Footer -->
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="small-12 columns">
                    <div class="row">
                        <div class="medium-2 columns">
                            <div class="row">
                            <?php $flogo = get_field('footer_logo', 'option'); ?>
                             <?php  if($flogo){ ?>
                   <a href="<?php echo bloginfo( 'url' ); ?>"><img src="<?php echo $flogo ?>"></a>
                             <?php  }?> 
                            </div>
                        </div>
                        <?php 
                            wp_nav_menu(array('menu'=>'footer-about-menu-p','container'=>'div','container_class'=>'medium-3 medium-offset-1 columns about','menu_id'=>'','menu_class'=>'','items_wrap'=>'<h1>About ZoomLocal</h1><ul id="%1$s" class="%2$s">%3$s</ul>'));
                        ?>
                        <?php 
                            wp_nav_menu(array('menu'=>'footer-browse-menu-p','container'=>'div','container_class'=>'medium-3 columns browse','menu_id'=>'','menu_class'=>'','items_wrap'=>'<h1>Browse</h1><ul id="%1$s" class="%2$s">%3$s</ul>'));
                        ?>
                        <?php 
                            wp_nav_menu(array('menu'=>'footer-help-menu-p','container'=>'div','container_class'=>'medium-3  columns help','menu_id'=>'','menu_class'=>'','items_wrap'=>'<h1>Help & Contact</h1><ul id="%1$s" class="%2$s">%3$s</ul>'));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="large-12 medium-12 columns">
                    <nav role="navigation">
                        <?php joints_footer_links(); ?>
                    </nav>
                </div>
                <div class="small-12 medium-8  large-8 columns bottom-footer-left">
                <?php $copyright = get_field('copyright', 'option'); ?>
                    <?php  if($copyright){ echo $copyright; }?> 
                </div>
                <div class="small-12 medium-4 large-4  bottom-footer-right  columns text-right">
                   <?php if( have_rows('social_icons', 'option') ): ?>

                  <p>

                <?php while( have_rows('social_icons', 'option') ): the_row(); ?>
               <a href="<?php the_sub_field('social_link'); ?>" class="bottom-footer-icons"><img src="<?php the_sub_field('social_image'); ?>" alt="/"></a>
                 

                 <?php endwhile; ?>

                    </p>

                <?php endif; ?>
            
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
</div>
<?php wp_footer(); ?>
<?php if(is_front_page() || is_woocommerce()): ?>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
    jQuery('.week-picker').datepicker( {
        // showWeek: true,
        onSelect: function(dateText, inst) {
            // $('#category-filter-list>li.date-li').remove();
            // selected_week = $.datepicker.iso8601Week(new Date(dateText));
            // var now = new Date(dateText);
            var d = new Date(dateText);
            d.setHours(0,0,0);
            d.setDate(d.getDate() + 4 - (d.getDay()||7));
            var yearStart = new Date(d.getFullYear(),0,1);
            selected_week = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
            selected_week--;
            // var start = new Date(now.getFullYear(), 0, 0);
            // var diff = now - start;
            // var oneDay = 1000 * 60 * 60 * 24;
            // var day = Math.floor(diff / oneDay);
            // selected_week = Math.ceil(day / 7);
            selected_year = dateText.split("/")[2]; 
            console.log(dateText);
            apply_search();
            return false;
        }
    });
</script>
<script>
jQuery(document).ready(function($){
    $(".ui-datepicker").hide();
    $(".week-picker").click(function(){
        $(".ui-datepicker").show();
    });
    $(".ui-state-default").click(function(){
        $(".ui-datepicker").hide();
    });
    $(document).ajaxComplete(function(){
        $(".ui-datepicker").hide();
    });
});
</script>
<?php endif; ?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<!--<script src="<?php //echo get_template_directory_uri(); ?>/js/vendor/jquery.min.js"></script>
<script src="<?php //echo get_template_directory_uri(); ?>/js/vendor/what-input.min.js"></script>
<script src="<?php //echo get_template_directory_uri(); ?>/js/foundation.min.js"></script>
<script src="<?php //echo get_template_directory_uri(); ?>/js/app.js"></script>-->
<!--<script src="<?php //echo get_template_directory_uri(); ?>/js/foundation.js"></script>-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php //echo get_template_directory_uri(); ?>/js/woco.accordion.js"></script>-->
<script>
jQuery(".accordion").accordion();
</script>	
<script type="text/javascript">
    jQuery(document).ready(function(){
        $(window).load(function(){
            $('.accordion-navigation > div').hide();
            $('.accordion-navigation a').click(function(){
                $( this ).toggleClass( "arrow-dir" );
                $(this).parent().children('div').toggle();
            });
        });
    }); 
</script>
<!--<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>-->
<!--<script src="<?php  bloginfo("template_directory"); ?>/assets/js/jquery.meanmenu.js"></script>-->
<script>
    jQuery(document).ready(function ($) {
        $('.nav-list').append("<div id='copied-nav-bar'></div>");
        $( ".loginmenu" ).clone().appendTo( "#copied-nav-bar" );
        //$('#copied-nav-bar').meanmenu();
        jQuery('.nav-content').meanmenu();
    });
</script>
<script>

</script>




</body>
</html> <!-- end page -->
