<!doctype html>
<html class="no-js"  <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Icons & Favicons -->
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
    <!--[if IE]>
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <![endif]-->
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png">
    <meta name="theme-color" content="#121212">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php wp_head(); ?>
    <!-- Drop Google Analytics here -->
    <!-- end analytics -->
    <!--Special configuration to css -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php  bloginfo("template_directory"); ?>/jquery-ui/specialAdjustments.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php  bloginfo("template_directory"); ?>/assets/css/meanmenu.css" />
</head>
<?php $b_class = is_user_logged_in() ? 'logged-in' : 'logged-out'; ?>
<body <?php body_class($b_class); ?> >
<?php if(!is_user_logged_in()){ 
            get_template_part('parts/login', 'form');
        } ?>
        <?php if(!is_user_logged_in()){ 
            get_template_part('parts/signup', 'form');
        } ?>
    <div class="main">
        <!-- Header of site -->
        <header class="global">
        
            <div class="container">
                <div class="small-12 medium-2 columns">
                <?php $logo = get_field('header_logo', 'option'); 
                       $mlogo = get_field('header_mobile_logo', 'option'); 
                ?>
                    <h1 class="logo"><a href="<?php echo get_bloginfo('url'); ?>"><img class="mainlogo hide-for-small-only"src="<?php echo $logo; ?>" alt="/"><img class='mobilelogo show-for-small-only' src="<?php echo $mlogo; ?>" alt="/"></a></h1>
                </div>
                <div class="small-12 medium-10 columns">
                    <div class="top-btn align-right hide-for-small-only loginmenu">
                    <?php
                   if ( is_user_logged_in() ) { ?>
                  <a class="sign-out" href="<?php echo wp_logout_url(home_url()); ?>">Sign Out</a>
                    <?php   } else { ?>
                    <!-- <a class="join" href="<?php //echo esc_url(home_url('/')); ?>my-account">Join</a> -->
                     <a data-open="myModals" class="join" href="#">Join</a>
                    <a data-open="myModal" class="login sign-out" href="#">Sign In</a>
                     <?php  }
                      ?>
                    </div>
                    <div class="top-search">
                    <!-- <form id="search-form" method="post" action="#">
                    <input class="search" id="search-box" type="text" placeholder="What are you looking for" placeholder="Search">
                    <button class="button postfix"> <i class="fa fa-search"></i> Search</button>
                    </form> -->
                    <?php
                    if ( !defined( 'YITH_WCAS' ) ) { exit; } // Exit if accessed directly
wp_enqueue_script('yith_wcas_frontend' );
?>
                    <div class="yith-ajaxsearchform-container">
  <form  role="search" method="get" id="yith-ajaxsearchform" action="#">
<div>
 <input type="search"
                   value="<?php echo get_search_query() ?>"
                   name="s"
                   id="yith-s"
                   class="yith-s search"
                   placeholder="What are you looking for"
                   data-loader-icon="<?php echo str_replace( '"', '', apply_filters('yith_wcas_ajax_search_icon', '') ) ?>"
                   data-min-chars="<?php echo get_option('yith_wcas_min_chars'); ?>" />

            <button class="button postfix"> <i class="fa fa-search"></i> Search</button>
            <input type="hidden" name="post_type" value="product" />
            <?php if ( defined( 'ICL_LANGUAGE_CODE' ) ): ?>
                <input type="hidden" name="lang" value="<?php echo( ICL_LANGUAGE_CODE ); ?>" />
            <?php endif ?>
        </div>
    </form>
</div>
                        <!--<input type="text" placeholder="What are you looking for" class="search">
                        <button><i class="fa fa-search"></i> Search</button>-->
                    </div>
                </div>
            </div>
            <?php get_template_part('parts/nav', 'top-offcanvas'); ?>
           
        </header>