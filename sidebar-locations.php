<div id="sidebar-locations" class="sidebar-locations" role="complementary">
    <div class="row">
        <div class="large-12-centered medium-12-centered columns">
            <?php if ( is_active_sidebar( 'sidebar-locations' ) ) : ?>

                    <?php dynamic_sidebar( 'sidebar-locations' ); ?>

            <?php endif; ?>
        </div>
    </div>
</div>