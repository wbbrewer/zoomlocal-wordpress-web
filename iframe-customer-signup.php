<?php
/*
Template Name: Iframe Customer Signup
*/

	?>
<?php 
if(isset($_POST['submit'])){
	if( ($_POST['emails'] == '') || ($_POST['passwords'] == '') || ($_POST['cpasswords'] == '') ){
		$error = "All fields are required";
	}else{
		if( $_POST['passwords'] != $_POST['cpasswords'] ){
			$error = "Both password should be same";
		}else{
			$user_name = $_POST['emails'];
			$user_pass = $_POST['passwords'];
			$user_email = $user_name;
			if ( username_exists( $user_name ) || email_exists($user_email) ) {
				$error = "An account already exists with this email or username.";
			}else{
			    $user_id = wp_create_user( $user_name, $user_pass, $user_email );
			     // Get current user object
			        $user = get_user_by( 'id', $user_id );

			        // Remove role
			        $user->remove_role( 'subscriber' );

			        // Add role
			        $user->add_role( 'customer' );

			        $success = "You have successfully created your account. Please login to continue.";
			}
		}
	}
}
?>

	<?php wp_head(); ?>
	
 <div class="main iframeform">
	<div  class="reveal-modal login">
	
	<form method="post" action="">
	
        <!-- header -->
       
               <div class="message">
		<p class="error"><?php echo isset($error) ? $error : ''; ?></p>
		<p class="success"><?php echo isset($success) ? $success : ''; ?></p>
	</div>
		<!-- email address -->
        <div class="row centered">
			<div class="small-10 small-centered medium-8 columns iframeemail">
            	<input id='username' type="email" name="emails" value=""  placeholder="email address">
            </div>
		</div>
		<!-- password -->
        <div class="row centered">
			<div class="small-10 small-centered medium-8 columns iframepassword">
            	<input id="password" type="password" name="passwords" value="" placeholder="password">
			</div>
		</div>
		<!-- password -->
        <div class="row centered">
			<div class="small-10 small-centered medium-8 columns iframepassword">
            	<input id="password" type="password" name="cpasswords" value="" placeholder="confirm password">
			</div>
		</div>
		<div class="row centered">
			<div class="small-10 small-centered medium-8 columns iframe-btn">
            	<input type="submit" name="submit" value="Join ZoomLocal" class="button loginbutton">
			</div>
		</div>
		 <!-- or sign in using -->
      <div class="row centered">
		<div class="small-10 small-centered medium-8 columns iframetext">
        	<p><span class="social-signin">or sign in using your account with</span></p>
        </div>

                                </div>
                                <!-- facebook, twitter, google -->
                                <div class="row">

                                    <div class="small-12 small-centered medium-12 medium-uncentered columns text-center iframesocial">
                                        <a target="_blank" rel="nofollow" href="<?php echo site_url( 'wp-login.php', 'https' ); ?>?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Facebook&amp;redirect_to=<?php $redirect_url = site_url( 'my-account', 'https' ); echo urlencode($redirect_url); ?>" title="Connect with Facebook" class="button icon facebook" data-provider="Facebook">
                                            <span><i class="fa fa-facebook"></i><?php _e('Facebook'); ?></span>
                                        </a>
                                    
                                        <a target="_blank" rel="nofollow" href="<?php echo site_url( 'wp-login.php', 'https' ); ?>?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Twitter&amp;redirect_to=<?php $redirect_url = site_url( 'my-account', 'https' ); echo urlencode($redirect_url); ?>" title="Connect with Twitter" class="button icon twitter" data-provider="Twitter">
                                            <span><i class="fa fa-twitter"></i><?php _e('Twitter'); ?></span>
                                        </a>
                                    
                                        <a target="_blank" rel="nofollow" href="#" title="Connect with Google+" class="button icon google-plus" data-provider="Google">
                                            <span><i class="fa fa-google-plus"></i><?php _e('Google'); ?></span>
                                        </a>
                                    </div>

                                </div>
                                <!-- Cancel -->
	</form>
	</div></div>
	<?php 
 wp_footer(); 
?>

<script type="text/javascript">
	$(document.body).addClass('iframe-j');
</script>