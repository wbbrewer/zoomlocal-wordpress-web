<?php 
/**
 *
 * Template Name: Vendor Product Slider
 *
 */
?>
<!DOCTYPE html>
<html>
<head>
<?php wp_head(); ?>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
</head>
<?php
$args 
        = 
        array( 
            'post_type'             => 'product',
            'post_status'           => 'publish',
            'posts_per_page'        => 16,
            'tax_query'             => array(
                'relation'  =>  'OR',
                array(
                    'taxonomy' => 'product_type',
                    'terms' => 'simple',
                    'field' => 'slug',
                ),
                array(
                    'taxonomy' => 'product_type',
                    'terms' => 'grouped',
                    'field' => 'slug',
                ),
                array(
                    'taxonomy' => 'product_type',
                    'terms' => 'variable',
                    'field' => 'slug',
                ),
                array(
                    'taxonomy' => 'product_type',
                    'terms' => 'external',
                    'field' => 'slug',
                ),
            )
        );

    $new_query = new WP_Query( $args );
    
?>
<body>
	<div id="wrapper" class="iframeslide"><!-- Wrapper -->
		<div class="content-area"><!-- Content Area -->
			<section class="slider-section">
				<div class="col-lg-12">
					<div class="slider-divs">

						<?php 
						if( $new_query->have_posts() ):
					       	while( $new_query->have_posts() ): $new_query->the_post();
									?>
									
									<div class="items">
									<?php  $post_image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), $size='medium' ); ?>
										<a target="_blank" href="<?php echo get_permalink(); ?>"><img src="<?php echo  $post_image_data[0]; ?>">
										<h4><?php //the_title(); 
										$user_id = $post->post_author;
                                        $key = 'yith_product_vendor_owner';
                                        $single = true;
                                        $user_last = get_user_meta( $user_id, $key, $single );
                                        $term = get_term($user_last,'yith_shop_vendor'); 
                                        echo  $vnames = $term->name;
                                          //echo $userid = $post->post_author; 
                                        //echo get_the_author_meta( 'first_name', $userid );
                                        //echo get_the_author_meta( 'last_name', $userid );
										?> </h4></a>
									</div>

							 		<?php 
							 	endwhile;
							endif;
						?>
					</div>
				</div>
			</section>
		</div>
	</div>
<div id="map"></div>
</body>
<?php
    wp_reset_postdata();
?>
<style>
.content-area .slider-section {
   background: #247b95 none repeat scroll 0 0;
   float: left;
   padding: 50px 0;
   width: 100%;
   height: auto
}
.content-area .slider-section .slider-divs .slick-list.draggable {
   margin: 0 auto;
   width:100%;
   max-width:1170px
}
.slick-slide {
	padding: 0px 15px;
}

.slick-dots {
	display: none !important;
}

.content-area .slider-section .slider-divs .slick-prev.slick-arrow {
   background: transparent url("http://myweb-development.com/zoomlocal/img/prev-icon.png") no-repeat scroll left top;
   border: medium none;
   height: 90px;
   position: absolute;
   text-indent: -9999px;
   top: 36%;
   width: 60px;
   left: 0;
   z-index: 9999;
}
.content-area .slider-section .slider-divs .slick-next.slick-arrow {
   background: transparent url("http://myweb-development.com/zoomlocal/img/next-icon.png") no-repeat scroll left top;
   border: medium none;
   height: 90px;
   position: absolute;
   text-indent: -9999px;
   top: 36%;
   width: 60px;
   right: 0;
   z-index:9999;
}


</style>
<?php 
 wp_footer(); 
?>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
<script>
jQuery(document).ready(function($){
	slickCarousel();

	function slickCarousel(){
		$('.slider-divs').slick({
		  slidesToShow: 4,
		  infinite: true,
		  rows: 2,
		  slidesToScroll: 1,
		  autoplay: false,
		  autoplaySpeed: 2000,
		  dots: false,
		  speed: 300,
		  responsive:true,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 770,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});
	}
});
</script>

<script>
ajax_url = '<?php echo admin_url( "admin-ajax.php", "relative" ); ?>';
    function initMap() {
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: {lat: 40.731, lng: -73.997}
      });
      var geocoder = new google.maps.Geocoder;
      var infowindow = new google.maps.InfoWindow;

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          my_cords = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          geocodeLatLng(geocoder, map, infowindow);
          
        });
      }
    }
    function geocodeLatLng(geocoder, map, infowindow) {
      	geocoder.geocode({'location': my_cords}, function(results, status) {
	        if (status === google.maps.GeocoderStatus.OK) {
	          	if (results[1]) {
	          		// Globally defined variable my_zipcode
		          	my_zipcode = results[1].address_components.pop().long_name;
		        }else{
					my_zipcode = '';
				}
				// console.log(my_zipcode);
				$.ajax({
			        url: ajax_url,
			        type: "POST",
			        data: {
			            'action':'product_search',
			            'value': {location: my_zipcode},
			        },
			        success:function(data) {
			        	console.log(data);
			        	 $('.items').remove();
			        	 $('.slider-divs').slick('unslick');
			        	var data = JSON.parse(data);
			        	var products = data['products'];
			        // console.log(data['script']);
			        	for( product in products){
			        		$('.slider-divs').append("<div class='items'><a target='_blank' href='"+products[product].url+"'><img src='"+products[product].img+"'><h4>"+products[product].titl+"</h4></a></div>");
			        	}
                           // alert(data['script']);
			        	$('.content-area').append(data['script']);
			        }
			    });
			}
      	});
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>

</html>