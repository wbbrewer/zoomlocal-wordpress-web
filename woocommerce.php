<?php get_header( 'shop' );
global $wp_query;
if ( is_shop() ) { // Only run on shop archive pages, not single products or other pages
	// Products per page
	$filters              = new ZL_Filters();
	$category_preference  = null;
	//$show_category_filter = isset( $wp_query->query_vars['categories'] );
	$show_category_filter = true;
	$show_location_filter = isset( $wp_query->query_vars['locations'] );
	$show_events_page = isset( $wp_query->query_vars['events-list'] );
	$per_page             = 25;
	$paged                = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	if ( get_query_var( 'taxonomy' ) ) { // If on a product taxonomy archive (category or tag)

		$args = array(
			'post_type'      => 'product',
			'posts_per_page' => $per_page,
			'tax_query'      => array(
				array(
					'taxonomy' => get_query_var( 'taxonomy' ),
					'field'    => 'slug',
					'terms'    => get_query_var( 'term' ),
				),
			),
		);

	} else { // On main shop page
		$args = array(
				'post_type'      => 'product',
				'orderby'        => 'date',
				'order'          => 'DESC',
				'posts_per_page' => $per_page,
				'paged'          => $paged,
		);
		
		if ( is_user_logged_in() && ! $show_category_filter && ! $show_events_page ) {
			$category_preference = json_decode( get_user_meta( get_current_user_id(), 'category_preference', true ) );
		}

		if ( $category_preference != null ) {
			$args['product_cat'] = implode( ',', $category_preference );
		}
	}

	do_action( 'woocommerce_before_main_content' );
	do_action( 'woocommerce_archive_description' );

	//prints the header
	if ( is_user_logged_in() && ! $show_category_filter && ! $show_location_filter  && ! $show_events_page) {
		?>
		<div class="row zl-shop-head">
			<div class="medium-5 medium-offset-1 columns text-left">
				<ul class="breadcrumbs">
					<li><a href="<?php echo get_bloginfo( 'url' ); ?>">Home</a></li>
					<li><a href="#">My Matching Listings</a></li>
				</ul>
			</div>
			<div class="medium-6 columns login-signup text-right">
				<a class="login" href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>">My Account</a>
			</div>
			<div class="medium-12 columns text-right"><h2>My Matching Listings</h2>

				<p>Below you will find your daily listings based on your account profile</p>

				<p><a href="<?php echo wc_customer_edit_account_url(); ?>"> Click here to change selections.</a></p>
			</div>
		</div>

	<?php
	} elseif ( $show_category_filter || $show_events_page) {
		?>
		<section class='filter'>
		<div class="container">
		<div class="row zl-shop-head">
			<!--<div class="medium-5 medium-offset-1 columns text-left end">
				<ul class="breadcrumbs">
					<li><a href="<?php //echo get_bloginfo( 'url' ); ?>">Home</a></li>
					<li><a href="#"><?php //echo ($show_events_page)?"Event List":"Listings";?></a></li>
				</ul>
			</div>-->
			<div class="medium-12 columns text-left">
				<div class="row collapse">
					<div class="medium-12 columns">
						
					</div>
					<div class="medium-12 columns">
						<p class="filterbuttons"><span class="zl-info">Add Filter:</span>
							<a id="category-filter-button" href="#" data-open="categoriesModal" class="button"><i
									class="fa fa-tags"></i> Categories</a>
							<a id="location-filter-button" href="#" data-open="locationsModal" class="button"><i
									class="fa fa-map-marker"></i> Locations</a>
							<a id="distance-filter-button" href="#" data-open="distanceModal" class="button"><i
									class="fa fa-compass"></i> Distance</a>
						</p>
					</div>
				</div>
			</div>
			<div class="medium-12 columns text-left">
				<ul id="category-filter-list" class="button-group">
				</ul>
			</div>
		</div>
		</div>
		</section>
		<section class='viewlisting'>
			<div class="container">
				<div class="row">
					<div class="small-12 listing-date">
						<div class="row">
							<div class="small-7 medium-7 text-left columns week-picker"><i class="fa fa-calendar"></i> <span class="left-date"> <<?php echo date("m/d/Y"); ?>></span></div>
							<div class="small-5 medium-5 text-right columns"><span>All Listings</span></div>
						</div>
					</div>
					<div class="small-12 text">
		                <p>Below you will find your daily listings based on your account profile settings.</p>
					</div>
				</div>				
			</div>
		</section>
		<?php
		$filters->zl_shop_filters();
	} elseif ( $show_location_filter ) {
		$filters->zl_shop_lcoation_filters();
	} elseif(false) {
		?>
		<div class="row zl-shop-head">
			<div class="medium-5 medium-offset-1 columns text-left">
				<ul class="breadcrumbs">
					<li><a href="#">Home</a></li>
					<li><a href="#">My Matching Listings</a></li>
				</ul>
			</div>
			<div class="medium-6 columns login-signup text-right">
				<a class="login"
				   href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>">My
					}
					Account</a>
			</div>
			<div class="medium-12 columns text-right"><h2>My Matching Listings</h2>

				<p>You can signup for an account and save your default listing preference.</p>

				<p><a href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>"> Click
						here to Signup.</a></p>
			</div>
		</div>
	<?php
	}

		//wp_set_object_terms( $object_id, $terms, $taxonomy, $append );
	//wp_set_object_terms($post_id, 'event', 'product_type');
	if($show_events_page){
		$args['tax_query']      = array(
			array(
				'taxonomy' =>'product_type',
				'field'    => 'slug',
				'terms'    => 'event',
			),
		);
		$args['meta_query']=array(
			'relation' => 'AND',
			array(
				'relation' => 'OR',
				array(
					'key' => '_rq_event_start_date',
					'value' => date('Y-m-d'),
					'compare' => '>=',
					'type' => 'NUMERIC'
				),
				array(
					'key' => '_rq_event_end_date',
					'value' => date('Y-m-d'),
					'compare' => '<=',
					'type' => 'NUMERIC'
				),
				array(
					'key' => '_changed_date',
					'type' => 'DATE'
				)
			),
			array(
				'key' => '_visibility',
				'value' => 'visible',
				'compare' => '='
			),
			array(
				'key' => '_featured',
				'value' => 'yes',
				'compare' => '='
			)

		);
		//change order
		unset($args['orderby']);
		unset($args['order']);
		$args['orderby'] = array(
			'date' => 'ASC',
			'_changed_date' => 'ASC'
		);
	}else{
		$args['tax_query']      = array(
			array(
				'taxonomy' =>'product_type',
				'field'    => 'slug',
				'terms'    => 'simple',
			),
		);
		//only featured posts from last 30 days
		$meta_query   = WC()->query->get_meta_query();
		$meta_query[] = array(
			array(
				'key'   => '_featured',
				'value' => 'yes'
			)/*,
			array(
				'key' => '_changed_date'
			)*/
		);
		$args['meta_query'] = $meta_query;
		//only post dates with from last month
	}
	$today=getdate();
	$args['date_query'] = array(
		array(
			'column' => 'post_date',
			'after'  => '1 month ago',
		),
		array(
			'column' => 'post_date',
			'before'  => array(
				'year'  => date('Y'),
				'month' => date('m'),
				'day'   => date('d')
			),
			'inclusive' => true,
		)
	);
	$products = new WP_Query( $args );

	// Standard loop
	$show_list = false;
	if($show_category_filter || is_user_logged_in() ){
		$show_list = true;
	}
	if($show_events_page){
		$show_list = true;
	}
	if ( $products->have_posts() ) :
		do_action( 'woocommerce_before_shop_loop' );
		woocommerce_product_loop_start();
		woocommerce_product_subcategories();
		while ( $products->have_posts() ) : $products->the_post();
			(!$show_events_page)?wc_get_template_part( 'content', 'product' ):wc_get_template_part( 'content', 'event' );
		endwhile;
		woocommerce_product_loop_end();
		wp_reset_postdata();
		do_action( 'woocommerce_after_shop_loop' );
		if ( $products->max_num_pages > 1 ):$filters->zl_load_more();
		endif;
	elseif ( ! woocommerce_product_subcategories( array(
		'before' => woocommerce_product_loop_start( false ),
		'after'  => woocommerce_product_loop_end( false )
	) )
	) :
		//wc_get_template( 'loop/no-products-found.php' );
	endif;
	do_action( 'woocommerce_after_main_content' );
} else {
	// If not on archive page (cart, checkout, etc), do normal operations
	do_action( 'woocommerce_before_main_content' );
	woocommerce_content();
	do_action( 'woocommerce_after_main_content' );
}
?>


​
<?php
get_footer( 'shop' );
