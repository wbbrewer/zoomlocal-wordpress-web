<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
/**
 * woocommerce_before_single_product hook
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );
global $woocommerce;
if ( post_password_required() ) {
	echo get_the_password_form();

	return;
}
?>

<div class="row zl-shop-head" style="margin-bottom:2rem;">
	<div class="medium-5 medium-offset-1 columns text-left">
		<!-- <ul class="breadcrumbs">
			 <li><a href=<?php //echo  get_home_url(); ?>">Home</a></li>
			<?php
			// global $product;
			// if($product->product_type==='event'){
			// 	$shop_page_url = site_url() . '/' . get_post( get_option( 'woocommerce_shop_page_id' ) )->post_name . '/';
			// 	?>
				<li><a href="<?php //echo $shop_page_url;?>events-list/">Events</a></li>
			<?php// }else{?>
			<li><a href="<?php //echo get_permalink( get_option( 'woocommerce_shop_page_id' )  );?>">Shop</a></li>
			<?php //}?>
			<li><a href="/"><?php //the_title();?></a></li>
		</ul> -->
	</div>
</div>
<section class="single_pro">
<div class="row">
<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>"
     id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="store-info-container">
		<div class="zl-store-header-wrapper row"
		     style="border: 1px solid rgb(142, 143, 143); padding: 1.25rem; margin-right: 1rem; margin-left: 1rem;">
		<!-- <div class="zl-store-header-wrapper row"
		     style="border: 1px solid rgb(142, 143, 143); padding: 1.25rem; margin-right: 1rem; margin-left: 1rem;"> -->
			<?php
			/** 
			 * woocommerce_before_single_product_summary hook
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
			?>

			<div class="summary entry-summary">

				<?php
				/**
				 * woocommerce_single_product_summary hook
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 */

				//display event details if product_type is event woocommerce_short_description
				add_action('woocommerce_single_product_summary',function(){
					global $product;

					if($product->product_type==='event'){
						$date_start = strtotime(get_post_meta($product->id, '_rq_event_start_date', true) );
						$date_end = strtotime(get_post_meta($product->id, '_rq_event_end_date', true) );
						$address = get_post_meta($product->id, '_rq_event_address_name', true).' '.get_post_meta($product->id, '_rq_event_region_name', true);

						?>
						<div class="row">
							<div itemprop="schedule" class="small-12 columns">
								<?php if($date_start){?>
								<span class='description'>Start Date:</span><span><?php echo date( get_option('date_format'),$date_start);?></span>
									<?php
									if($date_end){?>
										</br><span class='description'>End Date:</span><span><?php echo date( get_option('date_format'),$date_end);?></span>
									<?php }
									?>

							<?php }?>
							</div>
							<div itemprop="venue" class="small-12 columns">
								<?php if($address<>" "){?>
								<span class='description'>Venue:</span><span><?php echo $address;?></span>
								<?php }?>
							</div>
						</div>

					<?php	}
				},20);

				//moved the vendor summary from end of title to end of summary
				remove_action( 'woocommerce_single_product_summary', array(
					YITH_Vendors::instance()->frontend,
					'woocommerce_template_vendor_name'
				), 5 );
				add_action( 'woocommerce_single_product_summary', array(
					YITH_Vendors::instance()->frontend,
					'woocommerce_template_vendor_name'
				), 30 );

				//moved the price after product summary
				remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_price',10 );
				add_action('woocommerce_single_product_summary','woocommerce_template_single_price',50);




				do_action( 'woocommerce_single_product_summary' );

				?>

			</div>
		</div>
	</div>
	</div>

	<!-- .summary -->

	<?php
	/**
	 * woocommerce_after_single_product_summary hook
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	remove_action('woocommerce_after_single_product_summary','woocommerce_upsell_display',15);
	do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>"/>

<!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
