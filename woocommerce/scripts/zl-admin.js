/**
 * For Admin
 */

jQuery( function( $ ) {
    $( 'select#product-type' ).change( function () {
        var select_val = $( this ).val();
        if ( 'zl_membership' === select_val ) {
            $('.show_if_simple').show();
        }
    }).change();
});