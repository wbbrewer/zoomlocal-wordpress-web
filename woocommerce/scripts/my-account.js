$(document).ready(function () {
       jQuery('input.timepicker').timepicker({});
    jQuery("#store-icon").imagepicker({hide_select: true})
    if (jQuery("#selected-avatar").length) {
        jQuery("#store-icon").val(jQuery("#selected-avatar").val());
        jQuery("#store-icon").data('picker').sync_picker_with_select();
    }

});

//for address validation
var geocoder;
var map;
function initializemap() {
    geocoder = new google.maps.Geocoder();

    var mapOptions = {
        zoom: 12, //Change the Zoom level to suit your needs
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControlOptions: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE
        }
    }
    //map_canvas is just a <div> on the page with the id="map_canvas"
    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

    //Your Variable Containing The Address
    var street_1 = jQuery('#vendor-address1').val();
    var street_2 = jQuery('#vendor-address2').val();
    var city = jQuery('#vendor-city').val();
    var state = jQuery('#vendor-state').val();
    var country = 'USA';//jQuery('#country').val();
    var zip = jQuery('#vendor-zip').val();
    var address = street_1 + street_2 + city + state + country;
    document.getElementById('map_canvas').style.height = '350px';
    document.getElementById('map_canvas').style.width = '450px';
    google.maps.event.trigger(map, 'resize');
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            //places a marker on every location
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
            jQuery('#geo-loc').val(results[0].geometry.location.lat()+','+results[0].geometry.location.lng());
            document.getElementById("add-person").disabled = false;
        } else {

            document.getElementById('map_canvas').style.height = '0px';
            alert('Invalid address.. Please check Google Maps and check if the Address exists.');
            document.getElementById("add-person").disabled = true;
        }
    });
}

function initialize() {
jQuery('#add-person').prop('disabled', true);
}
google.maps.event.addDomListener(window, 'load', initialize);