/*Script for handling the shop page
 * @todo add initial states to input options
 * @todo fix known issue: when user modify the url directly on the same window/tab, the screen doesnt refresh (firefox v38)
 * @todo fix url not consistent when switching tab. the initial search in previous tab is not retained when switching back to it.
 * @todo add location filter after vendor added
 * */
$("document").ready(function () {
    $("input[name='cat_group[]']").attr('checked',false);
    $("input[name='loc_group[]']").attr('checked',false);
    $("#current-location").val(0);
    var latitudeAndLongitude=document.getElementById("latitudeAndLongitude"),
        location={
            latitude:'',
            longitude:''
        };

    if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(showPosition,notShared);
    }
    else{
        latitudeAndLongitude.innerHTML="Geolocation is not supported by this browser.";
    }
});


//update url when changing selections

$(document).on('click', '#category-filter-list > li > a', function () {
    var removeme = $(this).attr('data-id');
    var searchitem = $(this).attr('data-search');
    var currentlocation = $(this).attr('data-distance');

    if(currentlocation!=""){
        $("#current-location").val(0);
    }
    if(searchitem!=""){
        $('#yith-s').val("");
    }
    if(removeme!=""){
        $("input[value='"+removeme+"']").attr('checked',false);
    }
    apply_search();
    return false;
});
$('.applys').click(function() {
   $('#category-filter-list').empty(apply_search());
   $('input:checkbox').removeAttr('checked');
   apply_search()
});
/*load more button starts here*/
//search box/button starts here
$(document).on('submit', '#yith-ajaxsearchform', function () {
    apply_search();
    return false;
});
$(document).on('click', '#search-button', function () {
    apply_search();
    return false;
});
$(document).on('change', '#current-location', function () {
    apply_search();
    return false;
});
$(document).on('click', '#category-filter-list > li > a.date-item', function () {
    selected_week = '';
    selected_year = '';
    apply_search();
    return false;
});
//search box/button ends here
$(document).on('click', '#zl-load-more', function (event) {
    event.preventDefault();
    $data = $(this).attr('data-query');
    $('ul.products').append('<div id="preloader"></div>');
    $('#preloader').fadeIn('slow');
    jQuery.ajax({
        url : zl_ajax_script.ajax_url,
        type : 'post',
        data : {
            action : 'zl_load_more_ajax',
            args: $data
        },
        success : function( response ) {
            //alert(response);
            $('#preloader').fadeOut('slow',function(){$(this).remove();});
            $('#zl-load-more').fadeOut('fast',function(){$(this).remove();});
            $('ul.products').append(response);
        }
    });

    return false;
});
/*load more button ends here*/
$(document).on('change', 'input[type="checkbox"]', function () {
   apply_search();
    return false;
});

function apply_search(){
    $('#category-filter-list>li').remove();

    if(typeof(selected_week) !== 'undefined'){
        if( selected_week != '' ){
        $('#category-filter-list').append('<li class="date-li"><a class="button info tiny date-item" data-date="'+selected_year+'/'+selected_week+'">'+selected_year+'/'+selected_week+'</a></li>');
    }}else{ selected_week = selected_year = '';}
    
    var selected_category =  Array();
    $.each($("input[name='cat_group[]']:checked"), function() {
        selected_category.push($(this).val());
        var item = $(this);
        //console.log();
        $('#category-filter-list').append('<li><a class="button info tiny category-item" data-id="'+item.val()+'">'+item.next().text()+'</a></li>');
    });
    var selected_locations =  Array();
    $.each($("input[name='loc_group[]']:checked"), function() {
        selected_locations.push( $(this).val() );
        var item = $(this);
        $('#category-filter-list').append('<li><a class="button info tiny location-item" data-id="'+item.val()+'">'+item.next().text()+'</a></li>');
    });
    var search = "";
    search = $('#yith-s').val();
    if(search!=""){
        $('#category-filter-list').append('<li><a class="button info tiny search-item" data-search="'+search+'">'+search+'</a></li>');
    }
    var origin = "";
    origin = $('#latitudeAndLongitude').attr('data-location');

    if($("#current-location").val()!=0){
        $('#category-filter-list').append('<li><a class="button info tiny distance-item" data-distance="'+$("#current-location").val()+'"> Within '+$("#current-location").val()+' Miles</a></li>');
    }
    //console.log(origin);
    var distance = 0;
    distance = $('#current-location').val();
    $('ul.products').append('<div id="preloader"></div>');
    $('#preloader').fadeIn('slow');

    jQuery.ajax({
        url : zl_ajax_script.ajax_url,
        type : 'post',
        data : {
            action : 'zl_do_shop_filters',
            tab: selected_category.join(','),
            locs: selected_locations,
            search: search,
            search_year: selected_year,
            search_week: selected_week,
            origin: origin,
            distance: distance,
            use_events: zl_ajax_script.on_events_page
        },
        success : function( response ) {
            console.log(response);
            $('#preloader').fadeOut('slow',function(){$(this).remove();});
            $('#zl-load-more').fadeOut('fast',function(){$(this).remove();});
            $('.woocommerce-result-count').remove();
            $('.woocommerce-ordering').remove();
            $('.products').remove();
            $('.woocommerce-pagination').remove();
            $('#main').append(response);
        }
    });

    return false;
}


function showPosition(position){
    location.latitude=position.coords.latitude;
    location.longitude=position.coords.longitude;
    /*latitudeAndLongitude.innerHTML="Latitude: " + position.coords.latitude +
    "<br>Longitude: " + position.coords.longitude;*/
    var geocoder = new google.maps.Geocoder();
    var latLng = new google.maps.LatLng(location.latitude, location.longitude);

    if (geocoder) {
        geocoder.geocode({ 'latLng': latLng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                //console.log(results[0].formatted_address);
                jQuery('#latitudeAndLongitude').attr('data-location',results[0].formatted_address);
                latitudeAndLongitude.innerHTML="<i class='fa fa-map-marker'></i>Current Location:<span> "+results[0].formatted_address+"</span>";                
                //$('#address').html('Address:'+results[0].formatted_address);
            }
            else {
                //$('#address').html('Geocoding failed: '+status);
                console.log("Geocoding failed: " + status);
                latitudeAndLongitude.innerHTML="Please share your location to use this Filter";
            }
        }); //geocoder.geocode()
    }
} //showPosition

function notShared(){
    $.getJSON("http://freegeoip.net/json/", function(data) {
        location.latitude=data.latitude;
        location.longitude=data.longitude;
        var geocoder = new google.maps.Geocoder();
        var latLng = new google.maps.LatLng(location.latitude, location.longitude);

        if (geocoder) {
            geocoder.geocode({ 'latLng': latLng}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //console.log(results[0].formatted_address);
                    jQuery('#latitudeAndLongitude').attr('data-location',results[0].formatted_address);
                    //$('#address').html('Address:'+results[0].formatted_address);
                    latitudeAndLongitude.innerHTML="Current Location via IP Address: "+results[0].formatted_address;
                }
                else {
                    //$('#address').html('Geocoding failed: '+status);
                    console.log("Geocoding failed: " + status);
                    latitudeAndLongitude.innerHTML="Please share your location to use this Filter";
                }
            }); //geocoder.geocode()
        }
    });
}
