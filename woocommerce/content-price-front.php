<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}
?>
<a href="<?php echo get_home_url()?>/?add-to-cart=<?php echo $id;?>" class="package-button" data-prod="<?php echo $id;?>"><?php echo get_the_content(); ?></a>