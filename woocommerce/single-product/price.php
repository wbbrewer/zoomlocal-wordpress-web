<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>


<div class="main_price" style="width:100%">

	<div class="row">
		<div class="small-3 columns">
			<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="small-12 columns">

				<p class="price"><?php echo $product->get_price_html(); ?></p>

				<meta itemprop="price" content="<?php echo $product->get_price(); ?>" />
				<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
				<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

			</div>
		</div>
		<div class="small-9 columns">
			<ul class="soc_list" style="padding:5px;">
				<li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><img
							src="<?php echo get_template_directory_uri() . "/images/icons/fb.png"; ?>"/></a>
				</li>
				<li><a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=&description="><img
							src="<?php echo get_template_directory_uri() . "/images/icons/pin.png"; ?>"/></a>
				</li>
				<li><a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>"><img
							src="<?php echo get_template_directory_uri() . "/images/icons/tw.png"; ?>"/></a>
				</li>
				<li><a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><img
							src="<?php echo get_template_directory_uri() . "/images/icons/gplus.png"; ?>"/></a>
				</li>
				<li><a href="mailto:?subject=<?php urlencode(the_title('Check this listing at Zoomlocal: '))?>&body=Hey,%20I%20found%20this%20listing%20at%20Zoomlocal.%20Check%20this%20link%20out%20<?php the_permalink();?>"><img
							src="<?php echo get_template_directory_uri() . "/images/icons/mail.png"; ?>"/></a>
				</li>
				<li><a href="#" onclick="window.prompt('Copy the link: Ctrl+C/Cmd-C, Enter', '<?php the_permalink();?>');"><img
							src="<?php echo get_template_directory_uri() . "/images/icons/dots.png"; ?>"/></a>
				</li>
			</ul>
		</div>
	</div>

</div>

