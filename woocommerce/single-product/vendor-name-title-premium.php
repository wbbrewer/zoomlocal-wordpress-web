<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>


<div class="row small-collapse vendor-info">
	<div class="small-4 medium-2 column">
		<img src="<?php echo get_template_directory_uri() . "/images/icons/bag.jpg"; ?>">
	</div>
	<div class="small-8 medium-10 column">
		<address class="vendor-vcard">
			<?php
			$owner        = get_user_by( 'id', $vendor->owner );
			$header_image = wp_get_attachment_image( $vendor->header_image, 'big', false, array( 'class' => 'store-image' ) );
			$owner_avatar = get_avatar( $vendor->owner, '62' );

			?>
			<span><a href="<?php echo $vendor->get_url() ?>"><?php echo $vendor->name; ?></a></span>
			<?php if ( ! empty( $vendor->location ) ) : ?>
				<span>
                    <i class="fa fa-location-arrow"></i>
					<?php echo $vendor->location ?>
                </span>
			<?php endif; ?>
			<?php if ( ! empty( $vendor->telephone ) ) : ?>
				<span><i class="fa fa-phone"></i><a class="store-telephone" href="tel:+<?php echo $vendor->telephone ?>">
						<?php echo $vendor->telephone ?></a>
				</span>
			<?php endif; ?>


		</address>
	</div>
</div>