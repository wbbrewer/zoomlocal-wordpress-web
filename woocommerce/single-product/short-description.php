<?php
/**
 * Single product short description
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

if ( ! $post->post_excerpt ) {
	return;
}

?>
<div class="row">
	<div itemprop="description" class="small-12 columns">
		<?php echo apply_filters( 'woocommerce_short_description', "<span class='description'>Description:</span>".$post->post_excerpt ) ?>
	</div>
</div>

