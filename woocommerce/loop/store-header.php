<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$owner          = get_user_by( 'id', $vendor->owner );
$header_image   = wp_get_attachment_image( $vendor->header_image, 'big', false, array( 'class' => 'store-image' ) );
$owner_avatar   = get_avatar( $vendor->owner, '62' );
$vendor_reviews = $vendor->get_reviews_average_and_product();
?>

<section class="vendor_details">
<div class="row">

<div class="store-info-container text-center">
	<div class="zl-store-header-wrapper row"
	     style="border: 1px solid rgb(142, 143, 143); padding: 1.25rem; margin-right: 1rem; margin-left: 1rem;">
		<!--  Header Image -->
		<?php if ( ! empty( $vendor->header_image ) ): ?>
			<?php echo $header_image ?>
		<?php endif; ?>

		<!--  Store Information -->
		<div class="zl-store-info small-12 columns text-center">
			<h2> <?php echo $vendor->name ?></h2>

			<p class="zl-store-description"><span>Vendor Description: </span><?php echo $vendor->description ?></p>
			<img src="<?php echo get_template_directory_uri() . "/images/icons/bag.jpg"; ?>">

			<h3>Address</h3>

			<p class="zl-vendor-address">
				<span><?php echo $vendor->address1 . ' ' . $vendor->address2; ?></span>
				<span><?php echo $vendor->city . ',' . $vendor->state; ?></span>
				<span><?php echo $vendor->telephone ?></span>
				<span><a href="<?php echo   preg_replace( '/^(?!https?:\/\/)/', 'http://', $vendor->store_website ) ?>" target="_blank"><?php echo $vendor->store_website ?></a></span>
				<?php if ( isset( $vendor->hours ) ) {

					$hours_html = '<ul><span>Hours of Operation:</span>';
					$hours      = json_decode( $vendor->hours, true );
					foreach ( $hours as $day => $hours ) {
						$print_this_hour = ($hours[0]<>"" && $hours[1]<>"")?true:false;
						if($print_this_hour){
							$hours_html .= "<li><span class='day'>$day</span><span class='hours'>($hours[0] - $hours[1])</span></li>";
						}
					}
					$hours_html .= '</ul>';
					echo $hours_html;
				}
				?>
				<span style="font-size:.8rem;"><?php echo (isset($vendor->closed_on) && $vendor->closed_on<>"")? "Closed On: $vendor->closed_on" :""; ?></span>
			</p>
		</div>

		<!--  Store Information -->
		<div class="store-socials small-12 columns text-center">
        <span class="socials-container">
            <?php
            $src = "";
            foreach ( $vendor->socials as $social => $uri ) : ?>
	            <?php if ( ! empty( $uri ) ) : ?>
		            <?php
		            if ( $social == "facebook" ) {
			            $src = "/images/icons/fb.jpg";
		            } elseif ( $social === "twitter" ) {
			            $src = "/images/icons/tw.jpg";
		            } elseif ( $social === "google" ) {
			            $src = "/images/icons/gplus.jpg";
		            } elseif ( $social === "pinterest" ) {
			            $src = "/images/icons/pin.jpg";
		            } elseif ( $social === "instagram" ) {
			            $src = "/images/icons/camera.jpg";
		            }
		            ?>
		            <a class="vendor-social-uri"
		               href="<?php echo preg_replace( '/^(?!https?:\/\/)/', 'http://', $uri ) ?>" target="_blank">
			            <img src="<?php echo get_template_directory_uri() . $src; ?>"/>
		            </a>
	            <?php endif; ?>
            <?php endforeach; ?>

		</div>
		<?php
		//share THIS page to social media
		?>
		<div class="row">
			<div class="medium-12 columns clearfix">

				<ul class="soc-list inline-list inline-block">
					<li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><img
								src="<?php echo get_template_directory_uri() . "/images/icons/fb.png"; ?>"/></a>
					</li>
					<li><a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=&description="><img
								src="<?php echo get_template_directory_uri() . "/images/icons/pin.png"; ?>"/></a>
					</li>
					<li><a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>"><img
								src="<?php echo get_template_directory_uri() . "/images/icons/tw.png"; ?>"/></a>
					</li>
					<li><a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><img
								src="<?php echo get_template_directory_uri() . "/images/icons/gplus.png"; ?>"/></a>
					</li>
					<li><a href="mailto:?subject=<?php urlencode(the_title('Check this listing at Zoomlocal: '))?>&body=Hey,%20I%20found%20this%20listing%20at%20Zoomlocal.%20Check%20this%20link%20out%20<?php the_permalink();?>"><img
								src="<?php echo get_template_directory_uri() . "/images/icons/mail.png"; ?>"/></a>
					</li>
					<li><a href="#" onclick="window.prompt('Copy the link: Ctrl+C/Cmd-C, Enter', '<?php the_permalink();?>');"><img
								src="<?php echo get_template_directory_uri() . "/images/icons/dots.png"; ?>"/></a>
					</li>
				</ul>
			</div>

		</div>
	</div>
</div>
<div class="text-center  current-listing-title">
	<h3>Our Current Listings</h3>
</div>
</div>
</section>