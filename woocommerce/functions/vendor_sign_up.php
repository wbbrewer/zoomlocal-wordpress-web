<?php


function zl_vendor_signup(){

	if( isset($_POST['register-vendor'])){

		$store_name = !empty($_POST['vendor-name']) ? sanitize_text_field($_POST['vendor-name']) : '';
		$store_ein = !empty($_POST['vendor-ein']) ? sanitize_text_field($_POST['vendor-ein']) : '';
		$store_address1 = !empty($_POST['vendor-address1']) ? sanitize_text_field($_POST['vendor-address1']) : '';
		$store_address2 = !empty($_POST['vendor-address2']) ? sanitize_text_field($_POST['vendor-address2']) : '';
		$store_city = !empty($_POST['vendor-city']) ? sanitize_text_field($_POST['vendor-city']) : '';
		$store_state = !empty($_POST['vendor-state']) ? sanitize_text_field($_POST['vendor-state']) : '';
		$store_zip = !empty($_POST['vendor-zip']) ? sanitize_text_field($_POST['vendor-zip']) : '';
// location is what YITH uses for a combined address in the front page.
		$store_location = !empty($_POST['vendor-location']) ? sanitize_text_field($_POST['vendor-location']) : '';
		$store_telephone = !empty($_POST['vendor-telephone']) ? sanitize_text_field($_POST['vendor-telephone']) : '';
		$store_website = !empty($_POST['vendor-website']) ? sanitize_text_field($_POST['vendor-website']) : '';
// sanitize_email will record an empty value if it is not an emaill address.
		$store_email = !empty($_POST['vendor-email']) ? sanitize_email($_POST['vendor-email']) : '';
		$error = "The following fields are required" ;
		if( empty( $_POST['vendor-location'] ) ) {
			$error .= "<br>The Store address" ;
		}
		if( empty( $_POST['vendor-email'] ) ) {
			$error .= "<br>Store Email";
		}
		if( empty( $_POST['vendor-telephone'] ) ) {
			$error .= "<br>Business Phone" ;
		}
		if( empty( $_POST['vendor-name'] ) ) {
			$error .= "<br>Company Name" ;
		}
		else {
			$term = sanitize_text_field( $_POST['vendor-name'] );
			$duplicated = yith_wcpv_check_duplicate_term_name( $term, YITH_Vendors()->get_taxonomy_name() );
			if( $duplicated ){
				$error .= "<br>Company name (Name is already taken).";
			}
		}

		if($error==="The following fields are required"){
			//get_current_user_id()
			$term_info  = wp_insert_term( $store_name, YITH_Vendors()->get_taxonomy_name() );
			if( is_wp_error( $term_info ) ) {
				return;
			}/* Get the new vendor */
			$vendor      = yith_get_vendor($term_info['term_id']  );
			$customer_id = get_current_user_id();

			/* Set the vendor information by magic method */
			$vendor->owner                  = $customer_id;
			$vendor->location               = $store_location;
			$vendor->telephone              = $store_telephone;
			$vendor->store_email            = $store_email;
			$vendor->registration_date      = current_time( 'mysql' );
			$vendor->registration_date_gmt  = current_time( 'mysql', 1 );
			$vendor->enable_selling         = 'no';
			$vendor->pending = 'yes';

			/*Custom variables*/
			$vendor->ein = $store_ein;
			$vendor->address1 = $store_address1;
			$vendor->address2 = $store_address2;
			$vendor->city = $store_city;
			$vendor->state = $store_state;
			$vendor->zip = $store_zip;
			$vendor->store_website = $store_website;


			/* Set Owner */
			update_user_meta( $customer_id, YITH_Vendors()->get_user_meta_owner(), $vendor->id );
			update_user_meta( $customer_id, YITH_Vendors()->get_user_meta_key(), $vendor->id );

			yith_wcpv_add_vendor_caps( $customer_id );

			do_action( 'yith_new_vendor_registration', $vendor );
			//redirects the page if user already belongs to a store
			wp_redirect( home_url()."/my-account/" ); exit;

		}else{
			echo $error;
		}
	}
}