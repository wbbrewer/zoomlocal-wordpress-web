<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$is_pending = 'yes' == $vendor->pending ? true : false;
?>
<h2><?php _e( 'My Vendor Dashboard', 'yith_wc_product_vendors' ) ?></h2>

<p class="myaccount_vendor_dashboard">
	<?php
    if( $is_pending ){
        _e( "You'll be able to access your dashboard as soon as the administrator approves your vendor account.", 'yith_wc_product_vendors' );
        echo '<br/>';
    }

	_e( 'From your vendor dashboard you can view your recent commissions, view the sales report and manage your store and payment settings.', 'yith_wc_product_vendors' );

    if( ! $is_pending ){
	    $myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
	    $myaccount_page_url = '/manage-shop/';
	    if ( $myaccount_page_id ) {
		    $myaccount_page_url = get_permalink( $myaccount_page_id );
	    }
        echo '<br>';
			if($vendor->is_owner(get_current_user_id())){
				printf( __( 'Click <a href="%s">here</a> to edit <strong>your store Information</strong>.', 'yith_wc_product_vendors' ), $myaccount_page_url.'/manage-shop/' );
			}
	    }

    ?>
</p>
