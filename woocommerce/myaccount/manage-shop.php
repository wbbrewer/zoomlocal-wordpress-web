<?php
/**
 * Edit account form
 *
 * @author       moonl33 / Moon Lee
 * @package    WooCommerce/Templates
 * @version     2.2.7
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$yith_shop = yith_get_vendor( get_current_user_id(), 'user' );
// Check that the nonce is valid, and the user can edit this post.
if (
	isset( $_POST['my_image_upload_nonce'] )
	&& wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' )
) {
	// The nonce was valid and the user has the capabilities, it is safe to continue.

	// These files need to be included as dependencies when on the front end.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );

	// Let WordPress handle the upload.
	// Remember, 'my_image_upload' is the name of our file input in our form above.
	$attachment_id = media_handle_upload( 'my_image_upload', 0 );

	if ( is_wp_error( $attachment_id ) ) {
		echo "There was an error uploading the image.";
	} else {
		echo "The image was uploaded successfully!";
		$yith_shop->header_image = $attachment_id;
	}

} else {

	// The security check failed, maybe show the user an error.
}


/*Handles update store info form*/

if ( isset( $_POST['update-store-info'] ) ) {
	$message = "<span>Updating Basic Info</span>";
	echo $message;
	if ( isset( $_POST["vendor-name"] ) ) {
		$yith_shop->name = sanitize_text_field( $_POST["vendor-name"] );
	}
	if ( isset( $_POST["vendor-ein"] ) ) {
		$yith_shop->ein = sanitize_text_field( $_POST["vendor-ein"] );
	}
	if ( isset( $_POST["vendor-address1"] ) ) {
		$yith_shop->address1 = sanitize_text_field( $_POST["vendor-address1"] );
	}
	if ( isset( $_POST["vendor-address2"] ) ) {
		$yith_shop->address2 = sanitize_text_field( $_POST["vendor-address2"] );
	}
	if ( isset( $_POST["vendor-city"] ) ) {
		$yith_shop->city = sanitize_text_field( $_POST["vendor-city"] );
	}
	if ( isset( $_POST["vendor-state"] ) ) {
		$yith_shop->state = sanitize_text_field( $_POST["vendor-state"] );
	}
	if ( isset( $_POST["vendor-zip"] ) ) {
		$yith_shop->zip = sanitize_text_field( $_POST["vendor-zip"] );
		$zip            = substr( sanitize_text_field( $_POST["vendor-zip"] ), 0, 5 );
		$location_list  = json_decode( file_get_contents( get_template_directory() . '/assets/json/locations.json' ) );
		$location_array = array();
		foreach ( $location_list as $location ) {
			if ( $location->{'Zip Code'} == $zip ) {
				$yith_shop->location = sanitize_text_field( $location->{'Group'} );
				break;
			}
		}

	}
	if ( isset( $_POST["vendor-telephone"] ) ) {
		$yith_shop->telephone = sanitize_text_field( $_POST["vendor-telephone"] );
	}
	if ( isset( $_POST["vendor-website"] ) ) {
		$yith_shop->store_website = sanitize_text_field( $_POST["vendor-website"] );
	}
	if ( isset( $_POST["vendor-email"] ) ) {
		$yith_shop->store_email = sanitize_text_field( $_POST["vendor-email"] );
	}
	if ( isset( $_POST["geo-loc"] ) ) {
		$yith_shop->geoloc = sanitize_text_field( $_POST["geo-loc"] );
	}

}

/*Handles store description and business hours*/
$message_hours = "";
if ( isset( $_POST['save-hours'] ) ) {
	if ( isset( $_POST["description"] ) ) {
		$yith_shop->description = esc_textarea( stripslashes( $_POST["description"] ) );
	}
	if ( isset( $_POST["description"] ) ) {
		$yith_shop->store_icon = $_POST["store-avatar"];
	}

	$hours = array();
	if ( isset( $_POST["sun-from"] ) && isset( $_POST["sun-to"] ) ) {
		$hours["sun"][0] = sanitize_text_field( $_POST["sun-from"] );
		$hours["sun"][1] = sanitize_text_field( $_POST["sun-to"] );
	}
	if ( isset( $_POST["mon-from"] ) && isset( $_POST["mon-to"] ) ) {
		$hours["mon"][0] = sanitize_text_field( $_POST["mon-from"] );
		$hours["mon"][1] = sanitize_text_field( $_POST["mon-to"] );
	}
	if ( isset( $_POST["tue-from"] ) && isset( $_POST["tue-to"] ) ) {
		$hours["tue"][0] = sanitize_text_field( $_POST["tue-from"] );
		$hours["tue"][1] = sanitize_text_field( $_POST["tue-to"] );
	}
	if ( isset( $_POST["wed-from"] ) && isset( $_POST["wed-to"] ) ) {
		$hours["wed"][0] = sanitize_text_field( $_POST["wed-from"] );
		$hours["wed"][1] = sanitize_text_field( $_POST["wed-to"] );
	}
	if ( isset( $_POST["thu-from"] ) && isset( $_POST["thu-to"] ) ) {
		$hours["thu"][0] = sanitize_text_field( $_POST["thu-from"] );
		$hours["thu"][1] = sanitize_text_field( $_POST["thu-to"] );
	}
	if ( isset( $_POST["fri-from"] ) && isset( $_POST["fri-to"] ) ) {
		$hours["fri"][0] = sanitize_text_field( $_POST["fri-from"] );
		$hours["fri"][1] = sanitize_text_field( $_POST["fri-to"] );
	}
	if ( isset( $_POST["sat-from"] ) && isset( $_POST["sat-to"] ) ) {
		$hours["sat"][0] = sanitize_text_field( $_POST["sat-from"] );
		$hours["sat"][1] = sanitize_text_field( $_POST["sat-to"] );
	}

	$yith_shop->hours = wp_json_encode( $hours );
	if( isset($_POST["closed-on"])) {
		$yith_shop->closed_on = sanitize_text_field( $_POST["closed-on"] );
	}
	/*save socials*/
	$socials_array = array();
	if ( isset( $_POST['social-facebook'] ) ) {
		$socials_array['facebook'] = $_POST['social-facebook'];
	}
	if ( isset( $_POST['social-pinterest'] ) ) {
		$socials_array['pinterest'] = $_POST['social-pinterest'];
	}
	if ( isset( $_POST['social-twitter'] ) ) {
		$socials_array['twitter'] = $_POST['social-twitter'];
	}
	if ( isset( $_POST['social-google'] ) ) {
		$socials_array['google'] = $_POST['social-google'];
	}
	if ( isset( $_POST['social-instagram'] ) ) {
		$socials_array['instagram'] = $_POST['social-instagram'];
	}
	$yith_shop->socials = $socials_array;
	if ( isset( $_POST['cat_group'] ) ) {
		$store_category            = wp_json_encode( $_POST['cat_group'] );
		$yith_shop->store_category = $store_category;
	}


}


?>



<div class="row zl-shop-head">
	<div class="medium-5 medium-offset-1 columns text-left">
		<ul class="breadcrumbs">
			<li><a href="#">Home</a></li>
			<li><a href="#">Store</a></li>
			<li><a href="#">Setup</a></li>
		</ul>
	</div>
	<div class="medium-6 columns login-signup text-right">
		<?php  
			if( is_user_logged_in() ){
						if( is_vendor() || is_administrator() ){
							echo '<a class="login" href="'.get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ).'">My Account</a>';	
						}else{
							echo '<a class="login" href="http://listings-dev.zoomlocal.com/my-account/edit-account/">My Account</a>';	
						}
					}else{
						echo '<a class="login" href="'.get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ).'">My Account</a>';		
					}
		?>
	</div>
</div>
<?php wc_print_notices();
?>
<h2>Business Profile</h2>

<form class="zl-register" id="featured_upload" method="post" action="#" enctype="multipart/form-data">
	<div class="row">
		<div class="small-12 medium-4 columns">
			<img
				src="<?php echo ( $yith_shop->header_image ) ? wp_get_attachment_url( $yith_shop->header_image ) : wc_placeholder_img_src(); ?>">
		</div>
		<div class="small-12 medium-8 columns">
			<input type="file" name="my_image_upload" id="my_image_upload" multiple="false"/>
			<?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
			<input class="button" id="submit_my_image_upload" name="submit_my_image_upload" type="submit"
			       value="Upload"/>
		</div>
	</div>
</form>

<form class="zl-register" id="busineness-hours" method="post" action="#busineness-hours">
<div class="row">

	<div class="large-12 columns">
		<label><span>Store Icon</span></label>
		<select id="store-icon" class="image-picker show-html" name="store-avatar">
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/automotive_icon.png' ?>"
				value="automotive">automotive
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/boatingmarine_icon.png' ?>"
				value="boatingmarine"> boatingmarine
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/community_icon.png' ?>"
				value="community"> community
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/education_icon.png' ?>"
				value="education"> education
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/emergency_icon.png' ?>"
				value="emergency"> emergency
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/entertainment_icon.png' ?>"
				value="entertainment"> entertainment
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/governmentnonprofit.png' ?>"
				value="nonprofit"> nonprofit
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/HomeandGarden_icon.png' ?>"
				value="homegarden"> homegarden
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/medicalhealth_icon.png' ?>"
				value="medicalhealth"> medicalhealth
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/nightlife_icon.png' ?>"
				value="nightlife"> nightlife
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/petsanimals_icon.png' ?>"
				value="petsanimals"> petsanimals
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/professional_icon.png' ?>"
				value="professional"> professional
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/realestate_icon.png' ?>"
				value="realestate"> realestate
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/restaurants_icon.png' ?>"
				value="restaurants"> restaurants
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/services_icon1.png' ?>"
				value="services1"> services1
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/services_icon2.png' ?>"
				value="services2"> services2
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/shopping_icon.png' ?>"
				value="shopping"> shopping
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/sports_icon.png' ?>"
				value="sports"> sports
			</option>
			<option
				data-img-src="<?php echo get_template_directory_uri() . '/woocommerce/store-avatar/travel_icon.png' ?>"
				value="travel"> travel
			</option>
		</select>
	</div>
	<div class="large-12 columns">
		<label><span>Business Description</span></label>
		<textarea
			name="description"><?php echo esc_textarea( stripslashes( $yith_shop->description ) ) ?></textarea>
	</div>
</div>
<?php
$hours = json_decode( $yith_shop->hours );
?>
<div class="row">
<div class="small-12 columns">
	<label><span>Hours of Operation</span></label>
</div>
<div class="small-12 columns">
	<div class="row">
		<div class=" small-12 medium-10 large-6 columns end bus-time">
			<div class="row">
				<div class="small-3 columns">
					<label class="inline">Sun </label>
				</div>
				<div class="small-4 columns">
					<input class="timepicker " type="text" name="sun-from"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->sun[0] ) : ""; ?>"/>
				</div>
				<div class="small-1 columns">
					<label class="text-center inline"> - </label>
				</div>
				<div class="small-4 columns end">
					<input class="timepicker " type="text" name="sun-to"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->sun[1] ) : ""; ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 columns">
					<label class="inline">Mon </label>
				</div>
				<div class="small-4 columns">
					<input class="timepicker " type="text" name="mon-from"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->mon[0] ) : ""; ?>"/>
				</div>
				<div class="small-1 columns">
					<label class="text-center inline"> - </label>
				</div>
				<div class="small-4 columns end">
					<input class="timepicker " type="text" name="mon-to"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->mon[1] ) : ""; ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 columns">
					<label class="inline">Tue </label>
				</div>
				<div class="small-4 columns">
					<input class="timepicker " type="text" name="tue-from"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->tue[0] ) : ""; ?>"/>
				</div>
				<div class="small-1 columns">
					<label class="text-center inline"> - </label>
				</div>
				<div class="small-4 columns end">
					<input class="timepicker " type="text" name="tue-to"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->tue[1] ) : ""; ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 columns">
					<label class="inline">Wed </label>
				</div>
				<div class="small-4 columns">
					<input class="timepicker " type="text" name="wed-from"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->wed[0] ) : ""; ?>"/>
				</div>
				<div class="small-1 columns">
					<label class="text-center inline"> - </label>
				</div>
				<div class="small-4 columns end">
					<input class="timepicker " type="text" name="wed-to"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->wed[1] ) : ""; ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 columns">
					<label class="inline">Thu </label>
				</div>
				<div class="small-4 columns">
					<input class="timepicker " type="text" name="thu-from"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->thu[0] ) : ""; ?>"/>
				</div>
				<div class="small-1 columns">
					<label class="text-center inline"> - </label>
				</div>
				<div class="small-4 columns end">
					<input class="timepicker " type="text" name="thu-to"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->thu[1] ) : ""; ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 columns">
					<label class="inline">Fri </label>
				</div>
				<div class="small-4 columns">
					<input class="timepicker " type="text" name="fri-from"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->fri[0] ) : ""; ?>"/>
				</div>
				<div class="small-1 columns">
					<label class="text-center inline"> - </label>
				</div>
				<div class="small-4 columns end">
					<input class="timepicker " type="text" name="fri-to"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->fri[1] ) : ""; ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 columns">
					<label class="inline">Sat </label>
				</div>
				<div class="small-4 columns">
					<input class="timepicker " type="text" name="sat-from"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->sat[0] ) : ""; ?>"/>
				</div>
				<div class="small-1 columns">
					<label class="text-center inline"> - </label>
				</div>
				<div class="small-4 columns end">
					<input class="timepicker " type="text" name="sat-to"
					       value="<?php echo ( $hours <> null ) ? esc_attr( $hours->sat[1] ) : ""; ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="small-4 medium-3  columns">
					<label class="inline"> Closed on</label>
				</div>
				<div class="small-8 medium-9 columns">
					<input type="text" name="closed-on" value="<?php echo $yith_shop->closed_on?>">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="zl-filters">
	<div class="large-12 columns">

		<h2>Categories</h2>

		<p>Select one or more categories for services or items you offer <em>'in-store'</em> or online.</p>

		<div class="menu-categs-box">
			<?php
			$store_cats = json_decode( $yith_shop->store_category );
			$wcatTerms = get_terms( 'product_cat', array(
				'hide_empty' => 0,
				'orderby'    => 'ASC',
				'parent'     => 0,
			) );
			foreach ( $wcatTerms as $wcatTerm ) :
				?>
				<ul class="accordion" data-accordion role="tablist">
					<li class="accordion-navigation">
						<a href="#<?php echo $wcatTerm->slug; ?>" role="tab"
						   aria-controls="<?php echo $wcatTerm->slug; ?>"><?php echo $wcatTerm->name; ?></a>

						<div id="<?php echo $wcatTerm->slug; ?>" class="content active" role="tabpanel"
						     aria-labelledby="<?php echo $wcatTerm->slug; ?>">
							<ul class="wsubcategs">
								<?php $wsubargs = array(
									'hierarchical'     => 1,
									'show_option_none' => '',
									'hide_empty'       => 0,
									'parent'           => $wcatTerm->term_id,
									'taxonomy'         => 'product_cat'
								);
								$wsubcats       = get_categories( $wsubargs );?>
								<div class="row">
									<div class="large-12 columns">
										<?php foreach ( $wsubcats as $wsc ): ?>
											<div class="large-4 columns"><input name="cat_group[]"
											                                    value="<?php echo $wsc->term_id; ?>"
											                                    type="checkbox"  <?php if ( $store_cats <> null ) {
													echo in_array( $wsc->term_id, $store_cats ) ? "checked" : "";
												} ?>><label
													for="cat_group[]">
													<?php echo $wsc->name; ?></label></div>
										<?php endforeach; ?>
									</div>
								</div>
							</ul>
						</div>
					</li>
				</ul>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<div class="small-12 columns">
	<label><span><?php _e( 'Social Media' ); ?></span></label>
</div>
<div class="small-12 columns">
	<div class="row">
		<div class="small-12 medium-3 columns">
			<label for="social-facebook" class="inline"><?php _e( 'Facebook' ); ?></label>
		</div>
		<div class="small-12 medium-9 columns">
			<input type="text" class="input-text" name="social-facebook" id="social-facebook"
			       value="<?php echo isset( $yith_shop->socials['facebook'] ) ? esc_attr( $yith_shop->socials['facebook'] ) : ""; ?>"/>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-3 columns">
			<label for="social-pinterest" class="inline"><?php _e( 'Pinterest' ); ?></label>
		</div>
		<div class="small-12 medium-9 columns">
			<input type="text" class="input-text" name="social-pinterest" id="social-pinterest"
			       value="<?php echo isset( $yith_shop->socials['pinterest'] ) ? esc_attr( $yith_shop->socials['pinterest'] ) : ""; ?>"/>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-3 columns">
			<label for="social-twitter" class="inline"><?php _e( 'Twitter' ); ?></label>
		</div>
		<div class="small-12 medium-9 columns">
			<input type="text" class="input-text" name="social-twitter" id="social-twitter"
			       value="<?php echo isset( $yith_shop->socials['twitter'] ) ? esc_attr( $yith_shop->socials['twitter'] ) : ""; ?>"/>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-3 columns">
			<label for="social-google" class="inline"><?php _e( 'Google Plus' ); ?></label>
		</div>
		<div class="small-12 medium-9 columns">
			<input type="text" class="input-text" name="social-google" id="social-google"
			       value="<?php echo isset( $yith_shop->socials['google'] ) ? esc_attr( $yith_shop->socials['google'] ) : ""; ?>"/>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-3 columns">
			<label for="social-instagram" class="inline"><?php _e( 'Instagram' ); ?></label>
		</div>
		<div class="small-12 medium-9 columns">
			<input type="text" class="input-text" name="social-instagram" id="social-instagram"
			       value="<?php echo isset( $yith_shop->socials['instagram'] ) ? esc_attr( $yith_shop->socials['instagram'] ) : ""; ?>"/>
		</div>
	</div>

</div>
</div>
<input class="button" id="save-hours" name="save-hours" type="submit" value="Update Business Details"/>
</form>


<form class="zl-register" id="shop-managers" method="post" action="#">
<h2><?php _e( 'Company Info', 'yith_wc_product_vendors' ); ?></h2>

<!-- Admin First name -->

<!-- Vendor name -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3  columns">
				<label for="vendor-name" class="inline"><?php _e( 'Company Name', 'yith_wc_product_vendors' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="vendor-name" id="vendor-name"
				       value="<?php echo esc_attr( $yith_shop->name ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!-- Vendor ein -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3  columns">
				<label for="vendor-ein" class="inline"><?php _e( 'Company EIN', 'yith_wc_product_vendors' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="vendor-ein" id="vendor-ein"
				       value="<?php echo esc_attr( $yith_shop->ein ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!-- Vendor phone -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3  columns">
				<label for="vendor-telephone" class="inline"><?php _e( 'Business Phone', 'yith_wc_product_vendors' ); ?>
					<span class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="vendor-telephone" id="vendor-telephone"
				       value="<?php echo esc_attr( $yith_shop->telephone ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!-- Vendor website -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3  columns">
				<label for="vendor-website"
				       class="inline"><?php _e( 'Business Website', 'yith_wc_product_vendors' ); ?></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" name="vendor-website" id="vendor-website"
				       value="<?php echo esc_attr( $yith_shop->store_website ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!-- Vendor email -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3  columns">
				<label for="vendor-email" class="inline"><?php _e( 'Business Email', 'yith_wc_product_vendors' ); ?>
					<span class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="email" class="input-text" required name="vendor-email" id="vendor-email"
				       value="<?php echo esc_attr( $yith_shop->store_email ); ?>"/>
			</div>
		</div>
	</div>
</div>


<!-- Vendor address1 -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3  columns">
				<label for="vendor-address1"
				       class="inline"><?php _e( 'Business Address 1', 'yith_wc_product_vendors' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="vendor-address1" id="vendor-address1"
				       value="<?php echo esc_attr( $yith_shop->address1 ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!-- Vendor address2 -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3  columns">
				<label for="vendor-address2"
				       class="inline"><?php _e( 'Business Address 2', 'yith_wc_product_vendors' ); ?></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" name="vendor-address2" id="vendor-address2"
				       value="<?php echo esc_attr( $yith_shop->address2 ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!-- Vendor city -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3  columns">
				<label for="vendor-city" class="inline"><?php _e( 'Business City', 'yith_wc_product_vendors' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="vendor-city" id="vendor-city"
				       value="<?php echo esc_attr( $yith_shop->city ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!-- Vendor state -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3  columns">
				<label for="vendor-state" class="inline"><?php _e( 'Business State', 'yith_wc_product_vendors' ); ?>
					<span class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<select name="vendor-state" required name="vendor-state" id="vendor-state" class="select2-choice"
				        placeholder="">
					<option value="">Select an option…</option>
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="CA">California</option>
					<option value="CO">Colorado</option>
					<option value="CT">Connecticut</option>
					<option value="DE">Delaware</option>
					<option value="DC">District Of Columbia</option>
					<option value="FL">Florida</option>
					<option value="GA">Georgia</option>
					<option value="HI">Hawaii</option>
					<option value="ID">Idaho</option>
					<option value="IL">Illinois</option>
					<option value="IN">Indiana</option>
					<option value="IA">Iowa</option>
					<option value="KS">Kansas</option>
					<option value="KY">Kentucky</option>
					<option value="LA">Louisiana</option>
					<option value="ME">Maine</option>
					<option value="MD">Maryland</option>
					<option value="MA">Massachusetts</option>
					<option value="MI">Michigan</option>
					<option value="MN">Minnesota</option>
					<option value="MS">Mississippi</option>
					<option value="MO">Missouri</option>
					<option value="MT">Montana</option>
					<option value="NE">Nebraska</option>
					<option value="NV">Nevada</option>
					<option value="NH">New Hampshire</option>
					<option value="NJ">New Jersey</option>
					<option value="NM">New Mexico</option>
					<option value="NY">New York</option>
					<option value="NC">North Carolina</option>
					<option value="ND">North Dakota</option>
					<option value="OH">Ohio</option>
					<option value="OK">Oklahoma</option>
					<option value="OR">Oregon</option>
					<option value="PA">Pennsylvania</option>
					<option value="RI">Rhode Island</option>
					<option value="SC">South Carolina</option>
					<option value="SD">South Dakota</option>
					<option value="TN">Tennessee</option>
					<option value="TX">Texas</option>
					<option value="UT">Utah</option>
					<option value="VT">Vermont</option>
					<option value="VA">Virginia</option>
					<option value="WA">Washington</option>
					<option value="WV">West Virginia</option>
					<option value="WI">Wisconsin</option>
					<option value="WY">Wyoming</option>
					<option value="AA">Armed Forces (AA)</option>
					<option value="AE">Armed Forces (AE)</option>
					<option value="AP">Armed Forces (AP)</option>
					<option value="AS">American Samoa</option>
					<option value="GU">Guam</option>
					<option value="MP">Northern Mariana Islands</option>
					<option value="PR">Puerto Rico</option>
					<option value="UM">US Minor Outlying Islands</option>
					<option value="VI">US Virgin Islands</option>
				</select></p>
			</div>
		</div>
	</div>
</div>
<?php
/*@todo:Change this function here*/
$state = esc_attr( $yith_shop->state );
?>
<script>
	var state = document.getElementById("vendor-state").value = "<?php echo $state;?>";
</script>
<!-- Vendor zip -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3  columns">
				<label for="vendor-zip" class="inline"><?php _e( 'Business Zip', 'yith_wc_product_vendors' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="vendor-zip" id="vendor-zip"
				       value="<?php echo esc_attr( $yith_shop->zip ); ?>"/>
			</div>
		</div>
	</div>
</div>
<div id="map_canvas" style="margin:1rem 0;"></div>
<div id="verify-map">
	<a class="button" href="javascript:void(0); " onclick="return initializemap();">Verify Address</a>
	<input class="button" id="add-person" name="update-store-info" type="submit" disabled value="Submit for review"/>
</div>
<input type="hidden" name="vendor-location" id="vendor-location" value="tbd">
<input type="hidden" name="geo-loc" id="geo-loc" value="">
<input type="hidden" name="selected-avatar" id="selected-avatar"
       value="<?php echo esc_attr( $yith_shop->store_icon ); ?>">

</form>
