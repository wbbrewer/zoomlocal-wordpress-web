<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

?>

<!-- Register checkbox -->
<div id="yith-vendor-registration">
	<?php
		if(!is_user_logged_in()){?>
			<h2><?php _e('User Info', 'jointstheme'); ?></h2>
			<!-- First name -->
			<div class="row">
				<div class="small-12 columns">
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="reg_first_name" class="inline"><?php _e('First Name', 'woocommerce'); ?> <span class="required">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" class="input-text" required name="first_name" id="reg_first_name" value="<?php if (!empty($_POST['first_name'])) echo esc_attr($_POST['first_name']); ?>" />
						</div>
					</div>
				</div>
			</div>

			<!--  Last name -->
			<div class="row">
				<div class="small-12 columns">
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="reg_last_name" class="inline"><?php _e('Last Name', 'woocommerce'); ?> <span class="required">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" class="input-text" required name="last_name" id="reg_last_name" value="<?php if (!empty($_POST['last_name'])) echo esc_attr($_POST['last_name']); ?>" />
						</div>
					</div>
				</div>
			</div>

			<?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

				<p class="form-row form-row-wide">
					<label for="reg_username"><?php _e('Username', 'woocommerce'); ?> <span class="required">*</span></label>
					<input type="text" class="input-text" name="username" id="reg_username" value="<?php if (!empty($_POST['username'])) echo esc_attr($_POST['username']); ?>" />
				</p>

			<?php endif; ?>

			<!-- Email -->
			<div class="row">
				<div class="small-12 columns">
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="reg_email" class="inline"><?php _e('Email address', 'woocommerce'); ?> <span class="required">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="email" class="input-text" name="email" id="reg_email" value="<?php if (!empty($_POST['email'])) echo esc_attr($_POST['email']); ?>" />
						</div>
					</div>
				</div>
			</div>

			<h2><?php _e('Set Password', 'woocommerce'); ?></h2>

			<!-- Password -->
			<?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>

				<div class="row">
					<div class="small-12 columns">
						<div class="row">
							<div class="small-12 medium-4 columns">
								<label for="reg_password" class="inline"><?php _e('Password', 'woocommerce'); ?> <span class="required">*</span></label>
							</div>
							<div class="small-12 medium-8 columns">
								<input type="password" class="input-text" name="password" id="reg_password" />
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="small-12 columns">
						<div class="row">
							<div class="small-12 medium-4 columns">
								<label for="reg_password" class="inline"><?php _e('Re-type password', 'woocommerce'); ?> <span class="required">*</span></label>
							</div>
							<div class="small-12 medium-8 columns">
								<input type="password" class="input-text" name="password" id="reg_password" />
							</div>
						</div>
					</div>
				</div>

			<?php endif; ?>
	<?php	}
	?>
    <h2><?php _e('Company Info', 'yith_wc_product_vendors'); ?></h2>
    <!-- Admin First name -->

    <!-- Vendor name -->
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="small-12 medium-4 columns">
                    <label for="vendor-name" class="inline"><?php _e('Company Name', 'yith_wc_product_vendors'); ?> <span class="required">*</span></label>
                </div>
                <div class="small-12 medium-8 columns">
                    <input type="text" class="input-text" required name="vendor-name" id="vendor-name" value="<?php if (!empty($_POST['vendor-name'])) echo esc_attr($_POST['vendor-name']); ?>" />
                </div>
            </div>
        </div>
    </div>

    <!-- Vendor ein -->
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="small-12 medium-4 columns">
                    <label for="vendor-ein" class="inline"><?php _e('Company EIN', 'yith_wc_product_vendors'); ?> <span class="required">*</span></label>
                </div>
                <div class="small-12 medium-8 columns">
                    <input type="text" class="input-text" required name="vendor-ein" id="vendor-ein" value="<?php if (!empty($_POST['vendor-ein'])) echo esc_attr($_POST['vendor-ein']); ?>" />
                </div>
            </div>
        </div>
    </div>

    <!-- Vendor address1 -->
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="small-12 medium-4 columns">
                    <label for="vendor-address1" class="inline"><?php _e('Business Address 1', 'yith_wc_product_vendors'); ?> <span class="required">*</span></label>
                </div>
                <div class="small-12 medium-8 columns">
                    <input type="text" class="input-text" required name="vendor-address1" id="vendor-address1" value="<?php if (!empty($_POST['vendor-address1'])) echo esc_attr($_POST['vendor-address1']); ?>" />
                </div>
            </div>
        </div>
    </div>

    <!-- Vendor address2 -->
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="small-12 medium-4 columns">
                    <label for="vendor-address2" class="inline"><?php _e('Business Address 2', 'yith_wc_product_vendors'); ?></label>
                </div>
                <div class="small-12 medium-8 columns">
                    <input type="text" class="input-text" name="vendor-address2" id="vendor-address2" value="<?php if (!empty($_POST['vendor-address2'])) echo esc_attr($_POST['vendor-address2']); ?>" />
                </div>
            </div>
        </div>
    </div>

    <!-- Vendor city -->
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="small-12 medium-4 columns">
                    <label for="vendor-city" class="inline"><?php _e('Business City', 'yith_wc_product_vendors'); ?> <span class="required">*</span></label>
                </div>
                <div class="small-12 medium-8 columns">
                    <input type="text" class="input-text" required name="vendor-city" id="vendor-city" value="<?php if (!empty($_POST['vendor-city'])) echo esc_attr($_POST['vendor-city']); ?>" />
                </div>
            </div>
        </div>
    </div>

    <!-- Vendor state -->
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="small-12 medium-4 columns">
                    <label for="vendor-state" class="inline"><?php _e('Business State', 'yith_wc_product_vendors'); ?> <span class="required">*</span></label>
                </div>
                <div class="small-12 medium-8 columns">
                    <select name="vendor-state" required name="vendor-state" id="vendor-state" class="select2-choice" placeholder="">
                        <option value="">Select an option…</option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="DC">District Of Columbia</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                        <option value="AA">Armed Forces (AA)</option>
                        <option value="AE">Armed Forces (AE)</option>
                        <option value="AP">Armed Forces (AP)</option>
                        <option value="AS">American Samoa</option>
                        <option value="GU">Guam</option>
                        <option value="MP">Northern Mariana Islands</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="UM">US Minor Outlying Islands</option>
                        <option value="VI">US Virgin Islands</option></select></p>
                </div>
            </div>
        </div>
    </div>

    <!-- Vendor zip -->
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="small-12 medium-4 columns">
                    <label for="vendor-zip" class="inline"><?php _e('Business Zip', 'yith_wc_product_vendors'); ?> <span class="required">*</span></label>
                </div>
                <div class="small-12 medium-8 columns">
                    <input type="text" class="input-text" required name="vendor-zip" id="vendor-zip" value="<?php if (!empty($_POST['vendor-zip'])) echo esc_attr($_POST['vendor-zip']); ?>" />
                </div>
            </div>
        </div>
    </div>

    <!-- Vendor phone -->
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="small-12 medium-4 columns">
                    <label for="vendor-telephone" class="inline"><?php _e('Business Phone', 'yith_wc_product_vendors'); ?> <span class="required">*</span></label>
                </div>
                <div class="small-12 medium-8 columns">
                    <input type="text" class="input-text" required name="vendor-telephone" id="vendor-telephone" value="<?php if (!empty($_POST['vendor-telephone'])) echo esc_attr($_POST['vendor-telephone']); ?>" />
                </div>
            </div>
        </div>
    </div>

    <!-- Vendor website -->
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="small-12 medium-4 columns">
                    <label for="vendor-website" class="inline"><?php _e('Business Website', 'yith_wc_product_vendors'); ?></label>
                </div>
                <div class="small-12 medium-8 columns">
                    <input type="text" class="input-text" name="vendor-website" id="vendor-website" value="<?php if (!empty($_POST['vendor-website'])) echo esc_attr($_POST['vendor-website']); ?>" />
                </div>
            </div>
        </div>
    </div>

    <!-- Vendor email -->
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="small-12 medium-4 columns">
                    <label for="vendor-email" class="inline"><?php _e('Business Email', 'yith_wc_product_vendors'); ?> <span class="required">*</span></label>
                </div>
                <div class="small-12 medium-8 columns">
                    <input type="email" class="input-text" required name="vendor-email" id="vendor-email" value="<?php if (!empty($_POST['vendor-email'])) echo esc_attr($_POST['vendor-email']); ?>" />
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="vendor-location" id="vendor-location" value="tbd">
	<?php $user_info = (is_user_logged_in())?get_userdata(get_current_user_id()): false ?>
	<input type="hidden" name="vendor-owner-firstname" value="<?php if($user_info){echo $user_info->first_name;}?>">
	<input type="hidden" name="vendor-owner-lastname"  value="<?php if($user_info){echo $user_info->last_name;}?>">
</div>