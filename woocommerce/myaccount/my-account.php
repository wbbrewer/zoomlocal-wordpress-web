<?php
/**
 * My Account page
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $wp_query;
wc_print_notices();


if ( isset( $wp_query->query_vars['manage-shop'] ) && yith_get_vendor( get_current_user_id(), 'user' )->is_owner( get_current_user_id() ) ) {
	wc_get_template( 'myaccount/manage-shop.php' );
} else {
	?>
	<p class="myaccount_user">
		<?php
		printf(
			__( 'Hello <strong>%1$s</strong> (not %1$s? <a href="%2$s">Sign out</a>).', 'woocommerce' ) . ' ',
			$current_user->display_name,
			wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
		);

		printf( __( '<br>From your account dashboard you can view your recent orders, manage your shipping and billing addresses and <a href="%s">edit your password and account details</a>.', 'woocommerce' ),
			wc_customer_edit_account_url()
		);
		?>
	</p>
	<?php

	//Change yith hook so Shop Manager Role can have access to my-vendor-dashboard-on my account

	remove_action( 'woocommerce_before_my_account', array( YITH_Vendors::instance()->frontend, 'vendor_dashboard_endpoint' ),10 );
	add_action('woocommerce_before_my_account','zl_vendor_dashboard_endpoint',10);
	?>
	<?php do_action( 'woocommerce_before_my_account' ); ?>

	<?php wc_get_template( 'myaccount/my-downloads.php' ); ?>

	<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>

	<?php wc_get_template( 'myaccount/my-address.php' ); ?>

	<?php do_action( 'woocommerce_after_my_account' ); ?>
<?php
}


