<?php
/**
 * Edit account form
 *
 * @author       moonl33 / Moon Lee
 * @package    WooCommerce/Templates
 * @version     2.2.7
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<form action="" method="post" class="zl-register">
<h3>User Info</h3>
<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="reg_first_name" class="inline"><?php _e( 'First Name', 'woocommerce' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="account_first_name" id="account_first_name"
				       value="<?php echo esc_attr( $user->first_name ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!--  Last name -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="reg_last_name" class="inline"><?php _e( 'Last Name', 'woocommerce' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="account_last_name" id="account_last_name"
				       value="<?php echo esc_attr( $user->last_name ); ?>"/>
			</div>
		</div>
	</div>
</div>

<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

	<p class="form-row form-row-wide">
		<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="text" class="input-text" name="username" id="reg_username"
		       value="<?php echo esc_attr( $user->user_login ); ?>"/>
	</p>

<?php endif; ?>

<!-- Email -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="reg_email" class="inline"><?php _e( 'Email address', 'woocommerce' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="email" class="input-text" name="account_email" id="account_email"
				       value="<?php echo esc_attr( $user->user_email ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!-- Address 1 -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="reg_billing_address_1" class="inline"><?php _e( 'Address 1', 'woocommerce' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="billing_address_1" id="reg_billing_address_1"
				       value="<?php echo esc_attr( get_user_meta( $user->ID, 'billing_address_1', true ) ); ?>"/>
			</div>
		</div>
	</div>
</div>
<!-- Address 2 -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="reg_billing_address_2" class="inline"><?php _e( 'Address 2', 'woocommerce' ); ?> </label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" name="billing_address_2" id="reg_billing_address_2"
				       value="<?php echo esc_attr( get_user_meta( $user->ID, 'billing_address_2', true ) ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!-- City -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="reg_billing_city" class="inline"><?php _e( 'City', 'woocommerce' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="billing_city" id="reg_billing_city"
				       value="<?php echo esc_attr( get_user_meta( $user->ID, 'billing_city', true ) ); ?>"/>
			</div>
		</div>
	</div>
</div>

<!-- State -->

<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="reg_billing_state" class="inline"><?php _e( 'State', 'woocommerce' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-12 medium-9 columns">
				<select name="billing_state" required name="billing_state" id="billing_state" class="select2-choice"
				        placeholder="" title="State *">
					<option value="">Select an option…</option>
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="CA">California</option>
					<option value="CO">Colorado</option>
					<option value="CT">Connecticut</option>
					<option value="DE">Delaware</option>
					<option value="DC">District Of Columbia</option>
					<option value="FL">Florida</option>
					<option value="GA">Georgia</option>
					<option value="HI">Hawaii</option>
					<option value="ID">Idaho</option>
					<option value="IL">Illinois</option>
					<option value="IN">Indiana</option>
					<option value="IA">Iowa</option>
					<option value="KS">Kansas</option>
					<option value="KY">Kentucky</option>
					<option value="LA">Louisiana</option>
					<option value="ME">Maine</option>
					<option value="MD">Maryland</option>
					<option value="MA">Massachusetts</option>
					<option value="MI">Michigan</option>
					<option value="MN">Minnesota</option>
					<option value="MS">Mississippi</option>
					<option value="MO">Missouri</option>
					<option value="MT">Montana</option>
					<option value="NE">Nebraska</option>
					<option value="NV">Nevada</option>
					<option value="NH">New Hampshire</option>
					<option value="NJ">New Jersey</option>
					<option value="NM">New Mexico</option>
					<option value="NY">New York</option>
					<option value="NC">North Carolina</option>
					<option value="ND">North Dakota</option>
					<option value="OH">Ohio</option>
					<option value="OK">Oklahoma</option>
					<option value="OR">Oregon</option>
					<option value="PA">Pennsylvania</option>
					<option value="RI">Rhode Island</option>
					<option value="SC">South Carolina</option>
					<option value="SD">South Dakota</option>
					<option value="TN">Tennessee</option>
					<option value="TX">Texas</option>
					<option value="UT">Utah</option>
					<option value="VT">Vermont</option>
					<option value="VA">Virginia</option>
					<option value="WA">Washington</option>
					<option value="WV">West Virginia</option>
					<option value="WI">Wisconsin</option>
					<option value="WY">Wyoming</option>
					<option value="AA">Armed Forces (AA)</option>
					<option value="AE">Armed Forces (AE)</option>
					<option value="AP">Armed Forces (AP)</option>
					<option value="AS">American Samoa</option>
					<option value="GU">Guam</option>
					<option value="MP">Northern Mariana Islands</option>
					<option value="PR">Puerto Rico</option>
					<option value="UM">US Minor Outlying Islands</option>
					<option value="VI">US Virgin Islands</option>
				</select>
			</div>
			<?php
			/*@todo:Change this function here*/
			$state = esc_attr( get_user_meta( $user->ID, 'billing_state', true ) );
			?>
			<script>
				var state = document.getElementById("billing_state").value = "<?php echo $state;?>";
			</script>
		</div>
	</div>
</div>

<!-- Zip -->
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="reg_billing_postcode" class="inline"><?php _e( 'Zip', 'woocommerce' ); ?> <span
						class="required">*</span></label>

			</div>
			<div class="small-12 medium-9 columns">
				<input type="text" class="input-text" required name="billing_postcode" id="reg_billing_postcode"
				       value="<?php echo esc_attr( get_user_meta( $user->ID, 'billing_postcode', true ) ); ?>"/>
			</div>
		</div>
	</div>
</div>


<!-- Phone -->
<div class="row">
	<div class="small-12 columns">
      <div class="row">
                   <!-- <div class="small-3 columns">
                        <label for="reg_billing_phone" class="inline"><?php _e('Phone', 'woocommerce'); ?> <span class="required">*</span></label>
                    </div>
                    <div class="small-9 columns">-->
                        <!--<input type="hidden" class="input-text" required name="billing_phone" id="reg_billing_phone" value=" <?php if (!empty($_POST['billing_phone'])) echo esc_attr($_POST['billing_phone']); ?>" />-->
                    <input type="hidden" required name="billing_phone" id="reg_billing_phone" value="000-000-0000" />
                    </div>
               <!--  </div>	 -->	
                <!--<div class="row">
			<div class="small-3 columns">
				<label for="reg_billing_phone" class="inline"><?php _e( 'Phone', 'woocommerce' ); ?> <span
						class="required">*</span></label>
			</div>
			<div class="small-9 columns">
				<input type="text" class="input-text" required name="billing_phone" id="reg_billing_phone"
				       value="<?php echo esc_attr( get_user_meta( $user->ID, 'billing_phone', true ) ); ?>"/>
			</div>
		</div>-->
	</div>
</div>

<?php do_action( 'woocommerce_edit_account_form' ); ?>

<p class="form-row">
	<?php wp_nonce_field( 'save_account_details' ); ?>
	<input type="submit" class="button" name="save_account_details"
	       value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"/>
	<input type="hidden" name="action" value="save_account_details"/>
</p>

<?php do_action( 'woocommerce_edit_account_form_end' ); ?>

<!-- Password -->


<h3>Change/Set My Password</h3>
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="password_current" class="inline"><?php _e( 'Current Password', 'woocommerce' ); ?></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="password" class="input-text" name="password_current" id="password_current"/>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="password_1" class="inline"><?php _e( 'New Password', 'woocommerce' ); ?></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="password" class="input-text" name="password_1" id="password_1"/>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<label for="password_2" class="inline"><?php _e( 'Re-type password', 'woocommerce' ); ?></label>
			</div>
			<div class="small-12 medium-9 columns">
				<input type="password" class="input-text" name="password_2" id="password_2"/>
			</div>
		</div>
	</div>
</div>

<p>
	<?php wp_nonce_field( 'save_account_details' ); ?>
	<input type="submit" class="button" name="save_account_details"
	       value="<?php esc_attr_e( 'Change Password', 'woocommerce' ); ?>"/>
	<input type="hidden" name="action" value="save_account_details"/>
</p>

<!-- Categories -->
<div class="row">
	<div class="large-12 columns">
		<h3>Categories (Optional)</h3>
		<h5>Check as many categories of interest. Change selections as often as you´d like</h5>

		<div id="edit-category" class="menu-categs-box">
			<?php
			$category_preference = json_decode( get_user_meta( $user->ID, 'category_preference', true ) );
			$wcatTerms           = get_terms( 'product_cat', array(
				'hide_empty' => 0,
				'orderby'    => 'ASC',
				'parent'     => 0
			) );
			foreach ( $wcatTerms as $wcatTerm ) :
				?>
				<ul class="accordion" data-accordion role="tablist">
					<li class="accordion-navigation">
						<a href="#<?php echo $wcatTerm->slug; ?>" role="tab"
						   aria-controls="<?php echo $wcatTerm->slug; ?>"><?php echo $wcatTerm->name; ?></a>

						<div id="<?php echo $wcatTerm->slug; ?>" class="content" role="tabpanel"
						     aria-labelledby="<?php echo $wcatTerm->slug; ?>">
							<ul class="wsubcategs">
								<?php $wsubargs = array(
									'hierarchical'     => 1,
									'show_option_none' => '',
									'hide_empty'       => 0,
									'parent'           => $wcatTerm->term_id,
									'taxonomy'         => 'product_cat'
								);
								$wsubcats       = get_categories( $wsubargs );?>
								<div class="row">
									<div class="large-12 columns">
										<?php foreach ( $wsubcats as $wsc ): ?>
											<div class="large-4 columns"><input name="cat_group[]"
											                                    value="<?php echo $wsc->slug; ?>"
											                                    type="checkbox" <?php if ( $category_preference <> null ) {
													echo in_array( $wsc->slug, $category_preference ) ? "checked" : "";
												} ?> ><label
													for="cat_group[]">
													<?php echo $wsc->name; ?></label></div>
										<?php endforeach; ?>
									</div>
								</div>
							</ul>
						</div>
					</li>
				</ul>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<!--End Categories-->
<!-- Locations -->
<div class="row">
	<div class="large-12 columns">
		<h3>Locations (Optional)</h3>
		<h5>Locations Check as many locations of interest you would like listings from. Change selections as
			often as you'd like.</h5>

		<div id="locations-panel">
			<ul class="wsubcategs">
				<div class="row">
					<div class="large-12 columns">
						<?php
						$location_preference = json_decode( get_user_meta( $user->ID, 'location_preference', true ) );
						$location_list       = json_decode( file_get_contents( get_template_directory() . '/assets/json/locations.json' ) );
						$location_array      = array();
						foreach ( $location_list as $location ) {
							if ( ! in_array( $location->Group, $location_array ) ) {
								$location_array[] = $location->Group;
							}
						}//end foreach
						foreach ( $location_array as $key ) {
							?>
							<div class="large-4 columns"><input name="loc_group[]" value="<?php echo $key; ?>"
							                                    type="checkbox" <?php if ( $location_preference <> null ) {
									echo in_array( $key, $location_preference ) ? "checked" : "";
								} ?>>
								<label for="loc_group[]"><?php echo $key; ?></label>
							</div>
						<?php }//end foreach ?>
					</div>
				</div>
			</ul>
		</div>
	</div>
</div>
<!--End Locations-->
<!-- Delivery -->
<div class="row">
	<div class="large-12 columns">
		<h3>How do you want your listings</h3>
		<h5>Select any or all methods</h5>
		<?php $delivery_preference = json_decode( get_user_meta( $user->ID, 'delivery_preference', true ) ); ?>
		<div id="delivery-panel">
			<ul class="wsubcategs">
				<div class="row">
					<div class="small-12 columns">
						<div class="small-3 columns"><input name="del_group[]" value="daily-email"
						                                    type="checkbox" <?php if ( $delivery_preference <> null ) {
								echo in_array( "daily-email", $delivery_preference ) ? "checked" : "";
							} ?> >
							<label for="del_group[]">Daily Email</label>
						</div>
						<div class="small-9 columns end"><input name="del_group[]" value="mobile-app"
						                                        type="checkbox" <?php if ( $delivery_preference <> null ) {
								echo in_array( "mobile-app", $delivery_preference ) ? "checked" : "";
							} ?> >
							<label for="del_group[]">Mobile App</label>
							<a href="#">Download App</a>
						</div>
					</div>
				</div>
			</ul>
		</div>
	</div>
</div>
<!--End Delivery-->
<p>
	<?php wp_nonce_field( 'save_account_details' ); ?>
	<input type="submit" class="button" name="save_account_details"
	       value="<?php esc_attr_e( 'Save Preference', 'woocommerce' ); ?>"/>
	<input type="hidden" name="action" value="save_account_details"/>
</p>
</form>

