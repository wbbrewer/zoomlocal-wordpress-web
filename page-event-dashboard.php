<?php
/*
Template Name: New Front Event Dashboard
*/
?>

<?php
//need to make sure required plugins are enabled - move this to functions.php???
if ( ! class_exists( 'YITH_Vendors' ) ){
	exit;
}


//add conditional tags here and some checkers
global $current_user;
get_currentuserinfo();
//if its an edit post = make sure the user is the author of the post

$subscription_type = null;
$user_id           = get_current_user_id();
$access            = true;
$yith_shop = yith_get_vendor( get_current_user_id(), 'user' );
if($yith_shop->id===0){
	$access = false;
	$message = "Please create your vendor profile.";

	wp_redirect( home_url() );
	exit;
}

get_header(); ?>

<div id="content">

	<div id="inner-content" class="row">

		<div id="main" class="large-12 medium-12 columns" role="main">
			<?php
			if ( $access ) {
				include(locate_template('dashboard/event-container.php'));

			} else {
				echo "<p>You don't have access to this page. If you already have a subscription and you are seeing this message, please contact us. </p>
					  <p><span>$message<span></p>";
			}
			?>

		</div>
		<!-- end #main -->

	</div>
	<!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
