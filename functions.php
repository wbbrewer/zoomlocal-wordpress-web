<?php
// if($_SERVER['REMOTE_ADDR'] == '122.173.36.10'){
//   wp_set_current_user( 33 );
// }
// Theme support options
require_once(get_template_directory() . '/assets/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory() . '/assets/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory() . '/assets/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory() . '/assets/functions/menu.php');
require_once(get_template_directory() . '/assets/functions/menu-walkers.php');

// Register sidebars/widget areas
require_once(get_template_directory() . '/assets/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory() . '/assets/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory() . '/assets/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory() . '/assets/translation/translation.php');

// Adds customization to shop pages
require_once( get_template_directory(). '/assets/functions/zl_shop_pages.php');
require_once( get_template_directory(). '/assets/functions/zl-event-dashboard.php');

// Miscellaneous membership functions

require_once( get_template_directory(). '/assets/functions/pricing-tables.php');




// only need these if performing outside of admin environment
require_once(ABSPATH . 'wp-admin/includes/media.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php'); 
// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 
// Use this as a template for custom post types
// require_once(get_template_directory().'/assets/functions/custom-post-type.php');
// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php'); 
// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php');


/*
 * Add to cart route directly to checkout
 *
 * */

add_filter ('woocommerce_add_to_cart_redirect', 'redirect_to_checkout');
function redirect_to_checkout() {
	global $woocommerce;
	if(isset($_REQUEST['add-to-cart'])){

	}
	$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_REQUEST['add-to-cart'] ) );
	if( has_term( 'zl_membership', 'product_type', $product_id ) ){
		$checkout_url = get_permalink(get_option('woocommerce_cart_page_id'));
		return $checkout_url;
	};
}

/**
 * Zoom Local: Admin Area
 */
// Remove Admin bar logo from wp-admin area
function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}

add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

// Register Script


/**
 * Zoom Local: Custom Registration
 * Part 1 of 3 is the custom template found under <TEMPLATE>/woocommerce/myaccount/form-login.php
 */
/**
 * Zoom Local: Custom Registration
 * Part 2 of 3 is field validation.
 */



function listings_woocommerce_registration_errors( $validation_error, $username, $password ) {
	//if registration is from vendor-signup page then use different required fields fieldsg
	if ( $_POST['_wp_http_referer'] === "/vendor-signup/" ) {
		if ( ! isset( $_POST['first_name'] ) || empty( $_POST['first_name'] ) ) {
			$validation_error->add( 'error', 'Please enter your First name' );
		} elseif ( ! isset( $_POST['last_name'] ) || empty( $_POST['last_name'] ) ) {
			$validation_error->add( 'error', 'Please enter your Last name' );
		} elseif ( ! isset( $_POST['vendor-address1'] ) || empty( $_POST['vendor-address1'] ) ) {
			$validation_error->add( 'error', 'Please enter your address' );
		} elseif ( ! isset( $_POST['vendor-city'] ) || empty( $_POST['vendor-city'] ) ) {
			$validation_error->add( 'error', 'Please enter your city' );
		} elseif ( ! isset( $_POST['vendor-state'] ) || empty( $_POST['vendor-state'] ) ) {
			$validation_error->add( 'error', 'Please select your state' );
		} elseif ( ! isset( $_POST['vendor-zip'] ) || empty( $_POST['vendor-zip'] ) ) {
			$validation_error->add( 'error', 'Please enter your Zip' );
		} elseif ( ! isset( $_POST['vendor-telephone'] ) || empty( $_POST['vendor-telephone'] ) ) {
			$validation_error->add( 'error', 'Please enter your phone' );
		} elseif ( ! isset( $_POST['vendor-ein'] ) || empty( $_POST['vendor-ein'] ) ) {
			$validation_error->add( 'error', 'Please enter company ein' );
		} elseif ( ! isset( $_POST['vendor-email'] ) || empty( $_POST['vendor-email'] ) ) {
			$validation_error->add( 'error', 'Please enter company email' );
		} elseif ( ! isset( $_POST['vendor-name'] ) || empty( $_POST['vendor-name'] ) ) {
			$validation_error->add( 'error', 'Please enter company email' );
		}

	} else {
		if ( ! isset( $_POST['first_name'] ) || empty( $_POST['first_name'] ) ) {
			$validation_error->add( 'error', 'Please enter your First name' );
		} elseif ( ! isset( $_POST['last_name'] ) || empty( $_POST['last_name'] ) ) {
			$validation_error->add( 'error', 'Please enter your Last name' );
		} elseif ( ! isset( $_POST['billing_address_1'] ) || empty( $_POST['billing_address_1'] ) ) {
			$validation_error->add( 'error', 'Please enter your address' );
		} elseif ( ! isset( $_POST['billing_city'] ) || empty( $_POST['billing_city'] ) ) {
			$validation_error->add( 'error', 'Please enter your city' );
		} elseif ( ! isset( $_POST['billing_state'] ) || empty( $_POST['billing_state'] ) ) {
			$validation_error->add( 'error', 'Please select your state' );
		} elseif ( ! isset( $_POST['billing_postcode'] ) || empty( $_POST['billing_postcode'] ) ) {
			$validation_error->add( 'error', 'Please enter your Zip' );
		} elseif ( ! isset( $_POST['billing_phone'] ) || empty( $_POST['billing_phone'] ) ) {
			$validation_error->add( 'error', 'Please enter your phone' );
		}
	}
		if(isset( $_POST['vendor-name']) ){
		$term       = sanitize_text_field( $_POST['vendor-name'] );
		$duplicated = yith_wcpv_check_duplicate_term_name( $term, YITH_Vendors()->get_taxonomy_name() );
		if ( $duplicated ) {
			$validation_error->add( 'error', 'Store Name is Already Taken' );
		}

	}
	return $validation_error;
}
add_filter('woocommerce_register_post', 'listings_woocommerce_registration_errors', 10, 4);

/**
 * Zoom Local: Custom Registration
 * Part 3 of 3 is saving the values to the db.
 */
function listings_woocommerce_registration_save($user_id) {

	// First name
	if ( isset( $_POST['first_name'] ) ) {
		update_user_meta( $user_id, 'first_name', $_POST['first_name'] ); // saves to WP first_name
		update_user_meta( $user_id, 'billing_first_name', $_POST['first_name'] ); // saves to WC billing_firstname
	}

	// Last name
	if ( isset( $_POST['last_name'] ) ) {
		update_user_meta( $user_id, 'last_name', $_POST['last_name'] ); // saves to WP last name
		update_user_meta( $user_id, 'billing_last_name', $_POST['last_name'] ); // saves to WC billing_last_name
	}

	// Email
	if ( isset( $_POST['email'] ) ) {
		update_user_meta( $user_id, 'billing_email', $_POST['email'] ); // saves to WC billing_email
	}

	// Address 1
	if ( isset( $_POST['billing_address_1'] ) ) {
		update_user_meta( $user_id, 'billing_address_1', $_POST['billing_address_1'] ); // saves to WC billing_address_1
	}

	// Address 2
	if ( isset( $_POST['billing_address_2'] ) ) {
		update_user_meta( $user_id, 'billing_address_2', $_POST['billing_address_2'] ); // saves to WC billing_address_2
	}

	// City
	if ( isset( $_POST['billing_city'] ) ) {
		update_user_meta( $user_id, 'billing_city', $_POST['billing_city'] ); // saves to WC billing_city
	}

	// State
	if ( isset( $_POST['billing_state'] ) ) {
		update_user_meta( $user_id, 'billing_state', $_POST['billing_state'] ); // saves to WC billing_state
	}

	// Zip
	if ( isset( $_POST['billing_postcode'] ) ) {
		update_user_meta( $user_id, 'billing_postcode', $_POST['billing_postcode'] ); // saves to WC billing_postcode
	}

	// Phone
	if ( isset( $_POST['billing_phone'] ) ) {
		update_user_meta( $user_id, 'billing_phone', $_POST['billing_phone'] ); // saves to WC billing_phone
	}

	// Category Preference
	if ( isset( $_POST['cat_group'] ) ) {
		update_user_meta( $user_id, 'category_preference', wp_json_encode( $_POST['cat_group'] ) ); // saves the category as JSON format
	}

	// Location Preference
	if ( isset( $_POST['loc_group'] ) ) {
		update_user_meta( $user_id, 'location_preference', wp_json_encode( $_POST['loc_group'] ) ); // saves the location preference as JSON format
	}

	// Delivery Preference
	if (isset($_POST['del_group'])) {
		update_user_meta($user_id, 'delivery_preference', wp_json_encode($_POST['del_group'])); // saves the delivery preference as JSON format
	}
    //Save fields if register from vendor signup page
    if ( $_POST['_wp_http_referer'] === "/vendor-signup/" ) {
 
        $store_name     = ! empty( $_POST['vendor-name'] ) ? sanitize_text_field( $_POST['vendor-name'] ) : '';
        $store_ein      = ! empty( $_POST['vendor-ein'] ) ? sanitize_text_field( $_POST['vendor-ein'] ) : '';
        $store_address1 = ! empty( $_POST['vendor-address1'] ) ? sanitize_text_field( $_POST['vendor-address1'] ) : '';
        update_user_meta( $user_id, 'billing_address_1', $store_address1 );
        $store_address2 = ! empty( $_POST['vendor-address2'] ) ? sanitize_text_field( $_POST['vendor-address2'] ) : '';
        update_user_meta( $user_id, 'billing_address_2', $store_address2 );
        $store_city     = ! empty( $_POST['vendor-city'] ) ? sanitize_text_field( $_POST['vendor-city'] ) : '';
        update_user_meta( $user_id, 'billing_city', $store_city );
        $store_state    = ! empty( $_POST['vendor-state'] ) ? sanitize_text_field( $_POST['vendor-state'] ) : '';
        update_user_meta( $user_id, 'billing_state', $store_state );
        $store_zip      = ! empty( $_POST['vendor-zip'] ) ? sanitize_text_field( $_POST['vendor-zip'] ) : '';
        update_user_meta( $user_id, 'billing_postcode', $store_zip );
// location is what YITH uses for a combined address in the front page.
        $store_location  = ! empty( $_POST['vendor-location'] ) ? sanitize_text_field( $_POST['vendor-location'] ) : '';
        $store_telephone = ! empty( $_POST['vendor-telephone'] ) ? sanitize_text_field( $_POST['vendor-telephone'] ) : '';
        update_user_meta( $user_id, 'billing_phone', $store_telephone );
        $store_website   = ! empty( $_POST['vendor-website'] ) ? sanitize_text_field( $_POST['vendor-website'] ) : '';
// sanitize_email will record an empty value if it is not an emaill address.
        $store_email = ! empty( $_POST['vendor-email'] ) ? sanitize_email( $_POST['vendor-email'] ) : '';
        update_user_meta( $user_id, 'billing_email', $store_email );
 
        $term_info = wp_insert_term( $store_name, YITH_Vendors()->get_taxonomy_name() );
        if ( is_wp_error( $term_info ) ) {
            return;
        }/* Get the new vendor */
        $vendor      = yith_get_vendor( $term_info['term_id'] );
        $customer_id = $user_id;
 
        /* Set the vendor information by magic method */
        $vendor->owner                 = $customer_id;
        $vendor->location              = $store_location;
        $vendor->telephone             = $store_telephone;
        $vendor->store_email           = $store_email;
        $vendor->registration_date     = current_time( 'mysql' );
        $vendor->registration_date_gmt = current_time( 'mysql', 1 );
        $vendor->enable_selling        = 'no';
        $vendor->pending               = 'yes';
 
        /*Custom variables*/
        $vendor->ein           = $store_ein;
        $vendor->address1      = $store_address1;
        $vendor->address2      = $store_address2;
        $vendor->city          = $store_city;
        $vendor->state         = $store_state;
        $vendor->zip           = $store_zip;
        $vendor->store_website = $store_website;
 
        /* Set Owner */
        update_user_meta( $customer_id, YITH_Vendors()->get_user_meta_owner(), $vendor->id );
        update_user_meta( $customer_id, YITH_Vendors()->get_user_meta_key(), $vendor->id );
 
        yith_wcpv_add_vendor_caps( $customer_id );
 
        do_action( 'yith_new_vendor_registration', $vendor );
    }
}
//Begin BBrewer Add for Merge
//add_action('user_register', 'listings_woocommerce_registration_save', 10, 1);
//End BBrewer Add for Merge
add_action('woocommerce_created_customer', 'listings_woocommerce_registration_save', 20, 1);
add_action('woocommerce_save_account_details', 'listings_woocommerce_registration_save', 20, 1);
/**
 * Zoom Local: Custom Vendor Registration
 */
/// saving the values to the db
function zl_add_extra_termmeta_to_vendor($vendor) {
    // These are the extra fields we are adding for vendors
    //vendor-ein
    //vendor-address1
    //vendor-address2
    //vendor-city
    //vendor-state
    //vendor-zip
    //vendor-website
    $vendor->ein = $_POST['vendor-ein'];
    $vendor->address1 = $_POST['vendor-address1'];
    $vendor->address2 = $_POST['vendor-address2'];
    $vendor->city = $_POST['vendor-city'];
    $vendor->state = $_POST['vendor-state'];
    $vendor->zip = $_POST['vendor-zip'];
    $vendor->website = $_POST['vendor-website'];
    return $vendor;
}
add_filter('yith_create_vendor', 'zl_add_extra_termmeta_to_vendor');


/**
 * Zoom Local: Enable PHP from Text Widget
 */
function php_execute($html) {
    if (strpos($html, "<" . "?php") !== false) {
        ob_start();
        eval("?" . ">" . $html);
        $html = ob_get_contents();
        ob_end_clean();
    }
    return $html;
}
add_filter('widget_text', 'php_execute', 100);

/**
 * Zoom Local: Remove WP Admin bar
 */
add_filter('show_admin_bar', '__return_false');

//add_action('init', 'block_wpadmin');
//function block_wpadmin() {
//    $file = basename($_SERVER['PHP_SELF']);
//    if ($file == 'wp-login.php' || is_admin() && !current_user_can('edit_posts') && $file != 'admin-ajax.php'){
//        wp_redirect( home_url() );
//        exit();
//    }
//}

/**
 * Zoom Local: AJAX Login
 */
// Execute the action only if the user isn't logged in
function ajax_login_init() {

    wp_register_script('ajax-login-script', get_template_directory_uri() . '/assets/js/ajax-login-script.js', array('jquery'));
    wp_enqueue_script('ajax-login-script');

    wp_localize_script('ajax-login-script', 'ajax_login_object', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'redirecturl' => home_url(),
        'loadingmessage' => __('Sending user info, please wait...')
    ));

    // Enable the user with no privileges to run ajax_login() in AJAX
    add_action('wp_ajax_nopriv_ajaxlogin', 'ajax_login');
}
if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
}

function ajax_login() {

    // First check the nonce, if it fails the function will break
    check_ajax_referer('ajax-login-nonce', 'security');

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon($info, false);
    if (is_wp_error($user_signon)) {
        echo json_encode(array('loggedin' => false, 'message' => __('Wrong username or password.')));
    } else {
        echo json_encode(array('loggedin' => true, 'message' => __('Login successful, redirecting...')));
    }

    die();
}

/**
 * Zoom Local: WooCommerce Integration
 */
// @todo move everything to separate files/modules
//require_once(get_template_directory() . '/woocommerce/functions/shop.php');
require_once(get_template_directory() . '/woocommerce/functions/vendor_sign_up.php');
// unhook the WooCommerce wrappers
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

// hook in JointsWP wrappers 
function jointswp_theme_wrapper_start() {
    /*echo '<div id="content"><div id="inner-content" class="row"><div id="main" class="large-12 medium-8 small-centered columns" role="main">';*/
    //echo '<div id="content"><div id="inner-content" class="row"><div id="main" class="large-12 columns" role="main">';
    echo '<div id="content"><div id="inner-content"><div id="main" class="large-12" role="main">';
}

add_action('woocommerce_before_main_content', 'jointswp_theme_wrapper_start', 10);

function jointswp_theme_wrapper_end() {
    echo '</div></div></div>';
}

add_action('woocommerce_after_main_content', 'jointswp_theme_wrapper_end', 10);

// declare woocommerce support
function woocommerce_support() {
    add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'woocommerce_support');

// remove woocommerce style to prevent future conflicts
add_filter('woocommerce_enqueue_styles', '__return_false');

// remove "Add to Cart" buttons
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action('woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30);
remove_action('woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30);

function custom_json_api_prepare_post( $post_response, $post, $context ) {
 
    /**
     * This code brings in the WooCommerce data.
     */
    $meta = get_post_meta( $post['ID'] );
    $post_response['meta_data'] = $meta;
    
    /**
     * This code brings in the Yith Multi-Vendor data.
     */
    $vendor = yith_get_vendor( $post['ID'], 'product' );
    
    $vendor_obj;
    
    $vendorName = $vendor->name;
    if (!empty($vendorName)) {
        $vendor_obj['_name'] = $vendorName;
    }
    
    $vendorLocation = $vendor->location;
    if (!empty($vendorLocation)) {
        $vendor_obj['_location'] = $vendorLocation;
    }

    $vendorTelephone = $vendor->telephone;
    if (!empty($vendorTelephone)) {
        $vendor_obj['_telephone'] = $vendorTelephone;
    }
    
    $vendorEmail = $vendor->store_email;
    if (!empty($vendorEmail)) {
        $vendor_obj['_email'] = $vendorEmail;
    }
    
    $vendorHours = $vendor->hours;
    if (!empty($vendorHours)) {
        $vendor_obj['_hours'] = $vendorHours;
    }
    
    $vendorWebsite = $vendor->website;
    if (!empty($vendorWebsite)) {
        $vendor_obj['_website'] = $vendorWebsite;
    }
    
    $post_response['vendor_data'] = $vendor_obj;

    return $post_response;
}
add_filter( 'json_prepare_post', 'custom_json_api_prepare_post', 10, 3 );

function custom_json_prepare_term( $data, $term, $context ) {
 
//        global $wp_query;
//        $route = $wp_query->query['json_route'];
//        if ( ! preg_match( '/(terms\/.+)/', $route) )
//              return $data;
// 
//        $args = array(
//                'tax_query' => array(
//                        array(
//                                'taxonomy' => $term->taxonomy,
//                                'field' => 'slug',
//                                'terms' => $term->slug
//                        )
//                ),
//                'posts_per_page' => 5
//        );
//        $posts = get_posts( $args );
// 
//        $posts_arr = array();
//        foreach ( $posts as $p ) {
//                $posts_arr[] = array(
//                        'ID' => $p->ID,
//                        'title' => $p->post_title
//                        );
//        }
// 
//        $data['posts'] = $posts_arr;
 
        $vendor = yith_get_vendor( $term );
        
        if ($vendor->enable_selling === 'yes') {
            $data['owner'] = $vendor->owner;

            $data['location'] = $vendor->location;

            $data['telephone'] = $vendor->telephone;

            $data['store_email'] = $vendor->store_email;

            $data['enable_selling'] = $vendor->enable_selling;

            $data['ein'] = $vendor->ein;

            $data['address1'] = $vendor->address1;

            $data['address2'] = $vendor->address2;

            $data['city'] = $vendor->city;

            $data['state'] = $vendor->state;

            $data['zip'] = $vendor->zip;

            $data['website'] = $vendor->website;

            $data['header_image'] = $vendor->header_image;

            $data['socials'] = $vendor->socials;
        }
        
        return $data;
}
add_filter( 'json_prepare_term', 'custom_json_prepare_term', 10, 3 );

//DATA FOR LISTINGS

/* Add User Category preference in dashboard*/
add_action( 'show_user_profile', 'zoomlocal_user_category_admin', 20 );
add_action( 'edit_user_profile', 'zoomlocal_user_category_admin', 20 );

function zoomlocal_user_category_admin( $user_id ) {
    $category_preference = json_decode( get_user_meta( $user_id->ID, 'category_preference', true ) );
	if($category_preference == null){$category_preference=array();}
    ?>
    <table class="form-table">
        <tbody>
            <tr>
                <th>
                    <label for="category_preference">Categories</label>
                </th>
                <td>
                    <div class="large-12 columns">
                        <div class="menu-categs-box">
                            <?php $wcatTerms = get_terms( 'product_cat', array(
                                'hide_empty' => 0,
                                'orderby'    => 'ASC',
                                'parent'     => 0
                            ) );
                            foreach ( $wcatTerms as $wcatTerm ) :
                                ?>
                                <ul>
                                    <li>
                                        <span><?php echo $wcatTerm->name; ?></span>
                                        <div id="<?php echo $wcatTerm->slug; ?>">
                                            <ul>
                                                <?php $wsubargs = array(
                                                    'hierarchical'     => 1,
                                                    'show_option_none' => '',
                                                    'hide_empty'       => 0,
                                                    'parent'           => $wcatTerm->term_id,
                                                    'taxonomy'         => 'product_cat'
                                                );
                                                $wsubcats       = get_categories( $wsubargs );?>
                                                <div>
                                                    <div>
                                                        <?php foreach ( $wsubcats as $wsc ): ?>
                                                            <div><input name="cat_group[]"
                                                                                                value="<?php echo $wsc->slug; ?>"
                                                                                                type="checkbox"  <?php echo in_array( $wsc->slug, $category_preference ) ? "checked" : ""; ?> ><label
                                                                    for="cat_group[]">
                                                                    <?php echo $wsc->name; ?></label></div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
<?php
}

/* Add User Location preference in dashboard*/
add_action( 'show_user_profile', 'zoomlocal_user_location_admin', 20 );
add_action( 'edit_user_profile', 'zoomlocal_user_location_admin', 20 );

function zoomlocal_user_location_admin( $user_id ) {
    $location_preference = json_decode( get_user_meta( $user_id->ID, 'location_preference', true ) );
    if($location_preference==null){$location_preference = array();}
    ?>
    <table class="form-table">
        <tbody>
        <tr>
            <th><label for="location_preference">Location Preference</label></th>
            <td>
                <div>
                    <ul>
                        <?php
                        $location_list  = json_decode( file_get_contents( get_template_directory() . '/assets/json/locations.json' ) );
                        $location_array = array();
                        foreach ( $location_list as $location ) {
                            if ( ! in_array( $location->Group, $location_array ) ) {
                                $location_array[] = $location->Group;
                            }
                        }//end foreach
                        foreach ( $location_array as $key ) {
                            ?>
                            <li><input name="loc_group[]" value="<?php echo $key; ?>"
                                       type="checkbox" <?php echo in_array( $key, $location_preference ) ? "checked" : ""; ?> >
                                <label for="loc_group[]"><?php echo $key; ?></label>
                            </li>
                        <?php }//end foreach ?>
                    </ul>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
<?php
}

/* Add User Delivery preference in dashboard*/
add_action( 'show_user_profile', 'zoomlocal_user_delivery_admin', 20 );
add_action( 'edit_user_profile', 'zoomlocal_user_delivery_admin', 20 );

function zoomlocal_user_delivery_admin( $user_id ) {
    $delivery_preference = json_decode( get_user_meta( $user_id->ID, 'delivery_preference', true ) );
    if($delivery_preference==null){$delivery_preference=array();}
    ?>
    <table class="form-table">
        <tbody>
            <tr>
                <th><label for="delivery_preference">Listing Delivery</label></th>
                <td>
                    <span>Select how listings are delivered.</span>
                    <ul>
                        <li>
                            <input name="del_group[]" value="daily-email"
                                   type="checkbox" <?php echo in_array( "daily-email", $delivery_preference ) ? "checked" : ""; ?>  >
                            <label for="del_group[]">Daily Email</label>
                        </li>
                        <li>
                            <input name="del_group[]" value="mobile-app"
                                   type="checkbox" <?php echo in_array( "mobile-app", $delivery_preference ) ? "checked" : ""; ?> >
                            <label for="del_group[]">Mobile App</label>
                        </li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
<?php
}

/*Saves User Profile from Dashboard*/
add_action( 'personal_options_update', 'listings_woocommerce_registration_save' );
add_action( 'edit_user_profile_update', 'listings_woocommerce_registration_save' );


add_action('init','add_zl_endpoints');
function add_zl_endpoints(){
    add_rewrite_endpoint( 'manage-shop', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'categories', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'locations', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'shop-list', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'events-list', EP_ROOT | EP_PAGES );
}

//manage vendor capabilites - used in manage-shop
function zl_manage_vendor_caps( $user_id = 0, $method, $caps = '' ) {
    if ( $user_id > 0 && ( 'remove' == $method || 'add' == $method ) ) {

        $method .= '_cap';
        $user = new WP_User( $user_id );

        if ( '' == $caps ) {
            $caps = YITH_Vendors()->vendor_enabled_capabilities();
            if( 'remove' == $method ){
                $caps[] = 'publish_products';
            }
        } elseif(  is_string( $caps )  ) {
        $caps = array( $caps );
        }

        foreach ( $caps as $cap ) {
            $user->$method( $cap );
        }
    }
}

/** This function provides the roles associated with the user */
function get_user_role() {
    global $current_user;
    $user_roles = $current_user->roles;
    $user_role = array_shift($user_roles);
    return $user_role;
}

/** Determines if vendor is logged in */
function is_vendor() {
    if (get_user_role() == "shop_manager"){
        return true;
    }else{
        return false;
    }
}

/** Determines if administrator is logged in */
function is_administrator() {
    if (get_user_role() == "administrator"){
        return true;
    }else{
        return false;
    }
}
/*********************** PR START ***********************/
//Handles request made from listings dashboard
function get_data_callback()
{
    global $wpdb; //variable use to interact and perform queries to the db
    $userId = get_current_user_id();
    $type = $_POST["param"];
    $postType = $_POST["type"];
    //$postType='Product';
    $prefix = $wpdb->prefix;
    $vendor = yith_get_vendor( 'current', 'user' );
    get_currentuserinfo();
    $vendorId = $vendor->term_taxonomy_id;
    switch ($type)
    {
        case "promoted":
                $parentArray = Array();
                //Get product data from wp tables, as promoted listings
                $queryRecentListings = "SELECT DISTINCT P.ID AS 'id', P.post_title AS 'productName',
                DATE_FORMAT(P.post_date, '%m/%d/%Y') AS 'listingDate',   
                (CASE (SELECT meta_value FROM ".$prefix."postmeta where post_id = P.ID AND meta_key = '_stock_status' )
                WHEN 'instock'  THEN 'Not' ELSE 'Yes ' END) AS 'soldOut',
                (SELECT meta_value FROM ".$prefix."postmeta where post_id = P.ID AND meta_key = '_sku') AS 'sku',
                (SELECT T.name FROM ".$prefix."term_relationships as TR 
                INNER JOIN ".$prefix."term_taxonomy TT ON TR.term_taxonomy_id = TT.term_taxonomy_id
                INNER JOIN ".$prefix."terms T ON T.term_id = TT.term_id
                where TR.object_id = P.ID AND TT.taxonomy = 'product_cat'
                LIMIT 1) AS 'category'
                FROM ".$prefix."posts AS P
                INNER JOIN ".$prefix."postmeta AS PM ON P.id = PM.post_id
                INNER JOIN ".$prefix."term_relationships as TR ON TR.object_id = P.ID
                WHERE P.post_status = 'publish' AND P.post_type ='product' 
                      AND PM.meta_key = '_Featured' AND PM.meta_value = 'yes' 
                      AND TR.term_taxonomy_id = (select term_id from ".$prefix."terms where name = '".$postType."')
                      AND P.post_author IN(SELECT DISTINCT user_id 
                                            FROM ".$prefix."usermeta
                                            WHERE meta_key = 'yith_product_vendor_owner' AND
                                            meta_value = $vendorId)";
                $wpdb->show_errors(true);
                $RecentListingsRows = $wpdb->get_results( $queryRecentListings,OBJECT);
                //Creating array(array(),array(), array()) that is the acceptance format to 
                //create a valide json for the grids population

                foreach($RecentListingsRows as $key => $row)
                {
                    $parentArray[] = array("id"=>$row->id, 
                                        "ProductName"=>$row->productName, 
                                        "ProductCode"=>$row->sku, 
                                        "Category"=>$row->category, 
                                        "ListingDate"=>$row->listingDate, 
                                        "SoldOut"=>$row->soldOut
                                        );
                }

                wp_send_json_success($parentArray);
        break;

        case "NoPromoted":
        	$parentArray = Array();
                //Get product data from wp tables, as no promoted listings
                $queryRecentListings = "SELECT DISTINCT P.ID AS 'id', P.post_title AS 'productName',
                DATE_FORMAT(P.post_date, '%m/%d/%Y') AS 'listingDate',   
                (CASE (SELECT meta_value FROM ".$prefix."postmeta where post_id = P.ID AND meta_key = '_stock_status' )
                WHEN 'instock'  THEN 'Not' ELSE 'Yes ' END) AS 'soldOut',
                (SELECT meta_value FROM ".$prefix."postmeta where post_id = P.ID AND meta_key = '_sku') AS 'sku',
                (SELECT T.name FROM ".$prefix."term_relationships as TR 
                INNER JOIN ".$prefix."term_taxonomy TT ON TR.term_taxonomy_id = TT.term_taxonomy_id
                INNER JOIN ".$prefix."terms T ON T.term_id = TT.term_id
                where TR.object_id = P.ID AND TT.taxonomy = 'product_cat'
                LIMIT 1) AS 'category'
                FROM ".$prefix."posts AS P
                INNER JOIN ".$prefix."postmeta AS PM ON P.id = PM.post_id
                INNER JOIN ".$prefix."term_relationships as TR ON TR.object_id = P.ID
                WHERE P.post_status = 'publish' AND P.post_type ='product' 
                      AND PM.meta_key = '_Featured' AND PM.meta_value = 'no' 
                      AND TR.term_taxonomy_id = (select term_id from ".$prefix."terms where name = '".$postType."')
                      AND P.post_author IN(SELECT DISTINCT user_id 
                                            FROM ".$prefix."usermeta
                                            WHERE meta_key = 'yith_product_vendor_owner' AND
                                            meta_value = $vendorId)";
                $wpdb->show_errors(true);
                $RecentListingsRows = $wpdb->get_results( $queryRecentListings,OBJECT);
                //Creating array(array(),array(), array()) that is the acceptance format to 
                //create a valide json for the grids population

                foreach($RecentListingsRows as $key => $row)
                {
                    $parentArray[] = array("id"=>$row->id, 
                                        "ProductName"=>$row->productName, 
                                        "ProductCode"=>$row->sku, 
                                        "Category"=>$row->category, 
                                        "ListingDate"=>$row->listingDate, 
                                        "SoldOut"=>$row->soldOut
                                        );
                }
                wp_send_json_success($parentArray);
        break; 

        case "updateListingDate":
            //update post_date when refresh row button
            $id = $_POST["id"];
            $status = $_POST["status"];
            $listingDate = $_POST["post_date"];
            if($listingDate == ""){

               $query = "UPDATE ".$prefix."posts
                       SET post_date = now() 
                       WHERE post_status = 'publish' AND post_author = $userId AND ID = $id";
            }else{
                
                $query = "UPDATE ".$prefix."posts
                       SET post_date = STR_TO_DATE('$listingDate','%m/%d/%Y')
                        WHERE post_status = 'publish' AND post_author = $userId AND ID = $id";
            }
            
            $wpdb->show_errors(true);
            $value = $wpdb->query($query);

            if($value != false){
                $newCreditsValue = decreaseCredits();
                $response = array( 'success' => true, 'data' =>  true, 'totalCredits' => $newCreditsValue);
            }
            else{$response = array( 'success' => false, 'data' => $wpdb->last_error);}
            wp_send_json($response);
        break;

        case "updateToPromoted":
            //update post_date when refresh row button and update feature value to yes
            $id = $_POST["id"];
            $status = $_POST["status"];
            $listingDate = $_POST["post_date"];
            $wpdb->show_errors(true);
            if($listingDate == ""){

               $query = "UPDATE ".$prefix."posts
                       SET post_date = now() 
                       WHERE post_status = 'publish' AND post_author = $userId AND ID = $id";
            }else{
                
                $query = "UPDATE ".$prefix."posts
                       SET post_date = STR_TO_DATE('$listingDate','%m/%d/%Y')
                        WHERE post_status = 'publish' AND post_author = $userId AND ID = $id";
            }
            $value = $wpdb->query($query);

            $queryTwo = "UPDATE ".$prefix."postmeta 
                         SET meta_value = 'yes'
                         WHERE post_id = $id AND meta_key = '_featured'";
            $value = $wpdb->query($queryTwo);


            if($value != false){
                $newCreditsValue = decreaseCredits();
                $response = array( 'success' => true, 'data' =>  true, 'totalCredits' => $newCreditsValue);
            }
            else{$response = array( 'success' => false, 'data' => $wpdb->last_error);}
            wp_send_json($response);
        break; 

        case "DisplayCredits":
                //get total credits per loged user 
                $totalCredits = 00;
                $query = "SELECT sum(total_credits) AS 'total_credits' FROM ".$prefix."zoomlocal_credits 
                        WHERE expiration_date >= CURDATE() AND vendor_id = $vendorId";
                $value = $wpdb->query($query);
                foreach( $wpdb->get_results($query) as $key => $row) {
                    $totalCredits = $row->total_credits;
                }

                if($totalCredits == 0 || $totalCredits == null){$totalCredits = 0;}
                if($value != false){$response = array( 'success' => true, 'data' => $totalCredits);}
                else{$response = array( 'success' => false, 'data' => $wpdb->last_error);}
                wp_send_json($response);
        break;  
        
    }
}
add_action( 'wp_ajax_get_data', 'get_data_callback' );//action, method name from up 
add_action('wp_ajax_nopriv_get_data', 'get_data_callback');

//Handles request made from create listings
function create_listings_request_callback()
{
    //variable use to interact and perform queries to the db
    global $wpdb; 
    $userId = get_current_user_id();
    $type = $_POST["param"];
    $prefix = $wpdb->prefix;
    $vendor = yith_get_vendor( 'current', 'user' );
    get_currentuserinfo();
    $vendorId = $vendor->term_taxonomy_id;
    switch ($type)
    {
        case "get_categories":
             createCategoryTable($_POST["productId"]);


        break;
        case "insert_product":
            $title = $_POST["post_title"];
            $description = $_POST["post_content"];
            $date = date('Y-m-d', strtotime($_POST["post_date"]));
            $qty = $_POST["quantity"];
            $price =  $_POST["price"];
            $category =  $_POST["category"];
            if($_POST["facebook_check"] == true){$facebook = "yes";}else{$facebook = "no";}
            if($_POST["twitter_check"] == true){$twitter = "yes";}else{$twitter = "no";}
            if($_POST["instagram_check"] == true){$instagram = "yes";}else{$instagram = "no";}
            
            insertProduct("product", $title, $description, $date, "", $qty, $price, $category, $facebook,
                $twitter, $instagram, true, 0, "");
            
        break;
        case "update_product":
            $title = $_POST["post_title"];
            $description = $_POST["post_content"];
            $date = date('Y-m-d', strtotime($_POST["post_date"]));
            $qty = $_POST["quantity"];
            $price =  $_POST["price"];
            $category =  $_POST["category"];
            $productId =  $_POST["productId"];
            if($_POST["facebook_check"] == true){$facebook = "yes";}else{$facebook = "no";}
            if($_POST["twitter_check"] == true){$twitter = "yes";}else{$twitter = "no";}
            if($_POST["instagram_check"] == true){$instagram = "yes";}else{$instagram = "no";}

            insertProduct("product", $title, $description, $date,"", $qty, $price, $category, 
                           $facebook, $twitter, $instagram, false, $productId, "");
            
        break;
        case "delete_record":
                $id = $_POST["id"];
                deleteProduct($id);
        break;
        case "insert_event":
            $title = $_POST["post_title"];
            $description = $_POST["post_content"];
            $startDate = date('Y-m-d', strtotime($_POST["eventStart_date"]));
	        $endDate = $startDate;
			if($_POST["eventEnd_date"]<>""){
				$endDate = date('Y-m-d', strtotime($_POST["eventEnd_date"]));
			}
            $qty = $_POST["quantity"];
            $price =  $_POST["price"];
            $address =  $_POST["address"];
            $category =  $_POST["category"];
            if($_POST["facebook_check"] == true){$facebook = "yes";}else{$facebook = "no";}
            if($_POST["twitter_check"] == true){$twitter = "yes";}else{$twitter = "no";}
            if($_POST["instagram_check"] == true){$instagram = "yes";}else{$instagram = "no";}

            insertProduct("event", $title, $description, $startDate, $endDate, $qty, $price, $category,$facebook, $twitter, $instagram, true, 0, $address);
        break;
        case "update_event":
            $title = $_POST["post_title"];
            $description = $_POST["post_content"];
            $startDate = date('Y-m-d', strtotime($_POST["eventStart_date"]));
	        $endDate = $startDate;
	        if($_POST["eventEnd_date"]<>""){
		        $endDate = date('Y-m-d', strtotime($_POST["eventEnd_date"]));
	        }
            $qty = $_POST["quantity"];
            $price =  $_POST["price"];
            $address =  $_POST["address"];
            $category =  $_POST["category"];
            $productId =  $_POST["productId"];
            if($_POST["facebook_check"] == true){$facebook = "yes";}else{$facebook = "no";}
            if($_POST["twitter_check"] == true){$twitter = "yes";}else{$twitter = "no";}
            if($_POST["instagram_check"] == true){$instagram = "yes";}else{$instagram = "no";}

            insertProduct("event", $title, $description, $startDate, $endDate, $qty, $price, $category, $facebook ,$twitter ,$instagram, false, $productId, $address);
            
        break;
        case "load_product":
            $productId = $_POST["productId"];
            $type = $_POST["type"];
            $productQuery = "SELECT DISTINCT 
                            P.ID AS 'id',
                            P.post_excerpt AS 'description',
                            P.post_status AS 'status',
                            P.post_title AS 'title',
                            P.post_name AS 'slug',
                            (SELECT PM.meta_value FROM ".$prefix."postmeta AS PM WHERE PM.post_id = P.ID 
                            AND PM.meta_key = '_rq_event_start_date') AS 'startDate',
                            (SELECT PM.meta_value FROM ".$prefix."postmeta AS PM WHERE PM.post_id = P.ID 
                            AND PM.meta_key = '_rq_event_end_date') AS 'endDate',
                            DATE_FORMAT(P.post_date, '%m/%d/%Y') AS 'listingDate',
                            (SELECT PM.meta_value 
                            FROM ".$prefix."postmeta AS PM 
                            WHERE PM.post_id = P.ID AND PM.meta_key = '_stock') AS 'qty',
                            (SELECT PM.meta_value 
                            FROM ".$prefix."postmeta AS PM 
                            WHERE PM.post_id = P.ID AND PM.meta_key = '_regular_price') AS 'regularPrice',
                            (SELECT PM.meta_value
                            FROM ".$prefix."postmeta AS PM 
                            WHERE PM.post_id = P.ID AND PM.meta_key = '_sale_price') AS 'salesPrice'
                            FROM ".$prefix."posts AS P
                            INNER JOIN ".$prefix."postmeta AS PM ON P.ID = PM.post_id
                            WHERE P.id = $productId AND P.post_status = 'publish' AND P.post_type ='product' 
                            AND P.post_author IN(SELECT DISTINCT user_id FROM ".$prefix."usermeta 
                                                WHERE meta_key = 'yith_product_vendor' OR 
                                                meta_key = 'yith_product_vendor_owner'AND
                                                meta_value = $vendorId)";
            
            $wpdb->show_errors(true);
            $productRows = $wpdb->get_results( $productQuery, OBJECT);
            //Creating array(array(),array(), array()) that is the acceptance format to 
            //create a valide json for the grids population

            foreach($productRows as $key => $row)
            {
                $productArray[] = array("id"=>$row->id, 
                                "description"=>$row->description, 
                                "status"=>$row->status, 
                                "title"=>$row->title, 
                                "listingDate"=>$row->listingDate,
                                "slug"=>$row->slug, 
                                "qty"=>$row->qty,
                                "regularPrice"=>$row->regularPrice,
                                "salesPrice"=>$row->salesPrice,
                                "endDate"=>date('m/d/Y', strtotime($row->endDate)),
                                "startDate"=>date('m/d/Y', strtotime($row->startDate))
                                );
            }


            $queryImage = "SELECT guid FROM ".$prefix."posts 
                    WHERE ID = (SELECT meta_value FROM ".$prefix."postmeta 
                                WHERE post_id = $productId AND meta_key = '_thumbnail_id')";
            $imgArr = $wpdb->get_row( $queryImage, ARRAY_A );
            $imgUrl = $imgArr['guid'];
            $htmlImage = "<div >
                            <img  id=\"previewing\" src=\"$imgUrl\">
                        </div>";
            //address
            $queryAddress = "SELECT meta_value FROM ".$prefix."postmeta 
                            WHERE post_id = $productId AND meta_key = '_rq_event_address_name'";
            $addressVal = $wpdb->get_var( $queryAddress );
            
            $queryRegion = "SELECT meta_value FROM ".$prefix."postmeta 
                            WHERE post_id = $productId AND meta_key = '_rq_event_region_name'";
            $regionVal = $wpdb->get_var( $queryRegion );

            $queryCity = "SELECT meta_value FROM ".$prefix."postmeta 
                            WHERE post_id = $productId AND meta_key = '_rq_event_city_name'";
            $cityVal = $wpdb->get_var( $queryCity );

            $queryZipcode = "SELECT meta_value FROM ".$prefix."postmeta 
                            WHERE post_id = $productId AND meta_key = '_rq_event_zip_code'";
            $zipcodeVal = $wpdb->get_var( $queryZipcode );


            //$resultArr = array_merge($productArray, $categoryRows);
			
	   $response = array( 'success' => true, 'dataProduct' => $productArray,
                             'htmlImage' => $htmlImage, 'address'=> $addressVal,
                             'region'=> $regionVal, 'city'=> $cityVal, 'zipcode'=> $zipcodeVal
                            );


            wp_send_json($response);

        break;
        case "load_matchingItems":
            //Get products data from wp tables, as promoted listings
            $type = $_POST["post_type"];
            $queryRecentListings = "SELECT DISTINCT P.ID AS 'id', P.post_title AS 'productName',
            DATE_FORMAT(P.post_date, '%m/%d/%Y') AS 'listingDate',   
            (CASE (SELECT meta_value FROM ".$prefix."postmeta where post_id = P.ID AND meta_key = '_stock_status' )
            WHEN 'instock'  THEN 'Not' ELSE 'Yes ' END) AS 'soldOut',
            (SELECT meta_value FROM ".$prefix."postmeta where post_id = P.ID AND meta_key = '_sku') AS 'sku',
            (SELECT T.name FROM ".$prefix."term_relationships as TR 
            INNER JOIN ".$prefix."term_taxonomy TT ON TR.term_taxonomy_id = TT.term_taxonomy_id
            INNER JOIN ".$prefix."terms T ON T.term_id = TT.term_id
            WHERE TR.object_id = P.ID AND TT.taxonomy = 'product_cat'
            LIMIT 1) AS 'category'
            FROM ".$prefix."posts AS P
            INNER JOIN ".$prefix."postmeta AS PM ON P.id = PM.post_id
            INNER JOIN ".$prefix."term_relationships as TR ON TR.object_id = P.ID
            WHERE P.post_status = 'publish' AND P.post_type ='product' 
            AND TR.term_taxonomy_id = (select term_id from ".$prefix."terms where name = '".$type."')
            AND P.post_author IN(SELECT DISTINCT user_id FROM ".$prefix."usermeta 
                            WHERE meta_key = 'yith_product_vendor' OR 
                            meta_key = 'yith_product_vendor_owner'AND
                            meta_value = $vendorId)";
            $wpdb->show_errors(true);
            $RecentListingsRows = $wpdb->get_results( $queryRecentListings,OBJECT);
            //Creating array(array(),array(), array()) that is the acceptance format to 
            //create a valide json for the grids population

            foreach($RecentListingsRows as $key => $row)
            {
                $parentArray[] = array("id"=>$row->id, 
                                    "ProductName"=>$row->productName, 
                                    "Category"=>$row->category, 
                                    "ListingDate"=>$row->listingDate, 
                                    "SoldOut"=>$row->soldOut
                                    );
            }

            wp_send_json_success($parentArray);

        break;
        case "load_accountInfo":
            loadAccountInfo();
        break;
        case "update_to_not_promoted":
            $type = $_POST["post_type"];
            updateProductToNoPromoted($type);
        break;
        case "link_file_with_post" :
            //$productId =  $_POST["post_id"];
            $post_id =  $_POST["post_id"];
            $attachment_id = $_POST["attach_id"];
            //true if success
            $result = update_post_meta($post_id, '_thumbnail_id', $attachment_id);
            $response = array( 'success' => true, 'data' => $result );
            wp_send_json($response);
        break;
    }
}
//action, method name from up 
add_action( 'wp_ajax_create_listings_request', 'create_listings_request_callback' );
add_action('wp_ajax_nopriv_create_listings_request', 'create_listings_request_callback');

//create category table
function createCategoryTable($productId)

{
    global $wpdb; 
    $prefix = $wpdb->prefix;
    $htmlDiv = "";
    $htmlTd = "";
    $htmlInsideAccordion = "";
    $x = 0;
    $y = 0;
    $countIte = 0;
    $categoryArray = [];

    if($productId != "0"){
                $categoryQuery = "SELECT T.term_id AS 'id' FROM ".$prefix ."term_relationships as TR 
                        INNER JOIN ". $prefix ."term_taxonomy TT ON TR.term_taxonomy_id = TT.term_taxonomy_id
                        INNER JOIN ".$prefix ."terms T ON T.term_id = TT.term_id
                        where TR.object_id = $productId AND TT.taxonomy = 'product_cat'";
                        

        $categoryRows = $wpdb->get_results( $categoryQuery, OBJECT);
        foreach($categoryRows as $key => $row)
        {
            $categoryArray[] = $row->id;
        }

    }
    

    $query = "SELECT DISTINCT T.term_id AS 'id', 
                T.name AS 'category', 
                TT.parent AS 'parent',
                T.slug AS 'slug',
                IFNULL((SELECT name FROM ".$prefix."terms WHERE term_id = TT.parent ), 'General Category') AS 'parentName'
                FROM ".$prefix."terms as T 
                INNER JOIN ".$prefix."term_taxonomy TT ON TT.term_id = T.term_id
                WHERE  TT.taxonomy = 'product_cat' AND
                (SELECT name FROM ".$prefix."terms WHERE term_id = TT.parent ) IS NOT NULL
                ORDER BY parentName, T.name ASC";

    $wpdb->show_errors(true);
    $categoryRows = $wpdb->get_results( $query, OBJECT);
    foreach($categoryRows as $key => $row)
    {
        $childArray[] = array(  $row->id, 
                                $row->category, 
                                $row->slug, 
                                $row->parent,
                                $row->parentName  
                            );
    }
    
    $queryParent = "SELECT  DISTINCT IFNULL((SELECT name FROM ".$prefix."terms WHERE term_id = TT.parent ), 
    		'General Category') AS 'parentName'
                FROM ".$prefix."terms as T 
                INNER JOIN ".$prefix."term_taxonomy TT ON TT.term_id = T.term_id
                WHERE  TT.taxonomy = 'product_cat' AND 
                (SELECT name FROM ".$prefix."terms WHERE term_id = TT.parent ) IS NOT NULL
                ORDER BY parentName, T.name ASC";
    $categoryRows = $wpdb->get_results( $queryParent, OBJECT);
    foreach($categoryRows as $key => $row)
    {
        $parentArray[] = array($row->parentName);  
                                        
    }

    while($x < count($parentArray))
    {
       
        $htmlHead = "<h1>". $parentArray[$x][0] ."</h1><div class=\"checked-category\">";
        
        $countIte = 0;            
        while( $y < count($childArray) && $childArray[$y][4] == $parentArray[$x][0] )
        {
            
            $val = $childArray[$y][0];
            //checked values if product has categories assigned
            if(in_array($val, $categoryArray)){
                $htmlHead = "<h1>". $parentArray[$x][0] ."</h1><div class=\"checked-category\">";
                $htmlTd = $htmlTd . "<div class=\"medium-4 columns\"><input type=\"checkbox\" name=\"category\" value=\"".$childArray[$y][0]." \" checked>" . $childArray[$y][1] ."</div>";
            }
            else{
                $htmlTd = $htmlTd . "<div class=\"medium-4 columns\"><input type=\"checkbox\" name=\"category\" value=\"".$childArray[$y][0]."\">" . $childArray[$y][1] ."</div>";
            }
            

            if($countIte == 2){ $countIte = 0; $htmlTd = $htmlTd ;}
            else{ $countIte++; }
            if($y+1 < count($childArray)){
                if($childArray[$y+1][4] == $parentArray[$x][0] && $countIte == 2){$htmlTd;}
                if($childArray[$y+1][4] != $parentArray[$x][0] ){ $htmlTd = $htmlTd; }
            }
            else{$htmlTd = $htmlTd ;}
            
            $y++;
        }
        
        $htmlDiv = $htmlHead . $htmlTd ."</div>";
		$htmlTd = "";

                    
        $htmlArray[$x] = $htmlDiv;
        $x++;
    }
    $htmlInsideAccordion = implode('', $htmlArray);
    $accor = '<script>jQuery(".accordion").accordion({ 

heightStyle: "content" 

});</script>';
    $html = $htmlInsideAccordion;

    $response = array( 'success' => true, 'accordionContent' =>  $html, "accorscript" => $accor, 'val' => implode(',', $categoryArray) );  
    wp_send_json($response);
}

//decrease credits after operation
function decreaseCredits(){
    global $wpdb; //variable use to interact and perform queries to the db
    $userId = get_current_user_id();
    $totalCredits = 0;
    $newtotalCredits = 0;
    $prefix = $wpdb->prefix;
    $vendor = yith_get_vendor( 'current', 'user' );
    get_currentuserinfo();
    $vendorId = $vendor->term_taxonomy_id;
    //select credits - 1
    $query = "SELECT total_credits - 1 AS 'total_credits' FROM ".$prefix."zoomlocal_credits 
               WHERE expiration_date >= CURDATE() AND vendor_id = $vendorId  
               ORDER BY expiration_date, credit_id ASC LIMIT 1";
    foreach( $wpdb->get_results($query) as $key => $row) {
        $totalCredits = $row->total_credits;
    }
    //update credits to new value
    $queryUpdate = "UPDATE ".$prefix."zoomlocal_credits SET total_credits = $totalCredits
                    WHERE expiration_date >= CURDATE() AND vendor_id = $vendorId 
                    ORDER BY expiration_date, credit_id ASC LIMIT 1";

    $value = $wpdb->query($queryUpdate);
    
    //getting new credits values
    $queryNewTotal = "SELECT sum(total_credits) AS 'total_credits' FROM ".$prefix."zoomlocal_credits 
                        WHERE expiration_date >= CURDATE() AND vendor_id = $vendorId";
    foreach( $wpdb->get_results($queryNewTotal) as $key => $row) {
        $newtotalCredits = $row->total_credits;
    }
    return $newtotalCredits;

}



//When a listing date is < than today's date products should be displayed into no promoted listings, this method updates all products or events base on post_date per vendor
function updateProductToNoPromoted($type){
    global $wpdb; 
    $userId = get_current_user_id();
    $prefix = $wpdb->prefix;
    $vendor = yith_get_vendor( 'current', 'user' );
    get_currentuserinfo();
    $vendorId = $vendor->term_taxonomy_id;
    $query = "UPDATE ".$prefix."postmeta 
              SET meta_value = 'no'
              WHERE  meta_key = '_featured' AND post_id IN 
              (SELECT ID FROM ".$prefix."posts 
              WHERE (post_date < current_date() AND
                    current_date() > date_add(post_date, INTERVAL 30 DAY)) AND
                    post_type_real = '$type' AND post_author 
                    IN (SELECT DISTINCT user_id FROM ".$prefix."usermeta
                    WHERE meta_key = 'yith_product_vendor_owner'AND
                    meta_value = $vendorId))";

    $value = $wpdb->query($query);

    if($value != false){ $response = array( 'success' => true, 'data' =>  true); }
    else{ $response = array( 'success' => false, 'data' => $wpdb->last_error); }
    wp_send_json($response);
}

//Insert and update records related to products and events
//$type can be event or product
//$isInsert true or false, true execute and insert statment, false executes an update statament
function insertProduct($type, $title, $description, $date, $date2, $qty, $price, $category, 
                        $facebook, $twitter, $instagram, $isInsert, $productId,  $address )
{
    global $wpdb; 
    $userId = get_current_user_id();
    $prefix = $wpdb->prefix;
    $arrCategory = explode(",", $category);
    $priceArray = explode( " " , $price);
    $price = $priceArray[1];
    $vendor = yith_get_vendor( 'current', 'user' );
    get_currentuserinfo();
    $vendorId = $vendor->term_taxonomy_id;

    if($isInsert == true)
    {
        $post = array(
            'post_author' =>  $userId,
            'post_excerpt' => $description,
            'post_status' => "publish",
            'post_title' => $title,
            'post_parent' => '',
            'post_type' => "product",
            'post_date' => $date
        );

        //Create post
        //$post_id = wp_insert_post( $post, $wp_error );
        $post_id = wp_insert_post( $post );
        $updateQuery = "update ".$prefix."posts 
                        set post_type_real ='".$type."'
                        where ID = $post_id";
        $value = $wpdb->query($updateQuery);
    }
    else
    {
        $post = array(
            'ID'           => $productId,
            'post_author' =>  $userId,
            'post_excerpt' => $description,
            'post_status' => "publish",
            'post_title' => $title,
            'post_parent' => '',
            'post_type' => "product",
            'post_date' => $date
        );
        //wp_update_post( $post, $wp_error );
        wp_update_post( $post);
        $post_id = $productId;
    }
    
    //creating category array
    for ($i = 0; $i <= count($arrCategory) - 1 ; $i++) 
    {
        $catId = $arrCategory[$i];
        $queryCatName = "SELECT slug FROM ".$prefix."terms WHERE term_id = $catId";
        $categoryRow = $wpdb->get_row($queryCatName);
        $arrCatName[] = $categoryRow->slug;
    }

    //adding categories
    wp_set_object_terms($post_id, $arrCatName, 'product_cat', true );

    //add vendor to product
    wp_set_object_terms($post_id, $vendorId, 'yith_shop_vendor', true );


    //wp_set_object_terms($post_id, 'simple', 'product_type');
    $meta_type = 'post';
    if($type == "event")
    {
       $regionName = $_POST["regionName"];
       $cityName = $_POST["cityName"];
       $zipCode = $_POST["zipCode"];
	   /* $lat = $_POST["lat"];
	    $long = $_POST["long"];*/
       update_post_meta($post_id, '_rq_event_start_date', $date);

       update_post_meta($post_id, '_rq_event_end_date', $date2);
       update_post_meta($post_id, '_rq_event_address_name', $address);
       update_post_meta($post_id, '_rq_event_region_name', $regionName);
       update_post_meta($post_id, '_rq_event_city_name', $cityName);
	    update_post_meta($post_id, '_rq_event_zip_code', $zipCode);/*
	    update_post_meta($post_id, '_rq_event_lon_name', $lat);
	    update_post_meta($post_id, '_rq_event_lat)name', $long);*/
	    wp_set_object_terms($post_id, 'event', 'product_type');
    }
    else{
        wp_set_object_terms($post_id, 'simple', 'product_type');

    }
    update_post_meta($post_id, '_stock_status', "instock");
    if($type == "product" && $qty != "0")
    {
        update_post_meta($post_id, '_manage_stock', 'yes');
        update_post_meta($post_id, '_stock', $qty);
    }
    

    update_post_meta($post_id, '_regular_price',  $price );
    update_post_meta($post_id, '_sale_price',  $price );
    update_post_meta($post_id, '_price',  $price );
    update_post_meta($post_id, '_featured', "no" );
    update_post_meta($post_id, '_visibility', 'visible' );
    update_post_meta($post_id, 'total_sales', '0');
    update_post_meta($post_id, '_downloadable', 'yes');
    update_post_meta($post_id, '_virtual', 'yes');
    update_post_meta($post_id, '_share_facebook', $facebook);
    update_post_meta($post_id, '_share_twitter', $twitter);
    update_post_meta($post_id, '_share_instagram', $instagram);

    // file paths will be stored in an array keyed off md5(file path)
    /* $downdloadArray =array('name'=>"Test", 'file' => $uploadDIR['baseurl']."/video/".$video);

    $file_path = md5($uploadDIR['baseurl']."/video/".$video);

    $_file_paths[  $file_path  ] = $downdloadArray;
    // grant permission to any newly added files on any existing orders for this product
    //do_action( 'woocommerce_process_product_file_download_paths', $post_id, 0, $downdloadArray );
    update_post_meta( $post_id, '_downloadable_files ', $_file_paths);
    update_post_meta( $post_id, '_download_limit', '');
    update_post_meta( $post_id, '_download_expiry', '');
    update_post_meta( $post_id, '_download_type', '');
    update_post_meta( $post_id, '_product_image_gallery', '');*/

    $guid = $wpdb->get_var( "SELECT guid FROM ".$prefix."posts WHERE ID = $post_id");

    $response = array( 'success' => true, 'data' => $post_id, 'guid' => $guid , 'value' => $value);
    //$response = array( 'success' => false, 'data' => $wpdb->last_error);
    wp_send_json($response);
}


//delete product record by id
function deleteProduct($id){
    global $wpdb; 
    $userId = get_current_user_id();
    $prefix = $wpdb->prefix;

    $wpdb->delete( $prefix.'term_relationships', array( 'object_id' => $id ), array( '%d' ));
    $wpdb->delete( $prefix.'postmeta', array( 'post_id' => $id ), array( '%d' ) );
    $wpdb->delete( $prefix.'posts', array( 'ID' => $id ), array( '%d' ) );
    $response = array( 'success' => true, 'data' => "Yes");
    $wpdb->show_errors(true);
    wp_send_json($response);
    
}

//load account user information
function loadAccountInfo(){
    global $wpdb; 
    global $current_user;
    $userId = get_current_user_id();
    $prefix = $wpdb->prefix;
    $vendor = yith_get_vendor( 'current', 'user' );
    get_currentuserinfo();
    $vendorId = $vendor->term_taxonomy_id;

    //get total credits per loged user 
    $totalCredits = 00;
    $query = "SELECT sum(total_credits) AS 'total_credits' FROM ".$prefix."zoomlocal_credits 
                        WHERE expiration_date >= CURDATE() AND vendor_id = $vendorId";
    $value = $wpdb->query($query);
    foreach( $wpdb->get_results($query) as $key => $row) {
        $totalCredits = $row->total_credits;
    }

    if($totalCredits == 0 || $totalCredits == null){$totalCredits = 0;}

    $response = array( 'success' => true, 'vendorName' => $vendor->name, 'userName' => $current_user->display_name, 'credits' => $totalCredits);
    $wpdb->show_errors(true);
    //$response = array( 'success' => false, 'data' => $wpdb->last_error);
    wp_send_json($response);
}

//Load product and event information for share screen
function share_info_request_callback(){
    global $wpdb; //variable use to interact and perform queries to the db
    $productId = $_POST["post_id"];
    $prefix = $wpdb->prefix;
    $queryProduct = "SELECT post_name, post_content FROM ".$prefix."posts where ID = $productId";
    $productArr = $wpdb->get_row( $queryProduct, ARRAY_A );

    $queryImage = "SELECT guid FROM ".$prefix."posts 
                    WHERE ID = (SELECT meta_value FROM ".$prefix."postmeta 
                                WHERE post_id = $productId AND meta_key = '_thumbnail_id')";
    $imgArr = $wpdb->get_row( $queryImage, ARRAY_A );

    $productName = $productArr['post_name'];
    $productDes = $productArr['post_content'];
    $imgUrl = $imgArr['guid'];
    $html = "<h2>$productName</h2>
            <p>
            <span>
            $productDes
            </span></p><br>
            <div >
                <img src=\"$imgUrl\">
            </div>";

    $response = array( 'success' => true, 'postname' => $productArr['post_name'], 'description' => $productArr['post_name'], 'url' => $imgArr['guid'], 'html' => $html );
    
    wp_send_json($response);
}

//action, method name from up 
add_action( 'wp_ajax_share_info_request', 'share_info_request_callback' );
add_action('wp_ajax_nopriv_share_info_request', 'share_info_request_callback');

//add the functinality to upload a file using wp codex recommended code
function upload_file_callback()
{
   global $wpdb; 
    //---- upload image using wp recommended code in codex ----
    //the nonce was valid and the user has the capabilities, it is safe to continue.
    if ( isset( $_POST['my_image_upload_nonce'], $_POST['post_id'] ) 
        && wp_verify_nonce( $_POST['my_image_upload_nonce'], 'image_upload' )
        /*&& current_user_can( 'edit_post', $_POST['post_id'] )*/)
        {
        // The nonce was valid and the user has the capabilities, it is safe to continue.

        // These files need to be included as dependencies when on the front end.
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );
        
        // Let WordPress handle the upload.
        // Remember, 'my_image_upload' is the name of our file input in our form above.
        $attachment_id = media_handle_upload( 'image_upload', $_POST['post_id'] );
        
        if ( is_wp_error( $attachment_id ) ) {
            // There was an error uploading the image.
            $response = array( 'success' => false, 'data' => "There was an error uploading the image.");
        } else {
            // The image was uploaded successfully!
            //link image with product
            $response = array( 'success' => true, 'data' => $attachment_id);
        }

    } else {
        // The security check failed, maybe show the user an error.
        $response = array( 'success' => false, 'data' => "The security check failed", 'val'=>current_user_can( 'edit_post', $_POST['post_id'] ) );
    }

    wp_send_json($response);

 }
//action, method name from up 
add_action( 'wp_ajax_upload_file_request', 'upload_file_callback' );
add_action('wp_ajax_nopriv_upload_file_request', 'upload_file_callback');

/*********************** PR END ***********************/
/**************** Map query start ********************/
function location_search(){
    if ( isset($_REQUEST) ) {
        $search = $_REQUEST['value'];
        $s_para = array();
        parse_str($search, $s_para);
        $shop = $s_para['place'];
        $loc = $s_para['location'];
        $city = $state = $zip = '';
        $taxo = get_terms( 'yith_shop_vendor', array('hide_empty' => false, 'name__like' => $shop) );
        global $wpdb;
        $list = array();
        // echo $loc;
        if( is_numeric($loc) ){
            $zip = trim($loc);
            foreach ($taxo as $taxnmy) {
                $z_id = $wpdb->get_var( "SELECT `woocommerce_term_id` from $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $taxnmy->term_id AND `meta_key` = 'zip' AND `meta_value` LIKE '%".$zip."%'" );
                if($z_id != ''){ array_push($list,$z_id); } 
            }
        }
        else{
            $loc = explode(',', $loc);
            if(count($loc) == 1){
                $loc = trim($loc[0]);
                if($loc == ''){
                    foreach ($taxo as $taxnmy) {
                        $l_id = $taxnmy->term_id;
                        if($l_id != ''){ array_push($list,$l_id); } 
                    }
                }
                else{
                    foreach ($taxo as $taxnmy) {
                        $l_id = $wpdb->get_var( "SELECT `woocommerce_term_id` from $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $taxnmy->term_id AND (`meta_key` IN ('city', 'state') AND `meta_value` LIKE '%".$loc."%')" );
                        if($l_id != ''){ array_push($list,$l_id); } 
                    }
                }   
            }else{
                $cs_id = array();
                $city = trim($loc[0]);
                $state = trim($loc[1]);
                foreach ($taxo as $taxnmy) {
                    $c = $wpdb->get_var( "SELECT `woocommerce_term_id` from $wpdb->woocommerce_termmeta where (`woocommerce_term_id` = $taxnmy->term_id AND `meta_key` = 'city' AND `meta_value` LIKE '%".$city."%')" );
                    $s = $wpdb->get_var( "SELECT `woocommerce_term_id` from $wpdb->woocommerce_termmeta where (`woocommerce_term_id` = $taxnmy->term_id AND `meta_key` = 'state' AND `meta_value` LIKE '%".$state."%')" );
                    if(($c == $s) && ($c != '')){
                        array_push($list,$c);   
                    }
                }
            }
        }
        $geo = array();
        $shop = array();
        $tel = array();
        $geoloc = '';
        // print_r($list);
        $map_icon = array();
        // $geo = '';
        foreach ($list as $value) {
            $keys = $wpdb->get_col("SELECT * FROM $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $value", 2);
            $values = $wpdb->get_col("SELECT * FROM $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $value", 3);
            $geoloc = $wpdb->get_var("SELECT `meta_value` FROM $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $value AND `meta_key` = 'geoloc' ");
            if($geoloc != ''){
               array_push($geo, $geoloc);
                $shopname = $wpdb->get_var("SELECT `name` from $wpdb->terms where `term_id` = $value");
                $storelink = $wpdb->get_var("SELECT `slug` from $wpdb->terms where `term_id` = $value");
                $shopname = $shopname ? $shopname : '';
                $imagehead = $wpdb->get_var("SELECT `meta_value` FROM $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $value AND `meta_key` = 'header_image' ");
                $shoptel = $wpdb->get_var("SELECT `meta_value` FROM $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $value AND `meta_key` = 'telephone' ");
                $address1 = $wpdb->get_var("SELECT `meta_value` FROM $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $value AND `meta_key` = 'address1' ");
                $address2 = $wpdb->get_var("SELECT `meta_value` FROM $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $value AND `meta_key` = 'address2' ");
                $location = $wpdb->get_var("SELECT `meta_value` FROM $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $value AND `meta_key` = 'location' ");
                array_push($shop, $shopname);
                $my_postid = $imagehead;//This is page id or post id
                $gui_post = get_post($my_postid);
                $postimage = $gui_post->guid;
                $storeper = get_bloginfo( 'url' ).'/vendor/'.$storelink;
                array_push($tel,$shoptel ,$address1, $address2, $location, $postimage, $storeper);
                //$tempArray = array();
              //foreach( $shoptel as $key => $value )
               // {
              //  $tempArray[] = '[' .$key .',' .$value .']';
              //  }

                //$imploded = implode( ',' , $tempArray );

             // array_push($tel,$tempArray);

            }
            $keys[] = 'id';
            $values[] = $value;
            $keys[] = 'shop_name';
            $values[] = $wpdb->get_var("SELECT `name` from $wpdb->terms where `term_id` = $value");
            $keys[] = 'permalink';
            $link = $wpdb->get_var("SELECT `slug` from $wpdb->terms where `term_id` = $value");
            $values[] = get_bloginfo( 'url' ).'/vendor/'.$link;
            $header_id = $wpdb->get_var("SELECT `meta_value` FROM $wpdb->woocommerce_termmeta where `woocommerce_term_id` = $value AND `meta_key` = 'store_icon' ");
            if($header_id){
                $keys[] = 'header_image';
                $icon_url = $header_id;
                $icon_url = get_template_directory_uri().'/woocommerce/store-avatar/'.$icon_url.'_icon.png';
                $map_marker = get_template_directory_uri().'/woocommerce/store-avatar/small/'.$header_id.'_icon.png';
                // $icon_url = get_the_post_thumbnail( $header_id, array(10,10));
                // print_r($icon_url); 
                $values[] = $icon_url;
            }
            else{
                $keys[] = 'header_image';
                $icon_url = get_template_directory_uri().'/assets/img/shop.png';
                $map_marker = $icon_url;
                $values[] = $icon_url;
            }
            if($geoloc != ''){
                  array_push($map_icon, $map_marker);
                  $geoloc = '';
                  $map_marker = '';
            }
            //print_r(json_encode($shop));
           // die;

         // $tempArray = array();
             // foreach( $tel as $key => $value )
            //    {
              //  $tempArray[] = '[' .$key .':' .$value .']';
             //   }
                //$imploded = implode( ',' , $tempArray );

             //array_push($tel,$tempArray);

           //$page = get_page_by_title( 'GMBC Solutions' );

           //$demso = $page->ID;

            $result[] = array_combine($keys,$values);
            $data['success'] = '';
            $data['result'] = $result;
            $data['regionlocation'] = $geo;
            $data['contentstring'] = $shop;
            $data['icon_url'] = $map_icon;
            $data['tel'] = array_chunk($tel,6);
            $data['page'] = print_r($page);
        }
        if(!empty($data)){
            $data['success'] = 'true';
            print_r(json_encode($data));
        }else{
            $data['success'] = 'false';
            print_r(json_encode($data));
        }
        die;
    }
}
add_action( 'wp_ajax_location_search', 'location_search' );
add_action( 'wp_ajax_nopriv_location_search', 'location_search' );
 
/************************* Map query End **************************/

/****** remove the classes from menu *************/
add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1);
add_filter('page_css_class', 'my_css_attributes_filter', 100, 1);
function my_css_attributes_filter($var) {
  return is_array($var) ? array() : '';
}

/************************* Option Page **************************/
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
    acf_add_options_sub_page(array(
        'page_title'    => 'Theme Footer Settings',
        'menu_title'    => 'Footer',
        'parent_slug'   => 'theme-general-settings',
    ));
    
}

/** ajax call for vendor and their products **/
function product_search(){
    $search = $_REQUEST['value'];
    $loc = $search['location'];
    global $wpdb;
    if( !empty($loc) ){
        $owner_term_ids = $wpdb->get_col("SELECT `woocommerce_term_id` FROM $wpdb->woocommerce_termmeta WHERE `meta_key` = 'zip' AND `meta_value` = $loc ");
    }else{
        $owner_term_ids = $wpdb->get_col("SELECT `woocommerce_term_id` FROM $wpdb->woocommerce_termmeta WHERE `meta_key` = 'zip'");
    }
    $owner = array();
    foreach($owner_term_ids as $owner_term_id){
        $owner_id = $wpdb->get_var("SELECT `meta_value` FROM $wpdb->woocommerce_termmeta WHERE `meta_key` = 'owner' AND `woocommerce_term_id` =  $owner_term_id");
        array_push($owner, $owner_id);
    }
    $products = array();    
    $args 
        = 
        array( 
            'post_type'             => 'product',
            'post_status'           => 'publish',
            'author__in'            => $owner,
            'posts_per_page'        => empty($loc) ? 40 : -1,
            'tax_query'             => array(
                'relation'  =>  'OR',
                array(
                    'taxonomy' => 'product_type',
                    'terms' => 'simple',
                    'field' => 'slug',
                ),
                array(
                    'taxonomy' => 'product_type',
                    'terms' => 'grouped',
                    'field' => 'slug',
                ),
                array(
                    'taxonomy' => 'product_type',
                    'terms' => 'variable',
                    'field' => 'slug',
                ),
                array(
                    'taxonomy' => 'product_type',
                    'terms' => 'external',
                    'field' => 'slug',
                ),
            )
        );

    $new_query = new WP_Query( $args );
    $product = array();
    if( $new_query->have_posts() ):

        while( $new_query->have_posts() ): 

            $new_query->the_post();
            $post_id = get_the_ID();
            $post = get_post( $post_id );
            $user_id=$post->post_author;
            $key = 'yith_product_vendor_owner';
            $single = true;
            $user_last = get_user_meta( $user_id, $key, $single );
            $term = get_term($user_last,'yith_shop_vendor');
            $product['titl'] = $term->name;
            $product['url'] = get_permalink();
            $product['upids'] = $user_id;
            $product['img'] = wp_get_attachment_url( get_post_thumbnail_id() );
            array_push($products, $product);
        
        endwhile;
        $script = "<script>
        jQuery('.slider-divs').slick({ slidesToShow: 4, infinite: true, rows: 2, slidesToScroll: 1, autoplay: true, autoplaySpeed: 2000, dots: false, speed: 300, responsive:true, responsive: [ { breakpoint: 1024, settings: { slidesToShow: 3, slidesToScroll: 3, infinite: true, dots: true } }, { breakpoint: 770, settings: { slidesToShow: 2, slidesToScroll: 2 } }, { breakpoint: 480, settings: { slidesToShow: 1, slidesToScroll: 1 } } ] });</script>";
    endif;
    $data['products'] = $products;
    $data['script'] = $script;
    wp_reset_postdata();
    echo json_encode($data); 
    die;
}
add_action( 'wp_ajax_product_search', 'product_search' );
add_action( 'wp_ajax_nopriv_product_search', 'product_search' );



function zl_admin_js() {

	//wp_register_script( 'wc-admin-meta-boxes', WC()->plugin_url() . '/assets/js/admin/meta-boxes' . $suffix . '.js', array( 'jquery', 'jquery-ui-datepicker', 'jquery-ui-sortable', 'accounting', 'round', 'wc-enhanced-select', 'plupload-all', 'stupidtable', 'jquery-tiptip' ), WC_VERSION );
	wp_register_script( 'zl-admin-script', get_template_directory_uri() . '/woocommerce/scripts/zl-admin.js', array(), '1', true );
	wp_enqueue_script( 'zl-admin-script' );

}
add_action( 'admin_enqueue_scripts', 'zl_admin_js' );


//temporary functions for credits
function zl_get_user_credits($vendorId){
	global $wpdb;
	$totalCredits = 0;
	$query = "SELECT sum(total_credits) AS 'total_credits' FROM ".$wpdb->prefix."zoomlocal_credits
                        WHERE expiration_date >= CURDATE() AND vendor_id = $vendorId";
	$value = $wpdb->query($query);
	foreach( $wpdb->get_results($query) as $key => $row) {
		$totalCredits = $row->total_credits;
	}

	if($totalCredits == 0 || $totalCredits == null){$totalCredits = 0;}
	if($value != false){
		return $totalCredits;
	}else{
		return 0;
	}
}
//remove credits and return remaining after deduction
