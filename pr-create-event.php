<?php
/*
Template Name: pr-create-event
*/
?>

<?php get_header(); ?>
<head>
	<!--Special configuration to css -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php  bloginfo("template_directory"); ?>/jquery-ui/specialAdjustments.css" />
	<style type="text/css">
		.ui-common-table {
			width:100% !important;
		}

		.ui-jqgrid-view  {
			width:100% !important;
		}

		.ui-jqgrid-hbox {
			padding-right: 0px !important;
		}
		.ui-jqgrid .ui-jqgrid-pager .ui-pager-table {
			table-layout: auto !important;
		}

		.ui-jqgrid-pager td {
			width: auto !important;
		}
	</style>
</head>
<input type="hidden" id="productId"  value="0">
<input type="hidden" id="redirect"  value="N">

<div id="content" style="margin-top: 0px;">

<div id="inner-content" class="row">

<div id="main" class="large-12 medium-12 columns" role="main">

<?php get_template_part( 'parts/loop', 'page' ); ?>
<!-- Success messages -->
<div class="success" id="susccessDiv" style="display:none">
	<span id="successMsg"></span>
</div>
<div class="error" id="errorDiv" style="display:none">
	<span id="errorMsg"></span>
</div>
<div class="warning" id="warningDiv" style="display:none">
	<span id="warningMsg"></span>
</div>

<!-- PRODUCT AND ACCOUNT INFORMATION SECTION-->
<section id="productBasic"class="listingsection">

	<div class="user-detail mobile-pe show-for-small-only">
		<p>Welcome:&nbsp; <span class="displayLabelInfo" id="userName"></span></p>
		<p>Company:&nbsp;<span class="displayLabelInfo" id="companyName"></span></span></p>
		<p>Account level:&nbsp;<span class="displayLabelInfo" id="levelTitle"></span></p>
		<p>Posting remaining:&nbsp;<span class="displayLabelInfo" id="credits"> </span></p>
	</div>
	<div class="row listing-wrapper">
		<!--<div  style="width:50%; float: left;" ><-->
		<div class="medium-7 columns">
			<label class="displayLabel">Event Title *</label>
			<div class="input-section">
				<input type="text" name="fname" id="listingTitle" style="width:100%;">
			</div>
			<label class="displayLabel">Event Description *</label>
			<div class="text-area-section">
				<textarea id="listingDescription" rows="10" cols="50" style="width:100%;" placeholder="Insert up to 500 characters..."></textarea><br>
			</div>
		</div>
		<div class="medium-5 columns">
			<aside class="dynamic-user-detail">
				<div class="user-detail hide-for-small-only">
					<p>Welcome:&nbsp; <span class="displayLabelInfo" id="userName"></span></p>
					<p>Company:&nbsp;<span class="displayLabelInfo" id="companyName"></span></span></p>
					<p>Account level:&nbsp;<span class="displayLabelInfo" id="levelTitle"></span></p>
					<p>Posting remaining:&nbsp;<span class="displayLabelInfo" id="credits"> </span></p>
				</div>
				<!-- <nav class="top-bar" data-topbar style="padding-left:140px;" id="myAccountNav">
					my account menu -->
				<!--<section class="top-bar-section left">
			                            <?php //wp_nav_menu('menu=myAccount_menu'); ?>
			                        </section>
			                    </nav> -->
				<!-- <div style="padding-left:150px;">
					<span class="displayLabel">Company:</span><span class="displayLabelInfo" id="companyName"></span>
					<span class="displayLabel">Name:</span><span class="displayLabelInfo" id="userName"></span><br>
					<span class="displayLabel">Account Level:</span><span class="displayLabelInfo" id="levelTitle"></span><br>
					<span class="displayLabel">Posting left Pay Period:</span><span class="displayLabelInfo" id="credits"></span><br>
				</div><br> -->

				<!-- START: the product image -->
				<div class="entry-content imagesection" style="padding-left:90px;">
					<form id="featured_upload" method="post" action="#" enctype="multipart/form-data">
						<label for="image_upload" class='imglable'></label>
						<input type="file" name="image_upload" id="image_upload"  multiple="false" />
						<input style="display: none;" id="submit_image_upload" name="submit_image_upload" type="submit" value="Upload" />
						<input type="hidden" name="post_id" id="post_id" value="0" />
						<?php wp_nonce_field( 'image_upload', 'my_image_upload_nonce' ); ?>
						<input type='hidden' name='action' value='upload_file_request'>
						<input type="hidden" id="attachId"  value="0">
					</form>
					<div id="image_preview" >
						<img id="previewing" />
						<h4 id="loading" style="display: none;">loading...</h4>
						<div id="message"></div>
					</div>
				</div><br><br>
				<!-- END: the product image -->
			</aside>
		</div>
	</div>
</section>

<!-- PRODUCT TABS SECTION-->
<section class="listing-detail event-detail">
	<div class="row listing-detail-wrapper">
		<div class="medium-12 columns">
			<div class="list-detai-content">
				<div id="productTabsSection" >
					<div id="tabs" style="width:500px;">
						<ul>
							<li><a href="#tabs-1">Listing Detail </a></li>
							<li><a href="#tabs-2">Matching Item </a></li>
						</ul>
						<div id="tabs-1">
							<div id="listingDetailCont">
								<label class="displayLabel">Event Address *</label>
								<input type="text" name="fname" id="eventAddress"  placeholder="Insert up to 500 characters..."><br>
								<label class="displayLabel"> City Name *</label>
								<input type="text" name="fname" id="cityName"><br>
								<label class="displayLabel"> State Name *</label>
								<!-- <input type="text" name="fname" id="regionName"> -->
								<select  name="fname" id="regionName" >
									<option value="AL">AL</option>
									<option value="AK">AK</option>
									<option value="AZ">AZ</option>
									<option value="AR">AR</option>
									<option value="CA">CA</option>
									<option value="CO">CO</option>
									<option value="CT">CT</option>
									<option value="DE">DE</option>
									<option value="DC">DC</option>
									<option value="FL">FL</option>
									<option value="GA">GA</option>
									<option value="HI">HI</option>
									<option value="ID">ID</option>
									<option value="IL">IL</option>
									<option value="IN">IN</option>
									<option value="IA">IA</option>
									<option value="KS">KS</option>
									<option value="KY">KY</option>
									<option value="LA">LA</option>
									<option value="ME">ME</option>
									<option value="MD">MD</option>
									<option value="MA">MA</option>
									<option value="MI">MI</option>
									<option value="MN">MN</option>
									<option value="MS">MS</option>
									<option value="MO">MO</option>
									<option value="MT">MT</option>
									<option value="NE">NE</option>
									<option value="NV">NV</option>
									<option value="NH">NH</option>
									<option value="NJ">NJ</option>
									<option value="NM">NM</option>
									<option value="NY">NY</option>
									<option value="NC">NC</option>
									<option value="ND">ND</option>
									<option value="OH">OH</option>
									<option value="OK">OK</option>
									<option value="OR">OR</option>
									<option value="PA">PA</option>
									<option value="RI">RI</option>
									<option value="SC">SC</option>
									<option value="SD">SD</option>
									<option value="TN">TN</option>
									<option value="TX">TX</option>
									<option value="UT">UT</option>
									<option value="VT">VT</option>
									<option value="VA">VA</option>
									<option value="WA">WA</option>
									<option value="WV">WV</option>
									<option value="WI">WI</option>
									<option value="WY">WY</option>
								</select><br>
								<label class="displayLabel">Zip Code *</label>
								<input type="text" id="zipCode" name="zipCode" onKeyPress="return isNumber(event)" ><br>

								<!--  <label class="displayLabel"> Ticket Link </label>
									<input type="text" name="tlink" id="tlinks" placeholder="Enter Ticketmaster link or custom link to purchase tickets (optional)"><br>
-->
								<label class="displayLabel" >Price *</label>
								<input type="text" id="listingPriceRange" onKeyPress="return isNumber(event)"><br>
								<div id="timePickerSpecial">
									<label class="displayLabel" >Event Start Date *</label>
									<input type="text" id="eventStartDate">
								</div>
								<div id="timePickerSpecial2">
									<label class="displayLabel" >Event End Date </label>
									<input type="text" id="eventEndDate">
								</div>
							</div>
						</div>
						<div id="tabs-2">
							<table id="matchingItemsGrid"></table>
							<div id="pmatchingItemsGrid"></div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- CATEGORY SECTION-->
<section class="category-section">
	<div class="row">
		<div class="medium-12 columns">
			<div id="productCategory">
				<div class="category-header">
					<h5><span>Next step &gt;</span>Choose Event Categories</h5>
					<p>Check as many categories of interest. Change selections as often as you’d like.</p>
				</div>
				<!-- Category Accordion-->
				<div id="categoryDiv">
					<!-- <ul class="accordion" data-accordion style="line-height:15%">
					</ul> -->
					<div class="accordion">
					</div>
					<div class="scriptsdata"></div>
				</div>
			</div>
		</div>
		<!-- SOCIAL MEDIA SECTION-->
		<!-- <div id="productSocialMedia">
		<br><br><br>
		<table style="width:100%; background-color: transparent; border: none !important;">
		  <tr>
			<td>
				<input type="checkbox" name="facebookCheck" id="facebookCheck" value="facebook">&nbsp;
			<a class="social-icon fb" href="#"><i class="fa fa-facebook"></i>Share on Facebook</a>

			</td>
			<td>
				<input type="checkbox" name="twitterCheck" id="twitterCheck" value="twitter">&nbsp;
			<a class="social-icon twit" href="#"><i class="fa fa-twitter"></i></i>Share on Twitter</a>

			</td>
		  </tr>
		  </table>
		</div><br></br> -->
		<div class="medium-12 columns">
			<div class="edit-button">
				<div class="button-group buttongroup">
					<div id="productButtons">
						<button id="clearrall" onClick="clearForm()" class="btn button yel">Clear All</button>
						<button id="deleteBut" onClick="deleteListingData()" class="btn button mar">Delete</button>
						<button id="updateBut" onClick="saveOnly()" class="btn button gre">Save</button>
						<button id="insertBut" onClick="saveAndClose()" class="btn button navy">Save & Close</button>
						<!-- <button id="insertBut" onClick="saveAndClose()" class="buttonProducts buttonProductsBlueBack">Save & Close</button>
						<button id="updateBut" onClick="saveOnly()" class="buttonProducts">Save Changes</button>
						<button onClick="deleteEventData()" class="buttonProducts buttonProductsBlueBack">Delete Item</button>									
						<button onClick="clearForm()" class="buttonProducts buttonProductsGrayBack">Cancel</button>	 -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


</div> <!-- end #main -->

</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
<!-- Confirmation dialog -->
<div id="dialog-confirm" title="Confirmation">
	<p>
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
		Event Record will be deleted, Do you want to proceed?
	</p>
</div>
<?php include 'jqGridEnqueue.php'; ?>
<body>
<script type="text/javascript">
var ajax_url = '<?php echo admin_url( 'admin-ajax.php', 'relative' ); ?>';

//logic to save and close functionality
function saveAndClose()
{
	$("#redirect").val("Y");
	if( $("#productId").val() != "0" )
	{
		updateEvent(true);
	}else{
		insertEvent(true);
	}
}

//logic to save only
function saveOnly()
{
	$("#redirect").val("N");
	if( $("#productId").val() != "0" )
	{
		updateEvent(false);
	}else{
		insertEvent(false);
	}
}

//request to insert product
function insertEvent(close){
	var allCategoryVals = [];
	$('#categoryDiv :checked').each(function() {
		allCategoryVals.push($(this).val());
	});
	if( $("#listingTitle").val() == "" ||
		$("#listingDescription").val() == "" ||
		$("#eventStartDate").val() == "" ||
		$("#eventAddress").val() == "" ||
		$("#regionName").val() == "" ||
		$("#cityName").val() == "" ||
		$("#zipCode").val() == "" ||
		$("#listingPriceRange").val() == "" || allCategoryVals.length == 0)
	{
		displayErrorMessage("#errorDiv","#errorMsg" ,"Fields with * are mandatory, Please complete information");
	}
	else
	{
		if(allCategoryVals.length > 2){
			displayErrorMessage("#errorDiv","#errorMsg" ,"No more than two categories are allowed, please verify selected categories");
		}
		else
		{
			var data = {
				action: 'create_listings_request',
				param: 'insert_event',
				post_title: $("#listingTitle").val(),
				post_content: $("#listingDescription").val(),
				eventStart_date: $("#eventStartDate").val(),
				eventEnd_date: $("#eventEndDate").val(),
				quantity: "0",//$("#listingQty").val(),
				price: $("#listingPriceRange").val(),
				address: $("#eventAddress").val(),
				regionName: $("#regionName").val(),
				cityName: $("#cityName").val(),
				zipCode: $("#zipCode").val(),
				category: allCategoryVals.join(','),
				attach_id: $("#attachId").val(),
				facebook_check: $('#facebookCheck').is(":checked"),
				twitter_check:  $('#twitterCheck').is(":checked"),
				instagram_check: $('#instagramCheck').is(":checked")
			};

			//blocking screen
			block_screen();
			$.ajax({
				url: ajax_url,
				type: "POST",
				data:data,
				action:'create_listings_request',
				dataType: 'json',
				success: function(data){
					displayErrorMessage("#susccessDiv","#successMsg" ,"Event saved successfully");
					$("#productId").val(data.data);
					$("#post_id").val(data.data);
					if($('#facebookCheck').is(":checked") == true){shareFacebookWindow(data.guid);}
					if($('#twitterCheck').is(":checked") == true){shareTwitterWindow(data.guid);}
					if($("#image_upload").val() != ""){$("#image_upload").submit();}

					if(close == true && $("#image_upload").val() == ""){
						setTimeout(function(){
							window.location.replace("<?php echo get_site_url();?>/promoted-event-dashboard/");
						}, 2000);
					}else if( $("#image_upload").val() == "" ){
						unblock_screen();
					}
				},
				error: function(data) {
					console.log(data);
				}
			});
		}//else
	}
}

function updateEvent(close){
	var allCategoryVals = [];
	$('#categoryDiv :checked').each(function() {
		allCategoryVals.push($(this).val());
	});

	if( $("#listingTitle").val() == "" ||
		$("#listingDescription").val() == "" ||
		$("#eventStartDate").val() == "" ||
		$("#eventAddress").val() == "" ||
		$("#regionName").val() == "" ||
		$("#cityName").val() == "" ||
		$("#zipCode").val() == "" ||
		$("#listingPriceRange").val() == "" || allCategoryVals.length == 0 )
	{
		displayErrorMessage("#errorDiv","#errorMsg" ,"Fields with * are mandatory, Please complete information");
	}
	else
	{
		if(allCategoryVals.length > 2){
			displayErrorMessage("#errorDiv","#errorMsg" ,"No more than two categories are allowed, please verify selected categories");
		}
		else
		{
			var data = {
				action: 'create_listings_request',
				param: 'update_event',
				post_title: $("#listingTitle").val(),
				post_content: $("#listingDescription").val(),
				eventStart_date: $("#eventStartDate").val(),
				eventEnd_date: $("#eventEndDate").val(),
				quantity: "0",//$("#listingQty").val(),
				price: $("#listingPriceRange").val(),
				address: $("#eventAddress").val(),
				regionName: $("#regionName").val(),
				cityName: $("#cityName").val(),
				zipCode: $("#zipCode").val(),
				category: allCategoryVals.join(',') ,
				productId: $("#productId").val(),
				attach_id: $("#attachId").val(),
				facebook_check: $('#facebookCheck').is(":checked"),
				twitter_check:  $('#twitterCheck').is(":checked"),
				instagram_check: $('#instagramCheck').is(":checked")
			};

			//blocking screen
			block_screen();
			$.ajax({
				url: ajax_url,
				type: "POST",
				data:data,
				action:'create_listings_request',
				dataType: 'json',
				success: function(data){
					displayErrorMessage("#susccessDiv","#successMsg" ,"Changes saved successfully");
					if($('#facebookCheck').is(":checked") == true){shareFacebookWindow(data.guid);}
					if($('#twitterCheck').is(":checked") == true){shareTwitterWindow(data.guid);}
					if($("#image_upload").val() != ""){
						$("#image_upload").submit();
					}
					if(close == true && $("#image_upload").val() == ""){
						setTimeout(function(){
							window.location.replace("<?php echo get_site_url();?>/promoted-event-dashboard/");
						}, 2000);
					}else if( $("#image_upload").val() == "" ){
						unblock_screen();
					}
				},
				error: function(data) {
					console.log(data);
				}
			});
		}
	}
}

//Load promoted grid data
function loadCategoryData(productId){
	var dataPromoted = {
		action: 'create_listings_request',
		param: 'get_categories',
		productId: productId
	};
	$.ajax({
		url: ajax_url,
		type: "POST",
		data:dataPromoted,
		action:'create_listings_request',
		dataType: 'json',
		success: function(data){
			$('.accordion').html(data.accordionContent);
			$('.scriptsdata').html(data.accorscript);
		},
		error: function() {
			console.log("Error");
		}
	});
}

//request to load product information when page is call for update
function loadProductInformationUpdate(productId){
	var dataSend = {
		action: 'create_listings_request',
		param: 'load_product',
		productId:productId,
		type:"event"
	};
	$("#productId").val(productId);
	$("#post_id").val(productId);
	$.ajax({
		url: ajax_url,
		type: "POST",
		data:dataSend,
		action:'create_listings_request',
		dataType: 'json',
		success: function(data){
			$("#listingTitle").val(data.dataProduct[0].title);
			$("#listingDescription").val(data.dataProduct[0].description);
			$("#eventStartDate").val(data.dataProduct[0].startDate);
			$("#eventEndDate").val(data.dataProduct[0].endDate);
			$("#listingPriceRange").val(data.dataProduct[0].regularPrice);
			$('#listingPriceRange').priceFormat();
			$("#eventAddress").val(data.address);
			$("#regionName").val(data.region);
			//alert(data.region);
			$("#cityName").val(data.city);
			$("#zipCode").val(data.zipcode);
			//display image
			$("#image_preview").html(data.htmlImage);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

//load products by catgeory
function loadMatchingItemsData()
{
	var allCategoryVals = [];
	$('#categoryDiv :checked').each(function() {
		allCategoryVals.push($(this).val());
	});

	var dataSend = {
		action: 'create_listings_request',
		param: 'load_matchingItems',
		CategoryIds:allCategoryVals.join(','),
		post_type: 'event'
	};

	$.ajax({
		url: ajax_url,
		type: "POST",
		data:dataSend,
		action:'create_listings_request',
		dataType: 'json',
		success: function(data){
			createMatchingItemsGrid_grid(data.data);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function createMatchingItemsGrid_grid(jsonToDisplay){
	jQuery("#matchingItemsGrid").jqGrid({
		data: jsonToDisplay,
		datatype: "local",
		rowNum: 30,
		rowList: [10,20,30],
		colNames:['id','Product','Category','Listing Date','SoldOut'],
		colModel:[
			{name:'id',index:'id', width:10, sorttype:"int", key: true, hidden:true},
			{name:'ProductName',index:'ProductName', width:300},
			{name:'Category',index:'Category', width:100, editable:false},
			{name:'ListingDate',index:'ListingDate', width:100, align: "center", formatter:'date', sortable:true, sorttype:'date',
				formatoptions: {srcformat:"m/d/Y ", newformat: 'ShortDate' }, editable:false,edittype:"text",
				editoptions: {size: 10, maxlengh: 100,dataInit : function (elem) {
					//$(elem).datepicker();
				}}},
			{name: "SoldOut", width: 50, align: "center",
				formatter: "checkbox", formatoptions: { disabled: false},
				edittype: "checkbox", editoptions: {value: "Yes:No", defaultValue: "Yes"},
				stype: "select", searchoptions: { sopt: ["eq", "ne"],
				value: ":Any;true:Yes;false:No" } }

		],
		pager: "#pmatchingItemsGrid",
		viewrecords: true,
		height: 250,
		width: null,
		scrollOffset: 0,
		shrinkToFit: false,
		sortname: 'ProductName',
		multiselect: true,
		grouping:true,
		groupingView : {
			groupField : ['Category'],
			groupColumnShow : [false],
			groupCollapse : true,
		},
		caption: "Matching Items"
	});
}

//delete listing
function deleteEventData(){
	$( "#dialog-confirm" ).dialog({
		resizable: false,
		height:280,
		width:400,
		modal: true,
		buttons: {
			"Ok": function() {
				$( this ).dialog( "close" );
				var productId = $("#productId").val();
				var dataSend = {
					action: 'create_listings_request',
					param: 'delete_record',
					id:productId
				};

				$.ajax({
					url: ajax_url,
					type: "POST",
					data:dataSend,
					action:'create_listings_request',
					dataType: 'json',
					success: function(data){
						clearForm();
						displayErrorMessage("#susccessDiv","#successMsg" ,"Product deleted successfully");
					},
					error: function(data) {
						console.log(data);
					}
				});
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		}
	});
}

/*Clear form fields*/
function clearForm(){
	$("#listingTitle").val("");
	$("#listingDescription").val("");
	$("#eventStartDate").val("");
	$("#eventEndDate").val("");
	//$("#listingQty").val("");
	$("#listingPriceRange").val("");
}

//Load user account information
function loadAccountInfo(){
	var dataSend = {
		action: 'create_listings_request',
		param: 'load_accountInfo'
	};

	$.ajax({
		url: ajax_url,
		type: "POST",
		data:dataSend,
		action:'create_listings_request',
		dataType: 'json',
		success: function(data){
			console.log(data);
			$( "#companyName" ).html(" " + data.vendorName);
			$( "#userName" ).html(" " + data.userName);
			$( "#credits" ).html(" " + data.credits);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function displayImage(input)
{
	//$('#loadingmessage').show();
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#previewing').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
	//$('#loadingmessage').show();
}

//create link between product and file
function linkPostWithFile(){
	var data = {
		action: 'create_listings_request',
		param: 'link_file_with_post',
		attach_id: $("#attachId").val(),
		post_id: $("#productId").val()
	};

	$.ajax({
		url: ajax_url,
		type: "POST",
		data:data,
		action:'create_listings_request',
		dataType: 'json',
		success: function(data){
			$("#image_upload").val("");
			unblock_screen();
			if($("#redirect").val() == "Y")
			{
				setTimeout(function(){
					window.location.replace("<?php echo get_site_url();?>/promoted-event-dashboard/");
				}, 2000);
			}

		},
		error: function(data) {
			console.log(data);
		}
	});
}

$(document).ready(function () {
	//Initialized tabs section
	var productId = "<?php  if( isset($_REQUEST["productId"])){echo $_REQUEST["productId"];} else {echo "";}?>";

	$('#listingPriceRange').priceFormat({
		centsLimit: 2,
		thousandsSeparator: ''
	});
	$("#dialog-confirm").hide();
	$( "#tabs" ).tabs();
	$(".accordion div").addClass("active");

	loadMatchingItemsData();
	loadAccountInfo();

	if(productId != null && productId != ""){
		loadProductInformationUpdate(productId);
		loadCategoryData(productId);
	}else{
		loadCategoryData("0");
	}

	//date picker
	$( "#eventStartDate" ).datepicker({
		showOn: "button",
		buttonImage: "<?php echo get_template_directory_uri(); ?>/jquery-ui/images/calendar_icon.png",
		buttonImageOnly: true,
		inline: true
	});

	//date picker
	$( "#eventEndDate" ).datepicker({
		showOn: "button",
		buttonImage: "<?php echo get_template_directory_uri(); ?>/jquery-ui/images/calendar_icon.png",
		buttonImageOnly: true,
		inline: true
	});

	$.datepicker.setDefaults({
		beforeShow: function ( input, inst ) {
			setTimeout(function(){
				inst.dpDiv.css({
					zIndex: 10000
				});
			})
		}
	});
	// START: For the upload image feature
	$("#featured_upload").on('submit',(function(e) {
		e.preventDefault();
		if($("#productId").val() == "0")
		{
			displayErrorMessage("#warningDiv","#warningMsg" ,"Please save information before upload image");
		}
		else{
			if($("#image_upload").val() != "")
			{
				//$('#loadingmessage').show();
				$.ajax({
					url: ajax_url, // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: new FormData($(this)[0]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					processData:false,        // To send DOMDocument or non processed data file it is set to false
					success: function(data)   // A function to be called if request succeeds
					{
						$("#attachId").val(data.data);
						linkPostWithFile();
						//$('#loadingmessage').hide();
					},
					error: function(data) {
						console.log(data);
					}
				});
			}
		}
	}));
	// Function to preview image after validation
	$("#image_upload").change(function() {
		displayImage(this);
	});
	function imageIsLoaded(e) {
		$("#image_upload").css("color","green");
		$('#image_preview').css("display", "block");
		$('#previewing').attr('src', e.target.result);
		$('#previewing').attr('width', '250px');
		$('#previewing').attr('height', '230px');
	};
	//END: For the upload image feature
	$( "#clearrall" ).click(function() {
		$('.listing-wrapper').find('input:text').val('');
		$('.listing-detail').find('input:text').val('');
		$('#listingDescription').val('');
		$('#previewing').removeAttr('src');
		$('input:checkbox').removeAttr('checked');
	});
});



</script>
</body>
