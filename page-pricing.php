<?php
/*
Template Name: Pricing Table
*/
?>

<?php get_header(); ?>
<div id="content">
	<div id="inner-content" class="row">
		<div id="main" class="medium-12 columns" role="main">
			<div class="zl-pricing-table row">
				<div class="medium-12 columns">
					<div class="row">
						<div class="medium-12 columns">
							<span class="message text-center">Click on the plan you wish to join and click Buy Now button at the bottom</span>
						</div>
					</div>
					<ul class="row text-center show-for-medium-up price-header no-bullet">
						<li class="medium-3 medium-offset-3 columns ">Monthly</li>
						<li class="medium-3 columns">Quarterly</li>
						<li class="medium-3 columns">Annual</li>
					</ul>
					<ul class="row price-row text-center silver-package no-bullet">
						<li class="medium-3 columns">Silver Package</li>
						<li class="medium-3 columns show-for-small-only small-monthly">Monthly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3553]' ) ?></li>
						<li class="medium-3 columns show-for-small-only small-quarterly">Quarterly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3556]' ) ?></li>
						<li class="medium-3 columns show-for-small-only small-yearly">Yearly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3557]' ) ?></li>
					</ul>
					<ul class="row price-row text-center gold-package no-bullet">
						<li class="medium-3 columns">Gold Package</li>
						<li class="medium-3 columns show-for-small-only small-monthly">Monthly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3559]' ) ?></li>
						<li class="medium-3 columns show-for-small-only small-quarterly">Quarterly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3560]' ) ?></li>
						<li class="medium-3 columns show-for-small-only small-yearly">Yearly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3561]' ) ?></li>
					</ul>
					<ul class="row price-row text-center platinum-package no-bullet">
						<li class="medium-3 columns">Platinum Package</li>
						<li class="medium-3 columns show-for-small-only small-monthly">Monthly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3562]' ) ?></li>
						<li class="medium-3 columns show-for-small-only small-quarterly">Quarterly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3563]' ) ?></li>
						<li class="medium-3 columns show-for-small-only small-yearly">Yearly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3564]' ) ?></li>
					</ul>
					<ul class="row price-row text-center diamond-package no-bullet">
						<li class="medium-3 columns">Diamond Package</li>
						<li class="medium-3 columns show-for-small-only small-monthly">Monthly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3565]' ) ?></li>
						<li class="medium-3 columns show-for-small-only small-quarterly">Quarterly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3566]' ) ?></li>
						<li class="medium-3 columns show-for-small-only small-yearly">Yearly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3567]' ) ?></li>
					</ul>
					<ul class="row price-row text-center special-package no-bullet">
						<li class="medium-3 columns">Special Package</li>
						<li class="medium-3 columns show-for-small-only small-monthly">Monthly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3568]' ) ?></li>
						<li class="medium-3 columns show-for-small-only small-quarterly">Quarterly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3571]' ) ?></li>
						<li class="medium-3 columns show-for-small-only small-yearly">Yearly</li>
						<li class="medium-3 columns"><?php echo do_shortcode( '[membership_plan id=3569]' ) ?></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="medium-12 columns medium-text-right small-text-center">
					<a id="buy-now" href="#" data-product="0" class="button buy-now">BUY NOW</a>
				</div>
			</div><div class="row">
				<div class="medium-12 columns medium-text-right small-text-center">
					<!--<a href="#" class="button call-now">Need more extra Ads? Call 18001234567 - $7.50 good for 30 days</a>-->
				</div>
			</div>
		</div>
		<!-- end #main -->

	</div>
	<!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
