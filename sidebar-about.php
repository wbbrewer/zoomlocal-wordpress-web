<div id="sidebar-about" class="sidebar-about" role="complementary">
    <div class="row">
        <div class="small-12 small-centered large-uncentered columns">
            <?php if ( is_active_sidebar( 'sidebar-about' ) ) : ?>

                    <?php dynamic_sidebar( 'sidebar-about' ); ?>

            <?php endif; ?>
        </div>
    </div>
</div>