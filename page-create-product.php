<?php
/*
Template Name: Full Width (No Sidebar)
*/
if ( ! $user_id ) {
        $user_id = get_current_user_id();
}

    if (isset($_POST['submitted']) && isset($_POST['post_nonce_field']) && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce')) {

        if (trim($_POST['postTitle']) === '') {
            $postTitleError = 'Please enter a title.';
            $hasError = true;
        }

        $post_information = array(
            'post_author' => $user_id,
            'post_title' => wp_strip_all_tags($_POST['postTitle']),
            'post_content' => $_POST['postContent'],
            'post_type' => 'product',
            'post_status' => 'publish'
        );

        $post_id = wp_insert_post($post_information);

        if ($post_id) {
            update_post_meta($post_id, '_visibility', 'visible');

            /*
              update_post_meta( $post_id, '_stock_status', 'instock');
              update_post_meta( $post_id, 'total_sales', '0');
              update_post_meta( $post_id, '_downloadable', 'yes');
              update_post_meta( $post_id, '_virtual', 'yes');
              update_post_meta( $post_id, '_regular_price', "1" );
              update_post_meta( $post_id, '_sale_price', "1" );
              update_post_meta( $post_id, '_purchase_note', "" );
              update_post_meta( $post_id, '_featured', "no" );
              update_post_meta( $post_id, '_weight', "" );
              update_post_meta( $post_id, '_length', "" );
              update_post_meta( $post_id, '_width', "" );
              update_post_meta( $post_id, '_height', "" );
              update_post_meta($post_id, '_sku', "");
              update_post_meta( $post_id, '_product_attributes', array());
              update_post_meta( $post_id, '_sale_price_dates_from', "" );
              update_post_meta( $post_id, '_sale_price_dates_to', "" );
              update_post_meta( $post_id, '_price', "1" );
              update_post_meta( $post_id, '_sold_individually', "" );
              update_post_meta( $post_id, '_manage_stock', "no" );
              update_post_meta( $post_id, '_backorders', "no" );
              update_post_meta( $post_id, '_stock', "" );
             */
            //wp_redirect( home_url() );
            //exit;
        }
    }
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content" class="row">
			
				    <div id="main" class="large-12 medium-12 columns" role="main">
					
                                        <?php if ( user_can( $user_id, 'manage_woocommerce' ) ) : ?>
                                        <form action="" id="primaryPostForm" method="POST">
 
                                            <fieldset>
                                                <label for="postTitle"><?php _e('Post Title:', 'framework') ?></label>

                                                <input type="text" name="postTitle" id="postTitle" class="required" />
                                            </fieldset>

                                            <fieldset>
                                                <label for="postContent"><?php _e('Post Content:', 'framework') ?></label>

                                                <textarea name="postContent" id="postContent" rows="8" cols="30" class="required"></textarea>
                                            </fieldset>

                                            <fieldset>
                                                <input type="hidden" name="submitted" id="submitted" value="true" />
                                                <?php wp_nonce_field( 'post_nonce', 'post_nonce_field' ); ?>
                                                <button type="submit"><?php _e('Add Post', 'framework') ?></button>
                                            </fieldset>

                                        </form>
                                        <?php else: 
                                            wp_redirect(home_url());
                                            //
                                        ?>

                                        <?php endif; ?>
                                        
                                        
                                        
						<?php get_template_part( 'parts/loop', 'page' ); ?>
                                        
					    					
    				</div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->
<?php get_footer(); ?>
