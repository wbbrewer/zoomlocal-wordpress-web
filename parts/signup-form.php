<div id="myModals" class="reveal-modal reveal login" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
 <div class="main iframeform">
<form id="register" class="ajax-auth"  action="register" method="post">
   <a aria-label=""><img class="sm-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_125pxH.png" /></a>

        <a class="close-button" data-close aria-label="Close reveal" >&#215;</a>
    <!-- header -->

        <div class="row centered collapse">

            <div class="small-10 small-centered medium-8 columns">

                <h3>Welcome to ZoomLocal</h3>

                

                <p class="status"></p>

            </div>

        </div>
    <!-- <p class="status"></p> -->
    <?php wp_nonce_field('ajax-register-nonce', 'signonsecurity'); ?>         
     <!-- email address -->

        <div class="row centered">

            <div class="small-10 small-centered medium-8 columns iframeemail">
     <input id="email" type="text" class="required email" name="email" placeholder="email address">

    </div>

        </div>

        <!-- password -->
   
     <div class="row centered">
    <div class="small-10 small-centered medium-8 columns iframepassword">
    <input id="signonpassword" type="password" class="required" name="signonpassword" placeholder="password" >
    </div>
    </div>
     <!-- Confirm password -->

        <div class="row centered">

            <div class="small-10 small-centered medium-8 columns iframepassword">
    <input type="password" id="password2" class="required" name="password2" placeholder="confirm password">
    </div>
    </div>

    <div class="row centered">
          <div class="small-10 small-centered medium-8 columns iframe-btn">

    <input class="submit_button" type="submit" value="Join ZoomLocal">
    </div>
    </div>  
     <!-- or sign in using -->
      <div class="row centered">
        <div class="small-10 small-centered medium-8 columns iframetext">
            <p><span class="social-signin">or sign in using your account with</span></p>
        </div>

                                </div>
                                <!-- facebook, twitter, google -->
                                <div class="row">

                                    <div class="small-12 small-centered medium-12 medium-uncentered columns text-center iframesocial">
                                        <a target="_blank" rel="nofollow" href="<?php echo site_url( 'wp-login.php', 'https' ); ?>?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Facebook&amp;redirect_to=<?php $redirect_url = site_url( 'my-account', 'https' ); echo urlencode($redirect_url); ?>" title="Connect with Facebook" class="button icon facebook" data-provider="Facebook">
                                            <span><i class="fa fa-facebook"></i><?php _e('Facebook'); ?></span>
                                        </a>
                                    
                                        <a target="_blank" rel="nofollow" href="<?php echo site_url( 'wp-login.php', 'https' ); ?>?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Twitter&amp;redirect_to=<?php $redirect_url = site_url( 'my-account', 'https' ); echo urlencode($redirect_url); ?>" title="Connect with Twitter" class="button icon twitter" data-provider="Twitter">
                                            <span><i class="fa fa-twitter"></i><?php _e('Twitter'); ?></span>
                                        </a>
                                    
                                        <a target="_blank" rel="nofollow" href="#" title="Connect with Google+" class="button icon google-plus" data-provider="Google">
                                            <span><i class="fa fa-google-plus"></i><?php _e('Google'); ?></span>
                                        </a>
                                    </div>

                                </div>
                                <!-- Cancel --> 
                                <div class="row centered collapse">

                                    <div class="small-10 small-centered medium-7 columns">
                                        <a aria-label="Close reveal" data-close="" class="cancel-modal">Cancel</a>
                                    </div>

                                </div>
</form>
</div>
</div>