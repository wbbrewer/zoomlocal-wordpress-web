<div id="myModal" class="reveal-modal reveal login" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<form id="login" action="login" method="post">
    	<a aria-label=""><img class="sm-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_125pxH.png" /></a>
        <a class="close-button" data-close aria-label="Close reveal" >&#215;</a>
        <!-- header -->
        <div class="row centered collapse">
			<div class="small-10 small-centered medium-8 columns">
            	<h3>Account Sign In</h3>
                <hr>
                <p class="status"></p>
			</div>
		</div>
        <!-- email address -->
        <div class="row centered">
			<div class="small-10 small-centered medium-8 columns">
            	<input id="username" type="text" placeholder="email address">
            </div>
		</div>
        <!-- password -->
        <div class="row centered">
			<div class="small-10 small-centered medium-8 columns">
            	<input id="password" type="password" placeholder="password">
			</div>
		</div>
		<!-- remember me -->
		<div class="row">
			<div class="small-10 small-centered medium-8 columns">
				<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e('Remember Me', 'zoomlocal'); ?>
			</div>
		</div>
        <!-- forgot your password? -->
        <div class="row">
			<div class="small-10 small-centered medium-8 columns">
            <p><a class="forgetmenot" href="<?php echo esc_url(wc_lostpassword_url()); ?>"><?php _e('Forgot your password?', 'zoomlocal'); ?></a></p>
			</div>
		</div>
        <!-- login -->
        <div class="row centered">
			<div class="small-10 small-centered medium-8 columns">
            	<input class="button loginbutton" type="submit" value="<?php _e('Login'); ?>">
			</div>
		</div>
        <!-- or sign in using -->
      <div class="row centered">
		<div class="small-10 small-centered medium-8 columns">
        	<p><span class="social-signin">or sign in using your account with</span></p>
        </div>

                                </div>
                                <!-- facebook, twitter, google -->
                                <div class="row">

                                    <div class="small-12 small-centered medium-12 medium-uncentered columns text-center">
                                        <a rel="nofollow" href="<?php echo site_url( 'wp-login.php', 'https' ); ?>?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Facebook&amp;redirect_to=<?php $redirect_url = site_url( 'my-account', 'https' ); echo urlencode($redirect_url); ?>" title="Connect with Facebook" class="button icon facebook" data-provider="Facebook">
                                            <span><i class="fa fa-facebook"></i><?php _e('Facebook'); ?></span>
                                        </a>
                                    
                                        <a rel="nofollow" href="<?php echo site_url( 'wp-login.php', 'https' ); ?>?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Twitter&amp;redirect_to=<?php $redirect_url = site_url( 'my-account', 'https' ); echo urlencode($redirect_url); ?>" title="Connect with Twitter" class="button icon twitter" data-provider="Twitter">
                                            <span><i class="fa fa-twitter"></i><?php _e('Twitter'); ?></span>
                                        </a>
                                    
                                        <a rel="nofollow" href="#" title="Connect with Google+" class="button icon google-plus" data-provider="Google">
                                            <span><i class="fa fa-google-plus"></i><?php _e('Google'); ?></span>
                                        </a>
                                    </div>

                                </div>
                                <!-- Cancel -->
                                <div class="row centered collapse">

                                    <div class="small-10 small-centered medium-7 columns">
                                        <a class="cancel-modal" data-close aria-label="Close reveal">Cancel</a>
                                    </div>

                                </div>
                                <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>
                            </form>
                        </div>