<div id="create-event-header" class="row">
	<div class="medium-4 columns">
		<p>Welcome: <span id="username"><?php echo $current_user->display_name;?></span></p>

		<p>Company: <span id="company-name"><?php echo $yith_shop->name;?></span></p>
<!--
		<p>Subscription info: <span id="subscription-info">Some Subscription Info</span></p>-->
	</div>
	<div class="medium-8 columns ">
		<form  id="create-event-form"  method="post" action="#" enctype="multipart/form-data">
			<div class="row">
				<div class="small-3 column">
					<label for="event-title" class="inline"><?php _e( 'Event Title' ); ?><span
							class="required">*</span></label>
				</div>
				<div class="small-9 column">
					<input type="text" class="input-text" name="event-title" id="event-title"
					       value=""/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 column">
					<label for="event-description" class="inline"><?php _e( 'Event Description' ); ?><span
							class="required">*</span></label>
				</div>
				<div class="small-9 column">
					<textarea name="event-description" id="event-description"><?php echo "";//some event description?></textarea>
				</div>
			</div>
			<span>Event Details</span>

			<div class="row">
				<div class="small-3 column">
					<label for="event-address-street" class="inline"><?php _e( 'Street' ); ?><span
							class="required">*</span></label>
				</div>
				<div class="small-9 column">
					<input type="text" class="input-text" name="event-address-street" id="event-address-street"
					       value=""/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 column">
					<label for="event-address-city" class="inline"><?php _e( 'City' ); ?><span
							class="required">*</span></label>
				</div>
				<div class="small-9 column">
					<input type="text" class="input-text" name="event-address-city" id="event-address-city"
					       value=""/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 column">
					<label for="event-address-state" class="inline"><?php _e( 'State' ); ?><span
							class="required">*</span></label>
				</div>
				<div class="small-9 column">
					<input type="text" class="input-text" name="event-address-state" id="event-address-state"
					       value=""/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 column">
					<label for="event-address-zip" class="inline"><?php _e( 'Zip' ); ?><span
							class="required">*</span></label>
				</div>
				<div class="small-9 column">
					<input type="text" class="input-text" name="event-address-zip" id="event-address-zip"
					       value=""/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 column">
					<label for="event-date-start" class="inline"><?php _e( 'Start Date' ); ?><span
							class="required">*</span></label>
				</div>
				<div class="small-9 column">
					<input type="text" class="input-text" name="event-date-start" id="event-date-start"
					       value=""/>
				</div>
			</div>
			<div class="row">
				<div class="small-3 column">
					<label for="event-date-end" class="inline"><?php _e( 'End Date' ); ?></label>
				</div>
				<div class="small-9 column">
					<input type="text" class="input-text" name="event-date-end" id="event-date-end"
					       value=""/>
				</div>
			</div>
			<span>Event Image/Poster</span>

			<div class="row">
				<div class="small-3 columns">
					<img
						src="<?php echo ( false ) ? wp_get_attachment_url( 0 ) : wc_placeholder_img_src(); ?>">
				</div>
				<div class="small-9 columns">
					<input type="file" name="my_image_upload" id="my_image_upload" multiple="false"/>
					<?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
					<!--<input class="button" id="submit_my_image_upload" name="submit_my_image_upload" type="submit"
					       value="Upload"/>-->
				</div>
			</div>
			<div id="status"><span id="error-message"></span></div>
			<div class="text-center">
				<input type="hidden" class="input-text" name="event-lat" id="event-lat"
				       value=""/>
				<input type="hidden" class="input-text" name="event-lng" id="event-lng"
				       value=""/>
				<input type="hidden" class="input-text" name="event-id" id="event-id"
				       value="<?php echo (isset($_GET['productId']))?sanitize_text_field($_GET['productId']):"";?>"/>
				<a id="submit-event-button" href="#"  class="button">Create Event</a>
			</div>
		</form>

	</div>
</div>