<?php
/*display event line items called by zl-event-dashboard.php*/
$data = array(
	'ID' => $post->ID,
	'title' => $post->post_title,
	'list-date' => date('Y-m-d', strtotime($post->post_date)), //date('Y-m-d', strtotime($post->post_date)),
	'start-date' => get_post_meta($post->ID,'_rq_event_start_date',true),
	'location' => get_post_meta($post->ID,'_rq_event_city_name',true).' '.get_post_meta($post->ID,'_rq_event_region_name',true),
	'featured' => get_post_meta($post->ID,'_featured',true)
);
?>
<tr class="event-row" data-id="<?php echo $data['ID']; ?>">
	<td><?php echo $data['ID']; ?></td>
	<td><?php echo $data['title']; ?></td>
	<td><?php echo $data['list-date']; ?></td>
	<td><?php echo ($data['start-date'])?$data['start-date']:""; ?></td>
	<td class="text-capitalized"><?php echo ($data['location'])?$data['location']:""; ?></td>
	<td><?php echo ($data['featured']=="yes")?$data['featured']:"no"; ?></td>
</tr>

