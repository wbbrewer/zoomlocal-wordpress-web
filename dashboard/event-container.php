
<div id="event-dashboard-container" class="row">
	<div class="medium-12 columns">
		<section id="intro-message" class="row">
			<h3>Events Dashboard</h3>
			<div class="row">
				<div class="small-3 columns">
					<span>Username:</span>
					<span id="user-name"><?php echo $current_user->user_login?></span>
				</div>
			</div>
			<div class="row">
				<div class="small-3 columns">
					<span>Remaining Credits:</span><span id="remaining-credits"><?php echo zl_get_user_credits($yith_shop->id);?></span>
				</div>
			</div>
			<p><a id="event-add-new" class="button">Add New Event</a></p>
		</section>
	</div>
	<div class="row">
		<section id="event-list-section" class="medium-7 columns">
		</section>
		<section id="event-details" class="medium-5 columns">

		</section>
	</div>
</div>