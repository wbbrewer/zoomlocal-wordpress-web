<?php

?>
<div class="row">
	<div class="large-12 columns">
		<h3>Categories (Optional)</h3>
		<h5>Check as many categories of interest. Change selections as often as you´d like</h5>
		<div id="edit-category" class="menu-categs-box">
			<?php
			//$category_preference = json_decode( get_user_meta( $user->ID, 'category_preference', true ) );
			$cats = array();

			if(isset($event_id)){
				$categories_list = wp_get_post_terms($event_id,'product_cat');
				if(!empty($categories_list)){
					foreach($categories_list as $category){
						$cats[]=$category->slug;
					}
				}
			}

			$wcatTerms           = get_terms( 'product_cat', array(
				'hide_empty' => 0,
				'orderby'    => 'ASC',
				'parent'     => 0
			) );

			foreach ( $wcatTerms as $wcatTerm ) :
				?>
				<ul class="accordion" data-accordion role="tablist" data-allow-all-closed="true" data-multi-expand="true">
					<li class="accordion-item">
						<a id ="<?php echo $wcatTerm->term_id; ?>" class="panel-heading" href="#<?php echo $wcatTerm->slug; ?>" role="tab"
						   aria-controls="<?php echo $wcatTerm->slug; ?>"><?php echo $wcatTerm->name; ?></a>

						<div id="<?php echo $wcatTerm->slug; ?>" class="accordion-content" role="tabpanel"
						     aria-labelledby="<?php echo $wcatTerm->slug; ?>" data-tab-content aria-labelledby="<?php echo $wcatTerm->term_id; ?>">
							<ul class="wsubcategs">
								<?php $wsubargs = array(
									'hierarchical'     => 1,
									'show_option_none' => '',
									'hide_empty'       => 0,
									'parent'           => $wcatTerm->term_id,
									'taxonomy'         => 'product_cat'
								);
								$wsubcats       = get_categories( $wsubargs );?>
								<div class="row collapse">
									<div class="large-12 columns">
										<?php foreach ( $wsubcats as $wsc ): ?>
												<div class="row">
													<div class="small-1 columns">
														<input name="cat_group[]" value="<?php echo $wsc->slug; ?>" <?php if(in_array($wsc->slug,$cats)){echo "checked";}?> type="checkbox">
													</div>
													<div class="small-11 columns">
														<label for="cat_group[]"> <?php echo $wsc->name; ?></label>
													</div>
												</div>
										<?php endforeach; ?>
									</div>
								</div>
							</ul>
						</div>
					</li>
				</ul>
			<?php endforeach; ?>
		</div>
	</div>
</div>