<?php
if(isset($_POST['event_id'])){
	$event_id = $_POST['event_id'];
	$post_args = array(
		'post_type' => 'product',
		'author' => get_current_user_id(),
		'include' => $event_id
	);
	$post_array = get_posts($post_args);
	if(empty($post_array)){
		$access=false;
		$message="You done have access to this event. Please Contact Our Customer Service Team.";
	}else{
		foreach ( $post_array as $post ) : setup_postdata( $post );
		endforeach;
	}
	wp_reset_postdata();
}
?>

<form  id="create-event-form"  method="post" action="#" enctype="multipart/form-data">
	<?php //var_dump($post_array);?>
	<div class="row">
		<div class="small-3 column">
			<label for="event-title" class="inline"><?php _e( 'Event Title' ); ?><span
					class="required">*</span></label>
		</div>
		<div class="small-9 column">
			<input type="text" class="input-text" name="event-title" id="event-title"
			       value="<?php echo (isset($post))?$post->post_title:"";?>"/>
		</div>
	</div>
	<div class="row">
		<div class="small-3 column">
			<label for="event-description" class="inline"><?php _e( 'Event Description' ); ?><span
					class="required">*</span></label>
		</div>
		<div class="small-9 column">
			<textarea style="height: auto;" name="event-description" id="event-description"><?php echo (isset($post))?$post->post_content:"";?></textarea>
		</div>
	</div>
	<?php
	if(isset($_POST['event_id'])){
	?>
	<div class="row">
		<div class="small-3 column">
			<label for="event-listing-date" class="inline"><?php _e( 'Listing Date' ); ?></label>
		</div>
		<div class="small-9 column">
			<input type="text" class="input-text" name="event-listing-date" id="event-listing-date"
			       value="<?php echo (isset($post))? date('Y-m-d', strtotime($post->post_date)):"";?>"/></div>
	</div>
	<div class="row">
		<div class="small-3 column">
			<label for="event-promoted" class="inline"><?php _e( 'Promoted' ); ?></label>
		</div>
		<div class="small-9 column">
			<select id="event-promoted" name="event-promoted">
				<option value="no" <?php echo (isset($post))?((get_post_meta($post->ID,'_featured',true)=="no")?" selected=\"selected\"":""):"";?>>No</option>
				<option value="yes" <?php echo (isset($post))?((get_post_meta($post->ID,'_featured',true)=="yes")?" selected=\"selected\"":""):"";?>>Yes</option>
			</select>
		</div>
	</div>
	<?php
	}
	?>
	<span>Event Details</span>

	<div class="row">
		<div class="small-3 column">
			<label for="event-address-street" class="inline"><?php _e( 'Street' ); ?><span
					class="required">*</span></label>
		</div>
		<div class="small-9 column">
			<input type="text" class="input-text" name="event-address-street" id="event-address-street"
			       value="<?php echo (isset($post))?get_post_meta($post->ID,'_rq_event_address_name',true):"";?>"/>
		</div>
	</div>
	<div class="row">
		<div class="small-3 column">
			<label for="event-address-city" class="inline"><?php _e( 'City' ); ?><span
					class="required">*</span></label>
		</div>
		<div class="small-9 column">
			<input type="text" class="input-text" name="event-address-city" id="event-address-city"
			       value="<?php echo (isset($post))?get_post_meta($post->ID,'_rq_event_city_name',true):"";?>"/>
		</div>
	</div>
	<div class="row">
		<div class="small-3 column">
			<label for="event-address-state" class="inline"><?php _e( 'State' ); ?><span
					class="required">*</span></label>
		</div>
		<div class="small-9 column">
			<input type="text" class="input-text" name="event-address-state" id="event-address-state"
			       value="<?php echo (isset($post))?get_post_meta($post->ID,'_rq_event_region_name',true):"";?>"/>
		</div>
	</div>
	<div class="row">
		<div class="small-3 column">
			<label for="event-address-zip" class="inline"><?php _e( 'Zip' ); ?><span
					class="required">*</span></label>
		</div>
		<div class="small-9 column">
			<input type="text" class="input-text" name="event-address-zip" id="event-address-zip"
			       value="<?php echo (isset($post))?get_post_meta($post->ID,'_rq_event_zip_code',true):"";?>"/>
		</div>
	</div>
	<div class="row">
		<div class="small-3 column">
			<label for="event-date-start" class="inline"><?php _e( 'Start Date' ); ?><span
					class="required">*</span></label>
		</div>
		<div class="small-9 column">
			<input type="text" class="input-text" name="event-date-start" id="event-date-start"
			       value="<?php echo (isset($post))?get_post_meta($post->ID,'_rq_event_start_date',true):"";?>"/>
		</div>
	</div>
	<div class="row">
		<div class="small-3 column">
			<label for="event-date-end" class="inline"><?php _e( 'End Date' ); ?></label>
		</div>
		<div class="small-9 column">
			<input type="text" class="input-text" name="event-date-end" id="event-date-end"
			       value="<?php echo (isset($post))?get_post_meta($post->ID,'_rq_event_end_date',true):"";?>"/>
		</div>
	</div>
	<span>Event Image/Poster</span>

	<div class="row">
		<div class="small-3 columns">
			<img id="image-upload"
				src="<?php echo (isset($post))?((wp_get_attachment_url( get_post_thumbnail_id($post->ID)  ))? (wp_get_attachment_url( get_post_thumbnail_id($post->ID)  )) : wc_placeholder_img_src()):wc_placeholder_img_src(); ?>">
		</div>
		<div class="small-9 columns">
			<input type="file" name="my_image_upload" id="my_image_upload" multiple="false"/>
			<?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
			<!--<input class="button" id="submit_my_image_upload" name="submit_my_image_upload" type="submit"
				   value="Upload"/>-->
		</div>
	</div>
	<?php
	//categories
	include(locate_template('dashboard/categories-list.php'));
	?>
	<div id="status">
		<div class="alert-box warning">
			<strong>Oppps... </strong><span id="error-message"></span>
		</div>
	</div>
	<div class="text-center">
		<input type="hidden" class="input-text" name="event-lat" id="event-lat"
		       value="<?php echo (isset($post))?get_post_meta($post->ID,'_rq_event_lat_name',true):"";?>"/>
		<input type="hidden" class="input-text" name="event-lng" id="event-lng"
		       value="<?php echo (isset($post))?get_post_meta($post->ID,'_rq_event_lon_name',true):"";?>"/>
		<input type="hidden" class="input-text" name="old-listing-date" id="old-listing-date"
		       value="<?php echo (isset($post))? date('Y-m-d', strtotime($post->post_date)):"";?>"/>
		<input type="hidden" class="input-text" name="old-promoted" id="old-promoted"
		       value="<?php echo (isset($post))?get_post_meta($post->ID,'_featured',true):"";?>"/>
		<input type="hidden" class="input-text" name="event-id" id="event-id"
		       value="<?php echo (isset($event_id))?$event_id:"";?>"/>
		<?php if(isset($_POST['event_id'])){
			?>
			<a id="update-event-button" href="#"  class="button">Update</a>
			<a id="delete-event-button" href="#"  class="button">Delete</a>
			<div>
				<h6>Share this listing!</h6>
				<ul class="soc-list inline-list right">
					<li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink($post->ID); ?>"><img
								src="<?php echo get_template_directory_uri() . "/images/icons/fb.png"; ?>"/></a>
					</li>
					<li><a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php  echo get_permalink($post->ID); ?>&media=&description="><img
								src="<?php echo get_template_directory_uri() . "/images/icons/pin.png"; ?>"/></a>
					</li>
					<li><a target="_blank" href="https://twitter.com/home?status=<?php  echo  get_permalink($post->ID); ?>"><img
								src="<?php echo get_template_directory_uri() . "/images/icons/tw.png"; ?>"/></a>
					</li>
					<li><a target="_blank" href="https://plus.google.com/share?url=<?php  echo  get_permalink($post->ID); ?>"><img
								src="<?php echo get_template_directory_uri() . "/images/icons/gplus.png"; ?>"/></a>
					</li>
					<li><a href="mailto:?subject=<?php urlencode(the_title('Check this listing at Zoomlocal: '))?>&body=Hey,%20I%20found%20this%20listing%20at%20Zoomlocal.%20Check%20this%20link%20out%20<?php  get_permalink($post->ID);?>"><img
								src="<?php echo get_template_directory_uri() . "/images/icons/mail.png"; ?>"/></a>
					</li>
					<li><a href="#" onclick="window.prompt('Copy the link: Ctrl+C/Cmd-C, Enter', '<?php  echo  get_permalink($post->ID); ?>');"><img
								src="<?php echo get_template_directory_uri() . "/images/icons/dots.png"; ?>"/></a>
					</li>
				</ul>
			</div>
			<?php
		}
		else{?>
		<a id="create-event-button" href="#"  class="button">Create Event</a>
		<?php }?>
	</div>
</form>