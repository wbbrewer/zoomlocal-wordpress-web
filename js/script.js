var $ = jQuery.noConflict();

var Project = {

	init: function()
	{
		$('html').removeClass('no-js');
		Project.initFormFields();
	},
	initFormFields : function()
	{
		$('input[type="text"], input[type="email"], input[type="tel"], input[type="password"], textarea').focus(function() {
			if ($(this).val() === $(this).prop('defaultValue')) {
				$(this).val('');
			}
		});

		$('input[type="text"], input[type="email"], input[type="tel"], input[type="password"], textarea').blur(function() {
			if ($(this).val() === '') {
				$(this).val($(this).prop('defaultValue'));
			}
		});
	}
}

jQuery(document).ready(
	function()
	{
		Project.init();

		$(".hamburger").on('click', function(e){
			e.preventDefault();
			if(window.innerWidth<768){
				$('.mobile-menu').stop(1,0).slideToggle(250,
					function(){
						if($(this).css('display') == 'none'){
							$(this).css('display', '');
						}
					});
			}
		});

		function layout(){
			if(window.innerWidth >=768){
				$('.mobile-menu').css('display', 'none');

			}
		}

		$(window).resize(function(){
			layout();
		})
	}
	);
