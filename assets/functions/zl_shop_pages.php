<?php


//Simple class for the filters
class ZL_Filters {
	function zl_shop_filters() {
		?>

		<form action="" method="post" class="zl-filters">
			<div id="categoriesModal" class="reveal-modal reveal" data-reveal aria-labelledby="categoriesModalTitle"
			     aria-hidden="true" role="dialog">
			     <a aria-label=""><img class="sm-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_125pxH.png" /></a>
				 <a class="close-button" data-close aria-label="Close reveal" >&#215;</a>
				<!--<a href="#" data-reveal-id="categoriesModal" class="button">Categories&hellip;</a>-->
				<div class="row zl-filters">
					<div class="large-12 columns">
                    <h3 class="categoriesModalTitle"><i class="fa fa-tags"></i>Choose Categories you like</h3>
                    <hr>
						<!-- <h2 id="categoriesModalTitle">Categories List</h2>
                        			<h5>Select one or more categories and close to see results.</h5> -->
						<div class="menu-categs-box">
							<div class="accordion">
							<?php $wcatTerms = get_terms( 'product_cat', array(
								'hide_empty' => 1,
								'orderby'    => 'ASC',
								'parent'     => 0,
							) );
							foreach ( $wcatTerms as $wcatTerm ) :
								?>
								<h1><?php echo $wcatTerm->name; ?></h1>
								<div  class="checked-category">
								<div id="<?php echo $wcatTerm->slug; ?>" class="content active" role="tabpanel"
										     aria-labelledby="<?php echo $wcatTerm->slug; ?>">
											<ul class="wsubcategs">
												<?php $wsubargs = array(
													'hierarchical'     => 1,
													'show_option_none' => '',
													'hide_empty'       => 1,
													'parent'           => $wcatTerm->term_id,
													'taxonomy'         => 'product_cat'
												);
												$wsubcats       = get_categories( $wsubargs );?>
												<div class="row">
													<div class="large-12 columns">
													<div class="row">
														<?php foreach ( $wsubcats as $wsc ): ?>
															<div class="large-4 columns"><input name="cat_group[]"
															                                    value="<?php echo $wsc->slug; ?>" id="<?php echo $wsc->slug; ?>"
															                                    type="checkbox"><label
																	for="<?php echo $wsc->slug; ?>"><span></span>
																	<?php echo $wsc->name; ?></label></div>
														<?php endforeach; ?>
														</div>
													</div>
												</div>
											</ul>
										</div>
									</div>
								
							<?php endforeach; ?>
							</div>
						</div>
						<div class="row text-center">
						<a class="cancelmodel button" data-close aria-label="Close reveal" href="#">Apply</a>
						<a class="cancelmodel applys button" href="#">Clear All</a>
						<!-- <a class="cancelmodel button" data-close aria-label="Close reveal" href="#">Cancel</a> -->
						</div>
					</div>
				</div>
				<!--End Categories-->
			</div>
			<div id="locationsModal" class="reveal-modal reveal" data-reveal aria-labelledby="locationsModalTitle"
			           aria-hidden="true" role="dialog">
			           <a aria-label=""><img class="sm-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_125pxH.png" /></a>
				 <a class="close-button" data-close aria-label="Close reveal" >&#215;</a>

				<div id="locations-panel">
					<ul class="wsubcategs">
						<div class="row">
							<h3 id="locationsModalTitle"><i class="fa fa-tags"></i><?php _e( 'Zoom into an area near you' ); ?></h3>
							<hr>
							<!--<h5>Select one or more locations and close to see results.</h5>-->
							<div class="large-12 columns">
								<?php
								$location_list  = json_decode( file_get_contents( get_template_directory() . '/assets/json/locations.json' ) );
								$location_array = array();
								foreach ( $location_list as $location ) {
									if ( ! in_array( $location->Group, $location_array ) ) {
										$location_array[] = $location->Group;
									}
								}//end foreach
								//print_r($location_array);
								foreach ( $location_array as $key ) {
									?>
									<div class="large-4 columns"><input id="<?php echo $key; ?>" name="loc_group[]" value="<?php echo $key; ?>"
									                                    type="checkbox">
										<label for="<?php echo $key; ?>"><span></span><?php echo $key; ?></label>
									</div>
								<?php }//end foreach ?>
							</div>
						</div>
					</ul>
					<div class="row text-center">
					<a class="cancelmodel button" data-close aria-label="Close reveal" href="#">Apply</a>
					<a class="cancelmodel applys button" href="#">Clear All</a>
					<!-- <a class="cancelmodel button" data-close aria-label="Close reveal" href="#">Cancel</a> -->
					</div>
				</div>
			</div>
			<div id="distanceModal" class="reveal-modal reveal" data-reveal aria-labelledby="distanceModalTitle" aria-hidden="true" role="dialog">
				 <a aria-label=""><img class="sm-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_125pxH.png" /></a>
				 <a class="close-button" data-close aria-label="Close reveal" >&#215;</a>
				<h3 id="distanceModalTitle"><i class="fa fa-location-arrow"></i><?php _e( 'Filter results By Distance' ); ?></h3>
				<hr>
				
				<div class="row ">
					<div id="location-filter-details">
						<div class="medium-12 columns text-center">
						<p class='selectloc'><i class="fa fa-compass"></i>Select a distance from your current location to filter the listings.
							
	
							<!--<a id="use-current-location" href="#"class="button"> <i class="fa fa-map-marker"></i> Use Current Location</a>-->
							<select id="current-location">
								<option value="0" selected>Select Distance</option>
								<option value="5">5 Miles</option>
								<option value="10">10 Miles</option>
								<option value="25">25 Miles</option>
								<option value="50">50 Miles</option>
								<option value="100">100 Miles</option>
								<option value="250">250 Miles</option>
							</select></p>
						</div>
						<div class="medium-12 columns text-center">
							<p id="latitudeAndLongitude"></p>
						</div>
					</div>
					<div id="location-filter-message">
					</div>

				</div>
				<div class="row text-center">
				<a class="cancelmodel button" data-close aria-label="Close reveal" href="#">Apply</a>
				<a class="cancelmodel applys button" href="#">Clear All</a>
				<!-- <a class="cancelmodel button" data-close aria-label="Close reveal" href="#">Cancel</a> -->
				</div>
			</div>
		</form>
	<?php
	}

	function zl_shop_lcoation_filters() {
		?>
		<form action="" method="post" class="zl-filters">
			<div id="locations-panel">
				<ul class="wsubcategs">
					<div class="row">
						<h3><?php _e( 'Filter By Location' ); ?></h3>

						<div class="large-12 columns">
							<?php
							$location_list  = json_decode( file_get_contents( get_template_directory() . '/assets/json/locations.json' ) );
							$location_array = array();
							foreach ( $location_list as $location ) {
								if ( ! in_array( $location->Group, $location_array ) ) {
									$location_array[] = $location->Group;
								}
							}//end foreach
							foreach ( $location_array as $key ) {
								?>
								<div class="large-4 columns"><input name="loc_group[]" value="<?php echo $key; ?>"
								                                    type="checkbox">
									<label for="loc_group[]"><?php echo $key; ?></label>
								</div>
							<?php }//end foreach ?>
						</div>
					</div>
				</ul>
			</div>

		</form>


	<?php
	}
	function zl_load_more($next=null){
		global $args;
		$data_query = wp_json_encode($args);
		if($next<>null){
			$data_query = wp_json_encode($next);
		}
		if($data_query<>null){
			echo '<div class="text-center"><span><a class="button" id="zl-load-more" href="#" data-query="'.htmlentities($data_query,ENT_QUOTES, 'UTF-8').'">Load More</a></span></div>';
		}
	}

}

add_action( 'wp_ajax_nopriv_zl_load_more_ajax', 'zl_load_more_ajax' );
add_action( 'wp_ajax_zl_load_more_ajax', 'zl_load_more_ajax' );
function zl_load_more_ajax(){
	global $wp_query;
	$next_page = new ZL_Filters();
	$filtered="";
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ){
		$args = isset($_POST['args'])? json_decode(stripslashes($_POST['args']),true) : array();
		$args['paged']=$args['paged']+1;
		if($args<>null){
			$wp_query            = new WP_Query( $args );
			ob_start();
			if ( $wp_query->have_posts() ) {
				while ( have_posts() ) {
					the_post();
					wc_get_template_part( 'content', 'product' );
				}
				//if(get_next_posts_link('',$wp_query->max_num_pages)<>null){
				if($wp_query->max_num_pages>$args['paged']){
					$next_page->zl_load_more($args);
				}
				$filtered = ob_get_contents();
			}
			ob_end_clean();
			echo ($filtered=="")?" ":$filtered;
			wp_reset_postdata();
			woocommerce_reset_loop();
		}
		die();
	}else{
		//errors
		exit();
	}
}
//add ajax functions for filters

add_action( 'wp_ajax_nopriv_zl_do_shop_filters', 'zl_do_shop_filters' );
add_action( 'wp_ajax_zl_do_shop_filters', 'zl_do_shop_filters' );
function zl_do_shop_filters() {
	global $wp_query, $wp_rewrite, $wpdb;
	$next_page         = new ZL_Filters();
	$products_per_page = 25;
	$filtered          = "";
	$do_events         = ( isset( $_POST['use_events'] ) && $_POST['use_events'] ) ? true : false;
	$temp_yr           = $_POST['search_year'];
	$temp_wk           = $_POST['search_week'];
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		if ( $_REQUEST['tab'] === '#daily-listing' ) {
			$category_preference = json_decode( get_user_meta( get_current_user_id(), 'category_preference', true ) );
			$args                = array(
				'post_type'      => 'product',
				'posts_per_page' => $products_per_page,
				'product_cat'    => implode( ',', $category_preference ),
				'paged'          => ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1
			);
			if ( isset( $_POST['referer'] ) && $_POST['referer'] == 1 ) {
				//only featured posts from last 30 days
				$meta_query         = WC()->query->get_meta_query();
				$meta_query[]       = array(
					'key'   => '_featured',
					'value' => 'yes'
				);
				$args['meta_query'] = $meta_query;
				//only post dates with from last month
				$args['date_query'] = array(
					'column' => 'post_date',
					'after'  => '1 month ago',
				);
			}

			if ( $do_events ) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'product_type',
						'field'    => 'slug',
						'terms'    => 'event',
					),
				);
				if ( ( $temp_yr != '' ) && ( $temp_wk != '' ) ) {
					$return_date        = getStartAndEndDate( $temp_wk, $temp_yr );
					$args['meta_query'] = array(
						'relation' => 'OR',
						array(
							'key'     => '_rq_event_start_date',
							'value'   => date( 'Y-m-d' ),
							'compare' => '>='
						),
						array(
							'key'     => '_rq_event_end_date',
							'value'   => date( 'Y-m-d' ),
							'compare' => '<='
						),
					);

				} else {
					$args['tax_query'] = array(
						array(
							'taxonomy' => 'product_type',
							'field'    => 'slug',
							'terms'    => 'simple',
						),
					);
					if ( ( $temp_yr != '' ) && ( $temp_wk != '' ) ) {
						$args['date_query'] = array(
							array(
								'year' => $temp_yr,
								'w'    => $temp_wk,
							),
						);
					}
				}

				$wp_query = new WP_Query( $args );

				ob_start();
				if ( $wp_query->have_posts() ) {

					do_action( 'woocommerce_before_shop_loop' );

					woocommerce_product_loop_start();
					woocommerce_product_subcategories();

					while ( have_posts() ) {
						the_post();
						wc_get_template_part( 'content', 'product' );
					}
					if ( $wp_query->max_num_pages > $args['paged'] ) {
						$next_page->zl_load_more( $args );
					}
					woocommerce_product_loop_end();

					//do_action( 'woocommerce_after_shop_loop' );


					$filtered = ob_get_contents();
				}
				ob_end_clean();
			} else {
				$args = array(
					'post_type'      => 'product',
					'posts_per_page' => $products_per_page,
					'paged'          => ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1,
				);
				if ( isset( $_POST['referer'] ) && $_POST['referer'] == 1 ) {
					//only featured posts from last 30 days
					$meta_query         = WC()->query->get_meta_query();
					$meta_query[]       = array(
						'key'   => '_featured',
						'value' => 'yes'
					);
					$args['meta_query'] = $meta_query;
					//only post dates with from last month
					$args['date_query'] = array(
						array(
							'column' => 'post_date',
							'after'  => '1 month ago',
						)
					);
				}


				if ( isset( $_POST['tab'] ) && $_POST['tab'] <> "" ) {
					$args['product_cat'] = $_POST['tab'];
				}
				if ( isset( $_POST['locs'] ) && $_POST['locs'] <> "" ) {
					$term_ids  = get_term_by( 'name', 'news', 'post_tag' );
					$post_data = $_POST['locs'];
					$q         = 'SELECT woocommerce_term_id FROM ' . $wpdb->woocommerce_termmeta . ' WHERE meta_key = "location" AND meta_value IN  ("' . implode( '","', $post_data ) . '")';
					$results   = $wpdb->get_row( $q, ARRAY_A );
					$prod_id   = array();
					if ( $results == null ) {
						$prod_id[] = 0;
					} else {
						foreach ( $results as $vendor_id => $key ) {
							$this_id = yith_get_vendor( $key );
							$prod_id = array_merge( $prod_id, $this_id->get_products() );
						}
					}
					$args['post__in'] = $prod_id;
				}
				if ( isset( $_POST['search'] ) && $_POST['search'] <> "" ) {
					$args['s'] = sanitize_text_field( $_POST['search'] );
				}


				$products_by_distance = array();
				$near_vendors         = array();

				if ( isset( $_POST['distance'] ) && $_POST['distance'] > 0 ) {
					$vendors     = YITH_Vendors()->get_vendors( array( 'enabled_selling' => true ) );
					$start_point = "";
					if ( isset( $_POST['origin'] ) && $_POST['origin'] <> "" ) {
						$start_point = sanitize_text_field( $_POST['origin'] );
						$start_lat   = sanitize_text_field( $_POST['origin_lat'] );
						$start_long  = sanitize_text_field( $_POST['origin_long'] );
					}
					$end_point = "";
					foreach ( $vendors as $vendor ) {
						$end_point = explode( ",", $vendor->geoloc );
						if ( $end_point <> false && isset( $end_point[1] ) && isset( $end_point[0] ) ) {
							$d = getDistance( $start_lat, $start_long, $end_point[0], $end_point[1] );
							if ( $d < $_POST['distance'] && $d <> - 1 ) {
								$near_vendors[] = $vendor->id;
							}
						}

					}
					//return vendor id -1 if distance filter is enabled
					if ( empty( $near_vendors ) ) {
						$near_vendors[] = - 1;
					}
				}

				if ( $do_events ) {
					if ( ! empty( $near_vendors ) ) {
						$args['tax_query'] = array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'product_type',
								'field'    => 'slug',
								'terms'    => 'event',
							),
							array(
								'taxonomy' => 'yith_shop_vendor',
								'field'    => 'id',
								'terms'    => $near_vendors
							),
						);
					} else {

						$args['tax_query'] = array(
							array(
								'taxonomy' => 'product_type',
								'field'    => 'slug',
								'terms'    => 'event',
							),
						);
					}

				} else {
					if ( ! empty( $near_vendors ) ) {
						$args['tax_query'] = array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'product_type',
								'field'    => 'slug',
								'terms'    => 'simple',
							),
							array(
								'taxonomy' => 'yith_shop_vendor',
								'field'    => 'id',
								'terms'    => $near_vendors
							),
						);
					} else {
						$args['tax_query'] = array(
							array(
								'taxonomy' => 'product_type',
								'field'    => 'slug',
								'terms'    => 'simple',
							),
						);
					}


				}
				$wp_query = new WP_Query( $args );

				if ( $wp_query->have_posts() ) {

					ob_start();
					do_action( 'woocommerce_before_shop_loop' );

					woocommerce_product_loop_start();
					woocommerce_product_subcategories();

					while ( have_posts() ) {
						the_post();
						( ! $do_events ) ? wc_get_template_part( 'content', 'product' ) : wc_get_template_part( 'content', 'event' );
					}
					if ( $wp_query->max_num_pages > $args['paged'] ) {
						$next_page->zl_load_more( $args );
					}
					woocommerce_product_loop_end();

					//do_action( 'woocommerce_after_shop_loop' );
					$filtered = ob_get_contents();

					ob_end_clean();
				}
			}
			echo ( $filtered == "" ) ? "<span class=\"woocommerce-result-count\">No Results Found</span>" : $filtered;
			wp_reset_postdata();
			woocommerce_reset_loop();
			die();
		} else {
			//errors
			exit();
		}
	}
}


function getStartAndEndDate($week, $year)
{

    $time = strtotime("1 January $year", time());
    $day = date('w', $time);
    $time += ((7*$week)+1-$day)*24*3600;
    $return[0] = date('Y-n-j', $time);
    $time += 6*24*3600;
    $return[1] = date('Y-n-j', $time);
    return $return;
}



function zl_shop_menus() {
	global $woocommerce;
	//$shop_page_url = wc_get_endpoint_url('shop','',get_permalink( wc_get_page_id( 'shop' ) ));
	$shop_page_url = site_url() . '/' . get_post( get_option( 'woocommerce_shop_page_id' ) )->post_name . '/';
	?>
	<li><a href="<?php echo $shop_page_url; ?>categories/">All Listings</a></li>
    <li><a href="<?php echo $shop_page_url; ?>events-list/">All Event List</a></li>
	<li><a href="https://listings.zoomlocal.com/search-zoomlocal">Search Vendors</a></li>			
<?php
}

function zl_vendor_dashboard_endpoint() {
	$vendor = yith_get_vendor( 'current', 'user' );

	if ( $vendor->is_valid() && $vendor->is_owner() ) {
		yith_wcpv_get_template( 'my-vendor-dashboard', array( 'vendor' => $vendor ), 'woocommerce/myaccount' );
	}
}

add_action( 'yith_vendors_account_approved', 'zl_customer_to_shop_manager', 90, 1 );


/**
 * @param $owner_id
 * This should change the default user role (customer) of yith_vendor_plugin to shop_manager. these roles are defined by woocommerce
 */
function zl_customer_to_shop_manager( $owner_id ) {
	$u = new WP_user( $owner_id );
	$u->remove_role( 'customer' );
	$u->add_role( 'shop_manager' );
}

add_action( 'yith_vendors_account_approved', 'zl_add_vendor_credits', 100, 4 );

/**
 * @param $owner_id ve
 * @param null $credits
 * @param null $expire_date
 */
function zl_add_vendor_credits( $owner_id, $credits = null, $expire_date = null ) {
	global $wpdb;
	$expire_on = ( $expire_date === null ) ? date( "Y-m-d H:i:s", strtotime( "+30 days", time() ) ) : $expire_date;
	//if date is not valid, exit
	if ( ! zl_validateDate( $expire_on ) ) {
		return;
	}
	$store        = yith_get_vendor( $owner_id, 'user' );
	$table        = $wpdb->prefix . "zoomlocal_credits";
	$credit_total = ( $credits === null ) ? 500 : $credits;
	$columns      = array(
		'expiration_date' => $expire_on,
		'total_credits'   => $credit_total,
		'vendor_id'       => $store->id
	);
	$format       = array(
		'%s',
		'%d',
		'%d'
	);
	$wpdb->replace( $table, $columns, $format );
}

//helper to validate date
function zl_validateDate( $date, $format = 'Y-m-d H:i:s' ) {
	$d = DateTime::createFromFormat( $format, $date );
	return $d && $d->format( $format ) == $date;
}

function getDistance($lat1, $lon1, $lat2, $lon2, $unit="m"){
	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);

	if ($unit == "K") {
		return ($miles * 1.609344);
	} else if ($unit == "N") {
		return ($miles * 0.8684);
	} else {
		return $miles;
	}
}