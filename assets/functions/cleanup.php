<?php

// Fire all our initial functions at the start
add_action( 'after_setup_theme', 'joints_start', 16 );

function joints_start() {

	// launching operation cleanup
	add_action( 'init', 'joints_head_cleanup' );
	// remove pesky injected css for recent comments widget
	add_filter( 'wp_head', 'joints_remove_wp_widget_recent_comments_style', 1 );
	// clean up comment styles in the head
	add_action( 'wp_head', 'joints_remove_recent_comments_style', 1 );
	// clean up gallery output in wp
	add_filter( 'gallery_style', 'joints_gallery_style' );

	// launching this stuff after theme setup
	joints_theme_support();

	// adding sidebars to Wordpress
	add_action( 'widgets_init', 'joints_register_sidebars' );
	// cleaning up excerpt
	add_filter( 'excerpt_more', 'joints_excerpt_more' );


} /* end joints start */

//The default wordpress head is a mess. Let's clean it up by removing all the junk we don't need.
function joints_head_cleanup() {
	// Remove category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// Remove post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// Remove EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// Remove Windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// Remove index link
	remove_action( 'wp_head', 'index_rel_link' );
	// Remove previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// Remove start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// Remove links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// Remove WP version
	remove_action( 'wp_head', 'wp_generator' );


} /* end Joints head cleanup */

// Remove injected CSS for recent comments widget
function joints_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

// Remove injected CSS from recent comments widget
function joints_remove_recent_comments_style() {
	global $wp_widget_factory;
	if ( isset( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'] ) ) {
		remove_action( 'wp_head', array(
			$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
			'recent_comments_style'
		) );
	}
}

// Remove injected CSS from gallery
function joints_gallery_style( $css ) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}

// This removes the annoying [�] to a Read More link
function joints_excerpt_more( $more ) {
	global $post;

	// edit here if you like
	return '...  <a class="excerpt-read-more" href="' . get_permalink( $post->ID ) . '" title="' . __( 'Read', 'jointstheme' ) . get_the_title( $post->ID ) . '">' . __( 'Read more &raquo;', 'jointstheme' ) . '</a>';
}

//  Stop WordPress from using the sticky class (which conflicts with Foundation), and style WordPress sticky posts using the .wp-sticky class instead
function remove_sticky_class( $classes ) {
	$classes   = array_diff( $classes, array( "sticky" ) );
	$classes[] = 'wp-sticky';

	return $classes;
}

add_filter( 'post_class', 'remove_sticky_class' );

//This is a modified the_author_posts_link() which just returns the link. This is necessary to allow usage of the usual l10n process with printf()
function joints_get_the_author_posts_link() {
	global $authordata;
	if ( ! is_object( $authordata ) ) {
		return false;
	}
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		esc_attr( sprintf( __( 'Posts by %s', 'jointstheme' ), get_the_author() ) ), // No further l10n needed, core will take care of this one
		get_the_author()
	);

	return $link;
}

function zl_registration_redirect() {
	return home_url( '/my-account/' );
}

add_filter( 'woocommerce_registration_redirect', 'zl_registration_redirect', 90 );

//use email as login instead of username
//remove wordpress authentication
/*remove_filter('authenticate', 'wp_authenticate_username_password', 20);
add_filter('authenticate', function($user, $email, $password){

	if(empty($email) || empty ($password)){
		$error = new WP_Error();

		if(empty($email)){
			$error->add('empty_username', __('<strong>ERROR</strong>: Email field is empty.'));
		}
		else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //Invalid Email
			$error->add('invalid_username', __('<strong>ERROR</strong>: Email is invalid.'));
		}

		if(empty($password)){ //No password
			$error->add('empty_password', __('<strong>ERROR</strong>: Password field is empty.'));
		}

		return $error;
	}

	$user = get_user_by('email', $email);

	if(!$user){
		$error = new WP_Error();
		$error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
		return $error;
	}
	else{
		if(!wp_check_password($password, $user->user_pass, $user->ID)){ //bad password
			$error = new WP_Error();
			$error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
			return $error;
		}else{
			return $user;
		}
	}
}, 20, 3);*/

/**
 * Restrict access to the administration screens.
 *
 * Only administrators will be allowed to access the admin screens,
 * all other users will be automatically redirected to the front of
 * the site instead.
 *
 * We do allow access for Ajax requests though, since these may be
 * initiated from the front end of the site by non-admin users.
 */
function restrict_admin_with_redirect() {

	if ( ! current_user_can( 'manage_options' ) && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
		wp_redirect( site_url() );
		exit;
	}
}

add_action( 'admin_init', 'restrict_admin_with_redirect', 1 );


add_filter( 'pre_user_login', 'zl_username_as_email' );
function zl_username_as_email( $user_login ) {
	if ( isset( $_POST['email'] ) ) {
		$user_login = sanitize_email( $_POST['email'] );
	}

	return $user_login;
}

//redirects to vendor signup page instead of consumer signup
add_action( 'template_redirect', 'redirect_my_account' );
function redirect_my_account() {
	global $wp_query;
	$reset_password_page = (isset($wp_query->query_vars['lost-password']))?true:false;
	if ( is_page( 'my-account' ) && ! is_user_logged_in() && ! $reset_password_page ) {
		wp_redirect( home_url( '/vendor-signup/' ) );
		exit();
	}
}

/*Tweaks to RedQ plugin starts here*/
if ( class_exists( 'RQ_Events' ) && is_admin() ) {
	global $rq_events, $woocommerce;
	remove_action( 'woocommerce_product_options_general_product_data', array( 'RQ_Events_Admin', 'event_data' ), 10 );
	add_action( 'woocommerce_product_options_general_product_data', 'zl_event_data', 10 );
	add_action( 'woocommerce_process_product_meta', 'zl_save_product_meta', 20 );
}
function zl_event_data() {
	global $post;
	$post_id = $post->ID;
	?>
	<script type="text/javascript">
		jQuery('._rq_event_start_date_field').after('<p class="form-field _rq_event_start_date_field">' +
		'<label for="_rq_event_end_date_field">Date Ends at...</label>' +
		'<input type="text" name="_rq_event_end_date" id="_rq_event_end_date" value="<?php echo get_post_meta( $post_id, '_rq_event_end_date', true ); ?>" placeholder="YYYY-MM-DD" />' +
		'</p>');
	</script>

<?php
}
function zl_save_product_meta($post_id){
	global $wpdb;
	$product_type         = empty( $_POST['product-type'] ) ? 'simple' : sanitize_title( stripslashes( $_POST['product-type'] ) );
	$has_additional_costs = false;
	if ( isset( $_POST['_featured'] ) &&  $_POST['_featured'] == 'on' ) {
		if(!add_post_meta($post_id, '_featured_date', date(' Y-m-d H:i:s '),true)){
			update_post_meta( $post_id, '_featured_date', date(' Y-m-d H:i:s ') );
		}
	}

	if ( 'event' !== $product_type ) {
		return;
	}
	$meta_to_save = array(
		'_rq_event_end_date'   => '',

	);
	//keep the whole checker for future use
	foreach ( $meta_to_save as $meta_key => $sanitize ) {
		$value = ! empty( $_POST[ $meta_key ] ) ? $_POST[ $meta_key ] : '';
		switch ( $sanitize ) {
			case 'int' :
				$value = absint( $value );
				break;
			case 'float' :
				$value = floatval( $value );
				break;
			case 'yesno' :
				$value = $value == 'yes' ? 'yes' : 'no';
				break;
			case 'issetyesno' :
				$value = $value ? 'yes' : 'no';
				break;
			case 'max_date' :
				$value = absint( $value );
				if ( $value == 0 )
					$value = 1;
				break;
			default :
				$value = sanitize_text_field( $value );
		}
		update_post_meta( $post_id, $meta_key, $value );
	}
}

function zl_featured_date( $product ) {
	$post_id = $product->id;
	if ( isset( $_POST['_featured'] ) &&  $_POST['_featured'] == 1 ) {
		if(!add_post_meta($post_id, '_featured_date', date(' Y-m-d H:i:s '),true)){
			update_post_meta( $post_id, '_featured_date', date(' Y-m-d H:i:s ') );
		}
	}
}
add_action( 'woocommerce_product_quick_edit_save', 'zl_featured_date' );

