<?php
add_action( 'wp_ajax_nopriv_zl_load_events_list', 'zl_load_events_list' );
add_action( 'wp_ajax_zl_load_events_list', 'zl_load_events_list' );


function zl_load_events_list(){
	global $post;
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ){
		$id = get_current_user_id();
		$args = array(
			'posts_per_page' => 99999,
			'author' => $id,
			'post_type' => 'product',
			'tax_query' => array(
				array(
					'taxonomy' => 'product_type',
					'field' => 'slug',
					'terms' => 'event'
				)
			),
		);
		$posts = get_posts($args);
		ob_start();
		foreach ( $posts as $post ) : setup_postdata( $post );
			include(locate_template('dashboard/event-item-list.php'));
		endforeach;
		$list = ob_get_contents();
		ob_end_clean();
		wp_reset_postdata();
		echo "
		<table id=\"event-list\">
			<thead>
				<tr>
					<th id=\"header-event-id\">ID</th>
					<th id=\"header-event-title\">Title</th>
					<th id=\"header-event-listing-date\">Listing Date</th>
					<th id=\"header-event-start-date\">Event Start Date</th>
					<th id=\"header-event-location\">Location</th>
					<th id=\"header-event-promoted\">Promoted</th>
				</tr>
			</thead>
			<tbody>
				$list
			</tbody>
		</table>";
		die();
	}else{
		exit;
	}
}

add_action( 'wp_ajax_nopriv_zl_load_events_details', 'zl_load_events_details' );
add_action( 'wp_ajax_zl_load_events_details', 'zl_load_events_details' );

function zl_load_events_details(){
	global $post;
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		ob_start();
		include(locate_template('dashboard/event-details.php'));
		$output = ob_get_contents();
		ob_end_clean();
		echo $output;
		die();
	}else{
		exit;
	}
}

add_action( 'wp_ajax_nopriv_zl_dashboard_events_add', 'zl_dashboard_events_add' );
add_action( 'wp_ajax_zl_dashboard_events_add', 'zl_dashboard_events_add' );

function zl_dashboard_events_add(){
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		ob_start();
		include(locate_template('dashboard/event-details.php'));
		$output = ob_get_contents();
		ob_end_clean();
		echo $output;
		die();
	}else{
		exit;
	}
}


add_action( 'wp_ajax_nopriv_zl_dashboard_events_delete', 'zl_dashboard_events_delete' );
add_action( 'wp_ajax_zl_dashboard_events_delete', 'zl_dashboard_events_delete' );

function zl_dashboard_events_delete(){
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		if(isset($_POST['event_id'])){
			if(wp_delete_post( $_POST['event_id'] )){
				$attachments = get_posts( array(
					'post_type'      => 'attachment',
					'posts_per_page' => -1,
					'post_status'    => 'any',
					'post_parent'    => $_POST['event_id']
				) );

				foreach ( $attachments as $attachment ) {
					if ( false === wp_delete_attachment( $attachment->ID ) ) {
						// Log failure to delete attachment.
					}
				}
				wp_reset_postdata();
				echo "Event Deleted";
			}else{
				echo "Something went wrong";
			}
		}else{
			echo "Event Id not found";
		}
		die();
	}else{
		exit;
	}
}


add_action( 'wp_ajax_nopriv_zl_dashboard_events_create', 'zl_dashboard_events_create' );
add_action( 'wp_ajax_zl_dashboard_events_create', 'zl_dashboard_events_create' );

function zl_dashboard_events_create(){
	global $post;
	$remove_credits = false;
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		if (
			isset( $_POST['my_image_upload_nonce'] )
			&& wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' )
		) {
			// The nonce was valid and the user has the capabilities, it is safe to continue.

			// These files need to be included as dependencies when on the front end.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );

		} else {
		}

		$yith_shop = yith_get_vendor( get_current_user_id(), 'user' );
		$title = (isset($_POST['event-title']))?sanitize_text_field($_POST['event-title']):false;
		$description = (isset($_POST['event-description']))?sanitize_text_field($_POST['event-description']):false;
		$street = (isset($_POST['event-address-street']))?sanitize_text_field($_POST['event-address-street']):false;
		$city = (isset($_POST['event-address-city']))?sanitize_text_field($_POST['event-address-city']):false;
		$state = (isset($_POST['event-address-state']))?sanitize_text_field($_POST['event-address-state']):false;
		$zip = (isset($_POST['event-address-zip']))?sanitize_text_field($_POST['event-address-zip']):false;
		$start_date = (isset($_POST['event-date-start']))?preg_replace("([^0-9-])", "", $_POST['event-date-start']):false;
		$end_date = (isset($_POST['event-date-end']))?preg_replace("([^0-9-])", "", $_POST['event-date-end']):$start_date;
		$map_lat = (isset($_POST['event-lat']))?sanitize_text_field($_POST['event-lat']):false;
		$map_lng = (isset($_POST['event-lng']))?sanitize_text_field($_POST['event-lng']):false;
		$end_date = ($end_date=="")?$start_date:$end_date;
//validations
		if($title && $description && $street && $city && $state && $zip && $start_date && $end_date && $map_lat && $map_lng){
			//process insert or update product here
			$post_args = array(
				'post_title' => $title,
				'post_content' => $description,
				'post_status' => 'publish',
				'post_type' => 'product'
			);
			if(isset($_POST['event-id'])){
				$post_args['ID'] = $_POST['event-id'];
			}
			if(isset($_POST['event-listing-date'])){
				$post_args['post_date'] = date('Y-m-d H:i:s', strtotime($_POST['event-listing-date']));
				$post_args['post_date_gmt'] = $post_args['post_date'];
			}

			$post_id = wp_insert_post($post_args);
			//update meta if post is properly inserted
			if($post_id<>0){
				//set vendor
				wp_set_object_terms($post_id, $yith_shop->term_taxonomy_id, 'yith_shop_vendor', true );
				//update meta
				//if new listing date is not same as previous, update _changed_date
				if(isset($_POST['event-listing-date'])){
					//
					$prev_date = date("Y-m-d",strtotime(get_post($post_id)->post_date));
					if($prev_date<>date("Y-m-d",strtotime($post_args['post_date']))){
						update_post_meta($post_id,'_changed_date',date("Y-m-d"));
						//if post dates are changed, also remove credits
						$remove_credits = true;
					}
				}else{
					//this should only run on creation date, since event-listing-date is required on edits
					update_post_meta($post_id,'_changed_date',date("Y-m-d"));
				}
				update_post_meta($post_id, '_rq_event_start_date', $start_date);
				update_post_meta($post_id, '_rq_event_end_date', $end_date);
				update_post_meta($post_id, '_rq_event_address_name', $street);
				update_post_meta($post_id, '_rq_event_region_name', $state);
				update_post_meta($post_id, '_rq_event_city_name', $city);
				update_post_meta($post_id, '_rq_event_zip_code', $zip);
				update_post_meta($post_id, '_rq_event_lon_name', $map_lat);
				update_post_meta($post_id, '_rq_event_lat_name', $map_lng);
				update_post_meta($post_id, '_rq_event_country_name', 'USA');
				update_post_meta($post_id, '_visibility', 'visible');
				if(isset($_POST['event-promoted'])){

					$promoted_changed = update_post_meta($post_id,'_featured',$_POST['event-promoted']);
					//if they change to promoted/featured to yes, remove credits
					if($_POST['event-promoted']=="yes" && $promoted_changed){
						$remove_credits = true;
					}
				}

				wp_set_object_terms($post_id, 'event', 'product_type');
				if(isset($_FILES['my_image_upload'])){
					$attachment_id = media_handle_upload( 'my_image_upload', 0 );
					if ( is_wp_error( $attachment_id ) ) {

					} else {
						//echo "The image was uploaded successfully!";
						update_post_meta($post_id, '_thumbnail_id', $attachment_id);
					}
				}
				//set categories
				if(isset($_POST['categories'])){
					if($_POST['categories']==""){
						//remove categories
						wp_set_object_terms($post_id,null, 'product_cat' );
					}else{
						$categories = explode(',',$_POST['categories']);
						wp_set_object_terms($post_id, $categories, 'product_cat' );
					}
				}

				if($remove_credits){
					$remaining_creds = decreaseCredits();
				}

				if(isset($_POST['event-id'])){
					wp_publish_post( $post_id );
					echo "Event Updated";
				}else{
					echo $post_id;
				}
			}
		}else{
			echo "0";
		}
		die();
	}else{
		exit;
	}
}

add_action( 'wp_ajax_nopriv_zl_update_credits', 'zl_update_credits' );
add_action( 'wp_ajax_zl_update_credits', 'zl_update_credits' );

function zl_update_credits(){

	$yith_shop = yith_get_vendor( get_current_user_id(), 'user' );

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		echo zl_get_user_credits($yith_shop->id);
		die();
	}else{
		exit;
	}
}

