<?php
function joints_scripts_and_styles() {
	global $wp_styles, $wp_query, $post, $woocommerce; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
	if ( ! is_admin() ) {
		$theme_version = wp_get_theme()->Version;


		// Removes WP version of jQuery
		wp_deregister_script( 'jquery' );
		wp_deregister_script( 'jquery-migrate' );

		// Loads jQuery from vendor Files
		wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/vendor/foundation/js/vendor/jquery.js', array(), '2.1.3', true );
		wp_enqueue_script( 'jquery-migrate', get_template_directory_uri() . '/js/vendor/jquery-migrate-1.2.1.js', array( 'jquery' ), '1.2.1', true );

		// Modernizr from vendor Files
		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/vendor/foundation/js/vendor/modernizr.js', array(), '2.8.3', true );

		// Foundation Scripts

		wp_enqueue_script( 'whatinput-js', get_template_directory_uri() . '/js/vendor/what-input.min.js', array(), '', true );
		wp_enqueue_script( 'foundation.min-js', get_template_directory_uri() . '/js/foundation.min.js', array(), '', true );
		wp_enqueue_script( 'app-js', get_template_directory_uri() . '/js/app.js', array(), '', true );
		wp_enqueue_script( 'woco-js', get_template_directory_uri() . '/js/woco.accordion.js', array(), '', true );
		wp_enqueue_script( 'meanmenu-js', get_template_directory_uri() . '/assets/js/jquery.meanmenu.js', array(), '', true );

		// Adding Foundation scripts file in the footer
		//wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/assets/vendor/foundation/js/foundation.min.js', array( 'jquery' ), '', true );
       
		// Adding scripts file in the footer
		//wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );

		// Normalize from vendor files
		wp_enqueue_style( 'normalize-css', get_template_directory_uri() . '/assets/vendor/foundation/css/normalize.min.css', array(), '', 'all' );

		
		// Adding Foundation styles
		// wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/assets/vendor/foundation/css/foundation.min.css', array(), '', 'all' );

		// Register main stylesheet
		// wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

		

		//This is added to to wrap current listing on vendor page and have the styling applied
		if ( is_tax( 'yith_shop_vendor' ) ) {
			add_action( 'woocommerce_before_shop_loop', 'zl_current_related_listing_start', 90 );
			add_action( 'woocommerce_after_shop_loop', 'zl_current_related_listing_end', 90 );
		}
		function zl_current_related_listing_start() {
			echo '<div id="carousel-related" class="related products medium-12 columns">';
		}

		function zl_current_related_listing_end() {
			echo '</div>';
		}

		// Comment reply script for threaded comments
		if ( is_singular() AND comments_open() AND ( get_option( 'thread_comments' ) == 1 ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		//enqueue script for my-account
		if($post<>null){
			if ( $post->post_name === 'my-account' || $post->post_name === 'edit-account' ) {
				wp_enqueue_style( 'img-picker-css', get_template_directory_uri() . '/woocommerce/scripts/image-picker.css', array(), '', 'all' );
				wp_enqueue_style( 'zl-shop', get_template_directory_uri() . '/assets/css/zl-shop.css', array(), '', 'all' );
				wp_enqueue_style( 'my-account-css', get_template_directory_uri() . '/assets/css/my-account.css', array(), '', 'all' );
				wp_enqueue_style( 'time-picker-css', get_template_directory_uri() . '/assets/css/jquery.timepicker.css', array(), '', 'all' );
				wp_enqueue_script( 'maps-google-api-js', $google_mp_js, array( 'jquery' ), null, true );
				wp_enqueue_script( 'timepicker-js', get_template_directory_uri() . '/woocommerce/scripts/jquery.timepicker.js', array( 'jquery' ), '1.5', true );

			wp_enqueue_script( 'img-picker-js', get_template_directory_uri() . '/woocommerce/scripts/image-picker.min.js', array( 'jquery' ), null, true );

			wp_enqueue_script( 'img-picker-js', get_template_directory_uri() . '/woocommerce/scripts/image-picker.min.js', array( 'jquery' ), null, true );
			wp_enqueue_script( 'my-account-js', get_template_directory_uri() . '/woocommerce/scripts/my-account.js', array( 'jquery' ), null, true );

		}
		}


		//enqueue script for shop-pages
		if ( $GLOBALS['template'] === get_template_directory() . "/woocommerce/archive-product.php" || is_shop() || $GLOBALS['template'] === get_template_directory() . "/woocommerce.php" ) {

			$events_list = (isset($wp_query->query_vars['events-list']))?true:false;
			wp_enqueue_style( 'zl-shop', get_template_directory_uri() . '/assets/css/zl-shop.css', array(), '', 'all' );
			wp_enqueue_script( 'maps-google-api-js', 'https://maps.googleapis.com/maps/api/js', array( 'jquery' ), null, true );
			wp_enqueue_script( 'jquery-address', get_template_directory_uri() . '/assets/js/jquery.address-1.5.min.js', array( 'jquery' ), '1.5', true );
			wp_register_script( 'zl-filter-script', get_template_directory_uri() . '/woocommerce/scripts/shop-ajax.js', array(), null, true );
			wp_enqueue_script( 'zl-filter-script' );
			//if from listings, pass true else if  from events, pass true else false
			$referer = ( isset( $wp_query->query_vars['categories'] ) || is_front_page() ) ? true : $events_list;
			wp_localize_script(
				'zl-filter-script',
				'zl_ajax_script',
				array(
					'ajax_url'       => admin_url( 'admin-ajax.php' ),
					'on_events_page' => $events_list,
					'referer'        => $referer
				)
			);

		}

		if ( ! is_tax() && ! is_category() && basename( get_page_template() ) == "page-vendor-signup.php" ) {
			wp_enqueue_style( 'zl-shop', get_template_directory_uri() . '/assets/css/zl-shop.css', array(), '', 'all' );
		}

		// jquery-ui-css
		wp_enqueue_style( 'jquery-ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css' );

		// global overiding stylesheet
		wp_enqueue_style( 'global-overriding-css', get_template_directory_uri() . '/assets/css/global-override.css', array(), '', 'all' );

		// Bootstrap from CDN -pri
		wp_enqueue_style( 'font-awesome-min', '//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', array(), '', 'all' );

		// Foundation css-pri
		wp_enqueue_style( 'foundation', get_template_directory_uri() . '/css/foundation.css', array(), '', 'all' );

		// listing css-pri
		wp_enqueue_style( 'listing-p', get_template_directory_uri() . '/css/listing.css', array(), '', 'all' );

		// style css-pri
		wp_enqueue_style( 'style-p', get_template_directory_uri() . '/css/style.css', array(), '', 'all' );

		// WooCommerce css-pri
		wp_enqueue_style( 'woco-accordion', get_template_directory_uri() . '/css/woco-accordion.css', array(), '', 'all' );


	}

	if ( basename( get_page_template() ) === "page-pricing.php" ) {
		wp_enqueue_style( 'zl-pricing', get_template_directory_uri() . '/assets/css/pricing.css', array(), '', 'all' );
		wp_enqueue_script( 'zl-pricing-js', get_template_directory_uri() . '/assets/js/pricing.js', array(), '', 'true' );
	}
}

add_action( 'wp_enqueue_scripts', 'joints_scripts_and_styles', 999 );


add_action( 'wp_footer', 'back_to_top' );
function back_to_top() {
	global $wp_query, $woocommerce;
	$show_category_filter = isset( $wp_query->query_vars['categories'] );
	$show_events_page     = isset( $wp_query->query_vars['events-list'] );
	if ( $show_category_filter || $show_events_page || is_shop() ) {
		echo '<div id="float-back-to-top">
			<a id="category-filter-button-bot" href="#" data-reveal-id="categoriesModal" class="button" onclick="javascript: location.hash=\'#main\'; location.hash=\'\';"><i class="fa fa-tags"></i></a>
			 <a id="location-filter-button-bot" href="#" data-reveal-id="locationsModal" class="button"  onclick="javascript: location.hash=\'#main\'; location.hash=\'\';"><i class="fa fa-map-marker"></i></a>
			 <a id="distance-filter-button-bot" href="#" data-reveal-id="distanceModal" class="button"  onclick="javascript: location.hash=\'#main\'; location.hash=\'\';"><i class="fa fa-compass"></i></a>
			 <a id="totop" href="#" class="button"><i class="fa fa-arrow-up"></i></a>
		</div>';
	} else {
		echo '<div id="float-back-to-top">
			 <a id="totop" href="#" class="button"><i class="fa fa-arrow-up"></i></a>
		</div>';
	}
}


add_action( 'wp_footer', 'back_to_top_script', 900 );
function back_to_top_script() {
	global $wp_query;
	$show_category_filter = isset( $wp_query->query_vars['categories'] );
	$show_events_page     = isset( $wp_query->query_vars['events-list'] );
	if ( $show_category_filter || $show_events_page ) {

	}
	echo '<script type="text/javascript">
            jQuery(document).ready(function($){
			if ( $(this).scrollTop() > 350 ){
                $("#float-back-to-top").fadeIn();
			}else{
                $("#float-back-to-top").fadeOut();
            }
                $(window).scroll(function () {
                    if ( $(this).scrollTop() > 350 )
                        $("#float-back-to-top").fadeIn();
                    else
                        $("#float-back-to-top").fadeOut();
                });

                $("#totop").click(function () {
                    $("body,html").animate({ scrollTop: 0 }, 800 );
                    return false;
                });
            });
        </script>';
}

/*Other Custom Scripts*/
// Register Script
function zl_custom_scripts() {
	wp_register_script( 'foundation-datepicker', get_template_directory_uri() . '/assets/vendor/datepicker/foundation-datepicker.min.js', false, false, true );
	wp_register_style( 'foundation-datepicker-css', get_template_directory_uri() . '/assets/vendor/datepicker/foundation-datepicker.css', false, false );

	wp_register_script( 'create-event-js', get_template_directory_uri() . '/assets/js/create-event.js', false, false, true );
	wp_register_style( 'create-event-css', get_template_directory_uri() . '/assets/css/create-event.css', false, false );

	wp_register_script( 'event-dashboard-js', get_template_directory_uri() . '/assets/js/event-dashboard.js', false, false, true );
	wp_register_style( 'event-dashboard-css', get_template_directory_uri() . '/assets/css/event-dashboard.css', false, false );

	wp_register_script( 'maps-google', "//maps.googleapis.com/maps/api/js", false, false, true );

	if ( basename( get_page_template() ) === "page-create-event.php" ) {

		wp_enqueue_script( 'maps-google' );
		wp_enqueue_script( 'foundation-datepicker' );
		wp_enqueue_style( 'foundation-datepicker-css' );

		wp_enqueue_script( 'create-event-js' );
		wp_enqueue_style( 'create-event-css' );

	}
	if ( basename( get_page_template() ) === "page-event-dashboard.php" ){

		wp_enqueue_script( 'maps-google' );
		wp_enqueue_script( 'foundation-datepicker' );
		wp_enqueue_style( 'foundation-datepicker-css' );

		wp_enqueue_script('event-dashboard-js');
		wp_enqueue_style('event-dashboard-css');
		wp_localize_script(
			'event-dashboard-js',
			'events_dashboard_script',
			array(
				'ajax_url'       => admin_url( 'admin-ajax.php' ),
			)
		);

	}


}

add_action( 'wp_enqueue_scripts', 'zl_custom_scripts' );
