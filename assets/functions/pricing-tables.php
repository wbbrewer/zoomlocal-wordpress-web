<?php

/**
 * Created by Moon.
 */

// add a product type
add_filter( 'product_type_selector', 'zl_membership_product_type' );
function zl_membership_product_type( $types ){
	$types[ 'zl_membership' ] = __( 'Membership Plan' );
	return $types;
}

add_action( 'plugins_loaded', 'zl_membership_create_product_type' );
function zl_membership_create_product_type(){
	// declare the product class
	class WC_Product_Wdm extends WC_Product{
		public function __construct( $product ) {
			$this->product_type = 'zl_membership';
			parent::__construct( $product );
			// add additional functions here
		}
	}
}

/*Add shortcode for pricing table on front end this is used on page-pricing.php*/
function zl_pricing_product_shortcode($atts){
	if(empty($atts)){
		return '';
	}

	$meta_query = WC()->query->get_meta_query();

	$args = array(
		'post_type'      => 'product',
		'posts_per_page' => 1,
		'no_found_rows'  => 1,
		'post_status'    => 'publish',
		'meta_query'     => $meta_query,
	);

	if ( isset( $atts['id'] ) ) {
		$args['p'] = $atts['id'];
	}
	ob_start();

	$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );

	if ( $products->have_posts() ) : ?>
		<?php while ( $products->have_posts() ) : $products->the_post(); ?>

			<?php wc_get_template_part( 'content', 'price-front' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php endif;

	wp_reset_postdata();


	return '<div>' . ob_get_clean() . '</div>';


}
add_shortcode('membership_plan','zl_pricing_product_shortcode');