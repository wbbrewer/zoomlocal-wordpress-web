//smooth scroll
jQuery.fn.extend({
    scrollToMe: function () {
        var x = jQuery(this).offset().top - 100;
        jQuery('html,body').animate({scrollTop: x}, 400);
    }});


$('#event-date-start').fdatepicker({
    format: 'yyyy-mm-dd',
    disableDblClickSelection: true
});

$(document).on('click', '#submit-event-button', function (e) {
    e.preventDefault();
    var message = $('#error-message');
    message.text("");
    var title = $('#event-title').val();
    var description = $('#event-description').val();
    var street = $('#event-address-street').val();
    var city = $('#event-address-city').val();
    var state = $('#event-address-state').val();
    var zip = $('#event-address-zip').val();
    var start_date = $('#event-date-start').val();
    if(title==null || title==""){
        $("#event-title").scrollToMe();
        $("#event-title").focus();
        message.text("Please Enter Title");
        return false;
    }
    if(description==null || description==""){
        $("#event-description").scrollToMe();
        $("#event-description").focus();
        message.text("Please Enter Description");
        return false;
    }
    if(street==null || street==""){
        $("#event-address-street").scrollToMe();
        $("#event-address-street").focus();
        message.text("Please Enter Street Address");
        return false;
    }
    if(state==null || state==""){
        $("#event-address-state").scrollToMe();
        $("#event-address-state").focus();
        message.text("Please Enter State");
        return false;
    }
    if(zip==null || zip==""){
        $("#event-address-zip").scrollToMe();
        $("#event-address-zip").focus();
        message.text("Please Enter Zip");
        return false;
    }
    if(start_date==null || start_date==""){
        $("#event-date-start").scrollToMe();
        $("#event-date-start").focus();
        message.text("Event Start Date is required");
        return false;
    }
    if(city==null || city==""){
        message.text("Please Enter a City");
        $("#event-address-city").scrollToMe();
        $("#event-address-city").focus();
        return false;
    }
    //validates the address
    var map_address = street+' '+city+' '+state+' '+zip+' USA';
    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': map_address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            $("#event-lat").val(results[0].geometry.location.lat());
            $("#event-lng").val(results[0].geometry.location.lng());
        }else{
            $("#event-lat").val("");
            $("#event-lng").val("");
        }
    });
    if(
        $("#event-lat").val()==null ||$("#event-lat").val() == "",
        $("#event-lng").val()==null || $("#event-lng").val() == ""
    ){
        message.text("Address is not accurate. You can verify at maps.google.com.");
        return false;
    }

    $('#create-event-form').submit();
});