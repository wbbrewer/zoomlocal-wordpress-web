
//smooth scroll
jQuery.fn.extend({
    scrollToMe: function () {
        var x = jQuery(this).offset().top - 100;
        jQuery('html,body').animate({scrollTop: x}, 400);
    }});

var fadetimes = 1500; //fadetime

//load details using ajax on load
$(document).on('ready', function () {
    zl_load_list();
});
function zl_load_list(edit){
    var loader_id = "event-list-loader";
    var loader = "<div id =\""+loader_id+"\" class=\"loader-box\"><div class=\"loader\">Loading...</div></div>";
    $('section#event-list-section').append(loader);

    jQuery.ajax({
        url : events_dashboard_script.ajax_url,
        type : 'post',
        data : {
            action : 'zl_load_events_list'
            //args: "this"
        },
        success : function( response ) {

            $('table#event-list').remove();
            $('section#event-list-section').append(response);
            $('table#event-list').fadeIn(fadetimes);
            $("#"+loader_id).fadeOut(fadetimes,function (){
                $(this).remove();
            });
            if(edit===undefined){
                //maybe do something for default
            }else{
                $("tr[data-id='"+edit+"']").trigger("click");
            }

        }
    });
}

function load_credits(){
    jQuery.ajax({
        url : events_dashboard_script.ajax_url,
        type : 'post',
        data : {
            action : 'zl_update_credits'
        },
        success : function( response ) {
            $('#remaining-credits').text(response);
        }
    });
}

$(document).on('click', 'tr.event-row', function (e) {
    //dont use toggle
    $('tr.event-row').removeClass("displayed");
    $(this).addClass("displayed");

    var event_id = $(this).data("id");
    var loader_id = "event-loader";
    var loader = "<div id =\""+loader_id+"\" class=\"loader-box\"><div class=\"loader\">Loading...</div></div>";
    $('section#event-details').append(loader);
    var loader_list = "list-loader";
    var loader_list = "<div id =\""+loader_list+"\" class=\"loader-box\"><div class=\"loader\">Loading...</div></div>";
    $('section#event-list-section').append(loader_list);
    $("#event-loader,#list-loader").fadeIn(fadetimes);
    jQuery.ajax({
        url : events_dashboard_script.ajax_url,
        type : 'post',
        data : {
            action : 'zl_load_events_details',
            event_id: event_id
        },
        success : function( response ) {
            $('form#create-event-form,#event-loader,#list-loader').fadeOut(function(){
                $(this).remove();
            });
            $('section#event-details').append(response);
            $('form#create-event-form').fadeIn(fadetimes);
            $('#event-date-start, #event-date-end, #event-listing-date').fdatepicker({
                format: 'yyyy-mm-dd',
                disableDblClickSelection: true
            });
            $(document).foundation();
            $("#"+loader_id,"#"+loader_list).fadeOut(fadetimes,function (){
                $(this).remove();
            });
        }
    });
});

$(document).on('click', 'a#event-add-new', function (e) {
    var loader_id = "event-loader"
    var loader = "<div id =\""+loader_id+"\" class=\"loader-box\"><div class=\"loader\">Loading...</div></div>";
    //alert(loader);
    $('section#event-details').append(loader);
    $("#"+loader_id).fadeIn(fadetimes, function(){
            jQuery.ajax({
                url : events_dashboard_script.ajax_url,
                type : 'post',
                data : {
                    action : 'zl_dashboard_events_add'
                },
                success : function( response ) {
                    $('form#create-event-form').fadeOut(function(){
                        $(this).remove();
                    });
                    $('section#event-details').append(response);
                    $('form#create-event-form').fadeIn();
                    $('#event-date-start, #event-date-end').fdatepicker({
                        format: 'yyyy-mm-dd',
                        disableDblClickSelection: true
                    });
                    $(document).foundation();
                    $("#"+loader_id).fadeOut(fadetimes,function (){
                        $(this).remove();
                    });
                }
            });
        }
    );

});

$(document).on('click', 'a#delete-event-button', function (e) {
    e.preventDefault();
    var event_id = $('#event-id').val();
    var go = confirm('Are You Sure you want to delete this item?');
    if(go){
        jQuery.ajax({
            url : events_dashboard_script.ajax_url,
            type : 'post',
            data : {
                action : 'zl_dashboard_events_delete',
                event_id: event_id
            },
            success : function( response ) {
                zl_load_list();
                $('form#create-event-form').remove();
                var deleted =
                    '<form id="create-event-form">'
                    +'<div class="alert-box success">'
                    +'<strong> Event Deleted </strong> "You can click any item on the list to promote and share your listing!"'
                    +'</div>'
                    +'</form>';
                $('section#event-details').append(deleted);
                $('form#create-event-form').fadeIn(fadetimes);
            }
        });

    }

});


$(document).on('click', 'a#create-event-button,a#update-event-button', function (e) {
    e.preventDefault();
    $(this).addClass("disabled");
    var message = $('#error-message');

    //revalidates the address
    $("#event-lat").val("");
    $("#event-lng").val("");

    message.text("");
    var title = $('#event-title').val();
    var description = $('#event-description').val();
    var street = $('#event-address-street').val();
    var city = $('#event-address-city').val();
    var state = $('#event-address-state').val();
    var zip = $('#event-address-zip').val();
    var start_date = $('#event-date-start').val();
    if(title==null || title==""){
        $("#event-title").scrollToMe();
        $("#event-title").focus();
        message.text("Please Enter Title");
        $(this).removeClass("disabled");
        $("#status").fadeIn(fadetimes);
        return false;
    }
    if(description==null || description==""){
        $("#event-description").scrollToMe();
        $("#event-description").focus();
        message.text("Please Enter Description");
        $(this).removeClass("disabled");
        $("#status").fadeIn(fadetimes);
        return false;
    }
    if(street==null || street==""){
        $("#event-address-street").scrollToMe();
        $("#event-address-street").focus();
        message.text("Please Enter Street Address");
        $(this).removeClass("disabled");
        $("#status").fadeIn(fadetimes);
        return false;
    }
    if(state==null || state==""){
        $("#event-address-state").scrollToMe();
        $("#event-address-state").focus();
        message.text("Please Enter State");
        $(this).removeClass("disabled");
        $("#status").fadeIn(fadetimes);
        return false;
    }
    if(zip==null || zip==""){
        $("#event-address-zip").scrollToMe();
        $("#event-address-zip").focus();
        message.text("Please Enter Zip");
        $(this).removeClass("disabled");
        $("#status").fadeIn(fadetimes);
        return false;
    }
    if(start_date==null || start_date==""){
        $("#event-date-start").scrollToMe();
        $("#event-date-start").focus();
        message.text("Event Start Date is required");
        $(this).removeClass("disabled");
        $("#status").fadeIn(fadetimes);
        return false;
    }
    if(city==null || city==""){
        message.text("Please Enter a City");
        $("#event-address-city").scrollToMe();
        $("#event-address-city").focus();
        $(this).removeClass("disabled");
        $("#status").fadeIn(fadetimes);
        return false;
    }
    var submit = new FormData();
    submit.append('action','zl_dashboard_events_create');
    submit.append('event-title',title);
    submit.append('event-description',description);
    submit.append('event-address-street',street);
    submit.append('event-address-city',city);
    submit.append('event-address-state',state);
    submit.append('event-address-zip',zip);
    submit.append('event-date-start',start_date);
    submit.append('event-date-end', $('#event-date-end').val());
    submit.append('my_image_upload_nonce',$("#my_image_upload_nonce").val());
    submit.append('my_image_upload',$("#my_image_upload")[0].files[0]);

    var foo = [];
    //$("input[name='cat_group[]']:checked").val()
    $("input[name='cat_group[]']:checked").each(function(i, selected){
        foo[i] = $(selected).val();
    });
    submit.append('categories',foo);
    
    if($(this).attr('id')=='update-event-button'){
        if($('#event-listing-date').val() === ""){
            message.text("Please Enter Listing Date");
            $(this).removeClass("disabled");
            $("#status").fadeIn(fadetimes);
            return false;
        }
        submit.append('event-id',$('#event-id').val() );
        submit.append('event-listing-date', $('#event-listing-date').val());
        submit.append('event-promoted',$('#event-promoted').val());
    }

    var map_address = street+','+city+','+state+''+zip+',USA';
    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': map_address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            $("#event-lat").val(results[0].geometry.location.lat());
            $("#event-lng").val(results[0].geometry.location.lng());
            submit.append('event-lat',$("#event-lat").val());
            submit.append('event-lng',$("#event-lng").val());
            var go = true;
            if(
                $('#event-listing-date').val() != $('#old-listing-date').val() ||
                ($('#event-promoted').val() === "yes" && $('#old-promoted').val() == "no")
            ){
                go = confirm('Updating listing date or setting this event to promoted will cost you one credit?');
            }
            if(go){
                var loader_id = "event-loader2"
                var loader = "<div id =\""+loader_id+"\" class=\"loader-box\"><div class=\"loader\">Loading...</div></div>";
                //alert(loader);
                $('section#event-details').append(loader);
                $("#"+loader_id).fadeIn(fadetimes, function(){
                        jQuery.ajax({
                            url : events_dashboard_script.ajax_url,
                            type : 'post',
                            data : submit,
                            processData: false,
                            contentType: false,
                            success : function( response ) {
                                zl_load_list();
                                if(response=="Event Updated"){
                                    load_credits();
                                    $('form#create-event-form').fadeOut(function(){
                                        $(this).remove();
                                    });
                                    var updated =
                                        '<form id="create-event-form">'
                                            +'<div class="alert-box success">'
                                                +'<strong> Event Updated </strong> "You can click any item on the list to promote and share your listing!"'
                                            +'</div>'
                                        +'</form>';
                                    $('section#event-details').append(updated);
                                    $('form#create-event-form').fadeIn(fadetimes);
                                }else if(response=="0"){
                                    message.text("Error Updating Information. Please try again.");
                                    $(this).removeClass("disabled");
                                }else if(parseInt(response,10)>0){
                                    $('form#create-event-form').fadeOut(function(){
                                        $(this).remove();
                                    });
                                    var good =
                                        '<form id="create-event-form">'
                                            +'<div class="alert-box success">'
                                                +'<strong> Event Added </strong> "You can click any item on the list to promote and share your listing!"'
                                            +'</div>'
                                        +'</form>';
                                    $('section#event-details').append(good);
                                    $('form#create-event-form').fadeIn(fadetimes);
                                }

                                $('#event-date-start, #event-date-end').fdatepicker({
                                    format: 'yyyy-mm-dd',
                                    disableDblClickSelection: true
                                });
                                $(document).foundation();
                                $("#"+loader_id).fadeOut(fadetimes,function (){
                                    $(this).remove();
                                });
                            }
                        });
                    }
                );


            }
        }else{
            $("#event-lat").val("");
            $("#event-lng").val("");
            message.text("Please Verify Address");
            $(this).removeClass("disabled");
            return false;
        }
    });


});

//displaying image before it is uploaded
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image-upload').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('change', 'input#my_image_upload', function (e) {
    readURL(this);
});

